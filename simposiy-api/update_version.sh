#!/bin/bash

red=$(tput setaf 1)
green=$(tput setaf 2)

# Script to automatically update the version 
# in the Django project and to generate and 
# update the CHANGELOG.md file.

#### Enviroment Variabls ####

POETRY=true
PREFIX_RED="${red}[+]${reset} "

#### Functions ####
preInstall(){
  # Function for install git-changelog and commit-linter
  read -p "$PREFIX_RED${green}Do you want to install commit-linter and git-changelog: ${reset}" ANSWER; 

  if [[ $ANSWER =~ ^([yY][eE][sS]|[yY])$ ]]
  then 
    read -p "$PREFIX_RED${green}What utility to use to install ${red}pip${reset} or ${red}poetry${reset}: ${reset}" ANSWER_TWO; 

    if [[ $ANSWER_TWO == "poetry" ]]
    then 
      echo "$PREFIX_RED${green}Install package use poetry ${reset}"
    else
      echo "$PREFIX_RED${green}Install package use pip ${reset}"
      POETRY=false
    fi

    if [[ $POETRY ]]
    then
      poetry -D add git-changelog commit-linter
    else
      pip install git-changelog commit-linter
    fi
  fi
}

generateChangeLog(){
  # Function for generate changelog
  if [[ -e $(pwd)/CHANGELOG.md ]]
  then
    git-changelog -Tbio CHANGELOG.md -c angular -s fix,feat,refactor,test,ci
  else
     git-changelog -o CHANGELOG.md -c angular -s feat,fix,refactor,test,ci 
  fi
}

updateVersionToFile(){
  # Func replace old api version in settings file
  prefix="API_VERSION = "
  sed -i "s/$prefix$OLD_API_VERSION/$prefix$API_VERSION/" $(pwd)/$PATH_TO_SETTINGS/settings.py
}

updateAppVersion(){
  # Function for update version in setting.py

  read -p "$PREFIX_RED${green}Enter the name of the folder where the settings.py file is located : ${reset}" PATH_TO_SETTINGS; 
  prefix="API_VERSION = "
  while IFS= read -r LINE; do
      if [[ $LINE == API_VERSION* ]]
      then
        OLD_API_VERSION=${LINE#"$prefix"}
        echo "$ Current api version ${red}$OLD_API_VERSION${reset}"
      fi
    done < "$(pwd)/$PATH_TO_SETTINGS/settings.py"

  # TODO update version to file and create git teg
  read -p "$PREFIX_RED${green}Enter the new version: ${reset}" API_VERSION; 

  git tag -a $API_VERSION -m "version $API_VERSION"        
  generateChangeLog
  updateVersionToFile
  echo "Updated api version ${red}$API_VERSION${reset}"
}

##### Call Function #####
preInstall
updateAppVersion