from typing import Union
from redis import StrictRedis

from core.settings import REDIS_PORT, REDIS_URL
from .repository import Repository


class RdbRepository(Repository):
    """Base parent class for Redis repositories"""

    rdb: StrictRedis
    _REDIS_DB_NUMBER: int

    def get(self, name: Union[str, bytes]) -> Union[str, bytes]:
        """Returned value by key name"""
        self.rdb.get(name=name)

    def delete(self, *names: Union[str, bytes]) -> int:
        """Removed key by key name"""
        self.rdb.delete(*names)

    def scan_keys(self, pattern: str):
        "Returns a list of all the keys matching a given pattern"
        result = []
        cur, keys = self.rdb.scan(cursor=0, match=pattern, count=2)
        result.extend(keys)
        while cur != 0:
            cur, keys = self.rdb.scan(cursor=cur, match=pattern, count=2)
            result.extend(keys)
        return result


class FirebaseRdbRepository(RdbRepository):
    """This class used for FirebaseCloudMessaging"""

    _REDIS_DB_NUMBER: int = 2

    def __init__(self) -> None:
        self.rdb = StrictRedis(
            host=REDIS_URL,
            port=REDIS_PORT,
            db=self._REDIS_DB_NUMBER,
            decode_responses=True,
        )


class AdminEmailsRdbRepository(RdbRepository):
    """
        This class used for sending email 
        messages for administrators and moderators
    """

    _REDIS_DB_NUMBER: int = 3

    def __init__(self) -> None:
        self.rdb = StrictRedis(
            host=REDIS_URL,
            port=REDIS_PORT,
            db=self._REDIS_DB_NUMBER,
            decode_responses=True,
        )
