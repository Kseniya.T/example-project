from typing import Optional

from apps.notification_app.models import Notification
from .repository import Repository
from apps.user_app.models import CustomUser


class NotificationRepository(Repository):
    """Repository for `notification_app`"""

    _notification_cls = Notification
    """Using for operations with `Notification` model"""

    @classmethod
    def create_notification(cls, related_user: CustomUser, title: str,
                            body: str, payload: Optional[dict] = None) -> Notification:
        """Method for creating a `Notification` object"""

        notification: Notification = cls._notification_cls.objects.create(
            related_user=related_user,
            title=title,
            body=body,
            payload=payload,
        )

        return notification

    @classmethod
    def get_unsent_notifications(cls):
        """Method for retrieving unsent `Notification` objects"""
        return cls._notification_cls.filter(is_sent=False).order_by('pk')

    @classmethod
    def make_sent_notifications(cls, list_pk: list[int]):
        """Method for setting up is_sent field `Notification` objects to True """
        return cls._notification_cls.filter(pk__in=list_pk).update(is_sent=True)

    @classmethod
    def make_viewed_notification(cls, notification: Notification) -> None:
        """Method for making viewed `notification`"""
        notification.make_viewed()

    @classmethod
    def make_viewed_all_notifications(cls, related_user_id: int) -> None:
        """Method for making viewed all `notification` user"""
        cls._notification_cls.objects.filter(
            related_user_id=related_user_id, is_viewed=False).update(is_viewed=True)

    @classmethod
    def get_unviewed_notification(cls, pk: int, related_user_id: int) -> Optional[Notification]:
        """Method for retrieving `Notification` object by `pk` and `related_user_pk`"""
        pk_is_valid = cls.validate_number(pk)
        if not pk_is_valid:
            raise ValueError('Enter a valid number')
        return cls.get_object_or_none(
            cls._notification_cls,
            pk=pk,
            related_user_id=related_user_id,
            is_viewed=False,
        )

    @classmethod
    def get_unviewed_notifications(cls, list_pk: list[int], related_user_id: int):
        """Method for retrieving `Notification` objects by `list_pk` and `related_user_pk`"""
        return cls._notification_cls.objects.filter(related_user_id=related_user_id, pk__in=list_pk, is_viewed=False)
