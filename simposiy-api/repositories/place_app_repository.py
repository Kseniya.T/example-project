from typing import List
from django.db import IntegrityError

from apps.place_app.choices import WeekDaysChoices
from apps.user_app.models import CustomUser
from utils.exceptions import (MAxNumberOfImages, MaxLimitOfPlaceTypesError,
                              NotFullFilledError, ObjectNotFoundError,
                              UniqueConstraint, WeekDayDoesntExists,
                              MAxNumberOfDishes, WrongNumberOfSeatsError)
from .repository import Repository
from apps.place_app.models import Place, Dishes, PlaceImages, PlaceOpeningHours
from apps.place_app.dto import (CreateDishDTO, CreatePlaceDTO,
                                CreatePlaceImagesDTO, CreatePlaceOpeningHoursDTO,
                                FilterPlaceDTO, PlaceDTO, UpdateDishesDTO)


class PlaceAppRepository(Repository):
    """Repository for place_app"""

    @staticmethod
    def create_place(owner: CustomUser, dto: CreatePlaceDTO) -> Place:
        """Method for create place in db, returns place object """

        if len(dto.type) > 3:
            raise MaxLimitOfPlaceTypesError

        if dto.minimum_available_seats != 2:
            raise WrongNumberOfSeatsError(detail='The minimum available number of seats must be equal to 2 only')
        
        if dto.minimum_available_seats > dto.maximum_available_seats:
            raise WrongNumberOfSeatsError(detail='The maximum number of seats cannot be less than the specified minimum number of seats')
        
        try:
            place: Place =  Place.objects.create(
                owner=owner, name=dto.name, address=dto.address, menu=dto.menu, description=dto.description,
                deposit=dto.deposit, cover_image=dto.cover_image,  minimum_available_seats=dto.minimum_available_seats,
                maximum_available_seats=dto.maximum_available_seats, email=dto.email, phone_number=dto.phone_number,
                discount=dto.discount, web_site=dto.web_site, latitude=dto.latitude, longitude=dto.longitude, city=dto.city)
        except IntegrityError as e:
            raise UniqueConstraint(detail=e)
        for type_id in dto.type:
            place.type.add(type_id)
        return place

    @staticmethod
    def partial_update_place(place: Place, dto: PlaceDTO) -> Place:
        """Method for update existing place"""
        
        name = dto.name  if dto.name  != None else place.name  
        address = dto.address if dto.address != None else place.address 
        menu = dto.menu if dto.menu != None else place.menu 
        description = dto.description if dto.description != None else place.description 
        deposit = dto.deposit if dto.deposit != None else place.deposit 
        cover_image = dto.cover_image if dto.cover_image != None else place.cover_image 
        discount = dto.discount if dto.discount != None else place.discount 
        minimum_available_seats = dto.minimum_available_seats if dto.minimum_available_seats != None else place.minimum_available_seats 
        email = dto.email if dto.email != None else place.email 
        phone_number = dto.phone_number if dto.phone_number != None else place.phone_number 
        maximum_available_seats = dto.maximum_available_seats if dto.maximum_available_seats != None else place.maximum_available_seats 
        web_site = dto.web_site if dto.web_site != None else place.web_site 
        latitude = dto.latitude if dto.latitude != None else place.latitude 
        longitude = dto.longitude if dto.longitude != None else place.longitude 
        city = dto.city if dto.city != None else place.city 
                
        place.update_fields(
            name=name, address=address, menu=menu, description=description,
            deposit=deposit, cover_image=cover_image,
            discount=discount, minimum_available_seats=minimum_available_seats, email=email, phone_number=phone_number,
            maximum_available_seats=maximum_available_seats, web_site=web_site, latitude=latitude, longitude=longitude, city=city)
        
        if dto.type != None:
            # clear previuse place types
            place.type.set([])

            for type_id in dto.type:
                place.type.add(type_id)

        return place

    @staticmethod
    def create_opening_hours(place: Place, dto: CreatePlaceOpeningHoursDTO) -> list[PlaceOpeningHours]:
        """Method for create opening hours in db"""

        if len(dto.weekdays) < 7:
            raise NotFullFilledError(
                detail='Please specify opening hours for all days of the week.')
        if len(dto.weekdays) > 7:
            raise UniqueConstraint(detail='You specified more than 7 days.')

        for weekday in dto.weekdays:
            if WeekDaysChoices.get_choice_label_by_value(weekday.get('weekday')) == None:
                raise WeekDayDoesntExists

            if PlaceOpeningHours.objects.filter(place=place, weekday=weekday.get('weekday')).exists():
                raise UniqueConstraint(
                    detail="Opening hours are already listed for this place.")

            if weekday.get('from_hour') == None and weekday.get('to_hour') != None:
                raise NotFullFilledError(
                    detail='Please enter correct business hours')

            if weekday.get('from_hour') != None and weekday.get('to_hour') == None:
                raise NotFullFilledError(
                    detail='Please enter correct business hours')

            if weekday.get('from_hour') == None and weekday.get('to_hour') == None:
                weekday['is_dayoff'] = True

        weekdays_list = [PlaceOpeningHours(place=place, weekday=weekday.get('weekday'), from_hour=weekday.get(
            'from_hour'), to_hour=weekday.get('to_hour'), is_dayoff=weekday.get('is_dayoff')) for weekday in dto.weekdays]

        try:
            objs: list[PlaceOpeningHours] = PlaceOpeningHours.objects.bulk_create(
                weekdays_list)
        except Exception as e:
            raise UniqueConstraint

        return objs

    @staticmethod
    def update_opening_hours(place: Place, dto: CreatePlaceOpeningHoursDTO):
        """Method for bulk_update opening hours"""
        weekdays_list = [PlaceOpeningHours(place=place, pk=weekday.get('id'), weekday=weekday.get('weekday'), from_hour=weekday.get(
            'from_hour'), to_hour=weekday.get('to_hour'), is_dayoff=weekday.get('is_dayoff')) for weekday in dto.weekdays]

        if len(dto.weekdays) > 7:
            raise UniqueConstraint(detail='You specified more than 7 days.')

        for weekday in dto.weekdays:
            if WeekDaysChoices.get_choice_label_by_value(weekday.get('weekday')) == None:
                raise WeekDayDoesntExists

            if weekday.get('from_hour') == None and weekday.get('to_hour') != None:
                raise NotFullFilledError(
                    detail='Please enter correct business hours')

            if weekday.get('from_hour') != None and weekday.get('to_hour') == None:
                raise NotFullFilledError(
                    detail='Please enter correct business hours')

            if weekday.get('from_hour') == None and weekday.get('to_hour') == None:
                weekday['is_dayoff'] = True

        try:
            PlaceOpeningHours.objects.bulk_update(
                weekdays_list, fields=['place', 'weekday', 'from_hour', 'to_hour', 'is_dayoff'])
        except Exception as e:
            raise UniqueConstraint

        updated_data = list(
            PlaceOpeningHours.objects.filter(place=place).values())
        return updated_data

    @staticmethod
    def create_dishes(place: Place, dto: CreateDishDTO) -> list[Dishes]:
        """Method for create dish in db"""

        number_of_dishes_exists: int = Dishes.objects.filter(
            place=place).count()
        amount_of_dishes: int = len(dto.dishes) + number_of_dishes_exists

        if len(dto.dishes) > 5:
            raise MAxNumberOfDishes
        elif number_of_dishes_exists == 5:
            raise MAxNumberOfDishes(
                detail=f"You have already created max number of dishes for this location.")
        elif amount_of_dishes > 5:
            raise MAxNumberOfDishes(
                detail=f"You have already created {number_of_dishes_exists} dishes for this location. You can add max 5 dishes to one location.")

        dishes_list: List[Dishes] = [Dishes(name=dish.get('name'), image=dish.get(
            'image'), place=place) for dish in dto.dishes]
        objs = Dishes.objects.bulk_create(dishes_list)

        return objs

    @staticmethod
    def partial_update_dish(dish: Dishes, dto: UpdateDishesDTO) -> Dishes:
        """Method for update existing dishes"""

        dish.update_fields(
            name=dto.name,  image=dto.image,
        )

        return dish

    @staticmethod
    def delete_dish(dish: Dishes) -> None:
        """Delete dish"""

        dish.delete()

    @staticmethod
    def get_all_places_of_some_owner(owner_id: int):
        """"Methos for get all places of some owner"""
        try:
            owner = CustomUser.objects.get(id=owner_id)
        except Exception as e:
            raise ObjectNotFoundError
        return Place.objects.select_related('owner').prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type').filter(owner=owner).exclude(is_deleted=True)

    # @staticmethod
    # def convert_inmemoryfile_to_base64(querydict) -> dict:
    #     """Method to convert InMemoryUploadFile into base64 string and put it into nested dict."""
    #     files: list = querydict.getlist('image')
    #     internal_data: list = []
    #     for file in files:
    #         image_dict: dict = {}
    #         file_contents = file.read()
    #         # Encode the file contents in base64 format
    #         base64_encoded: bytes = base64.b64encode(file_contents)
    #         decoded_file: str = base64_encoded.decode('utf-8')
    #         image_dict['image'] = decoded_file
    #         internal_data.append(image_dict)
    #     data: dict = {'images': internal_data}
    #     return data

    @staticmethod
    def convert_inmemoryfile_to_files(querydict) -> dict:
        """Method to convert InMemoryUploadFile into nested dict with files."""
        files = querydict.getlist('image')
        data = {'images': []}
        for file in files:
            data['images'].append({'image': file})
        return data

    @staticmethod
    def create_images(place: Place, dto: CreatePlaceImagesDTO) -> list[PlaceImages]:
        """Method for add images to db"""

        number_of_images_exists: int = PlaceImages.objects.filter(
            place=place).count()
        amount_of_images: int = len(dto.images) + number_of_images_exists

        if len(dto.images) > 9:
            raise MAxNumberOfImages
        elif number_of_images_exists == 9:
            raise MAxNumberOfImages(
                detail=f"You have already added max number of images for this location.")
        elif amount_of_images > 9:
            raise MAxNumberOfImages(
                detail=f"You have already added {number_of_images_exists} images for this location. You can add max 9 images to one location.")

        images_list: List[PlaceImages] = [PlaceImages(image=image.get(
            'image'), place=place) for image in dto.images]
        objs = PlaceImages.objects.bulk_create(images_list)

        return objs

    @staticmethod
    def delete_image(image: PlaceImages) -> None:
        """Delete image"""

        image.delete()

    @staticmethod
    def filter_places(dto: FilterPlaceDTO):
        """Method for filter places by date, time, type, cost"""

        type_ids: list[int] | None = dto.type_ids
        cost_from: str | None = dto.cost_from
        cost_to: str | None= dto.cost_to
        
        if type_ids:
            type_ids = [int(x) for x in dto.type_ids] if type_ids[0] != '' else []

        queryset = Place.objects.select_related('owner').prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type').all()

        if type_ids:
            queryset = queryset.filter(type__in=type_ids).distinct()

        if cost_from and cost_to:
            queryset = queryset.filter(deposit__range=(cost_from, cost_to))
        elif cost_from:
            queryset = queryset.filter(deposit__gte=cost_from)
        elif cost_to:
            queryset = queryset.filter(deposit__lte=cost_to)
        
        return queryset
