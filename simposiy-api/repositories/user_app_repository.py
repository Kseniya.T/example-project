from typing import Dict

from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist

from apps.user_app.dto import CreateUserInterestsDTO, ProfileDTO, UserDetailDTO
from utils.exceptions import NotFullFilledError, UniqueConstraint
from .repository import Repository
from apps.user_app.models import CustomUser, Profile, UserInterests


class UserAppRepository(Repository):
    """Repository of user_app application"""

    @staticmethod
    def partial_update_user(user: CustomUser, dto: UserDetailDTO) -> CustomUser:
        """Method for update existing user"""

        user.update_fields(email=dto.email, phone_number=dto.phone_number)
        return user

    @staticmethod
    def delete_user(user: CustomUser):
        """Method for remove user"""

        user.update_fields(is_deleted=True)


class ProfileAppRepository(Repository):
    """Repository of user_app application"""

    @staticmethod
    def is_personal_profile_fileds_filled(dto: ProfileDTO):
        if dto.user_name == None or dto.user_name == '':
            raise NotFullFilledError(
                detail=f'Fill requared data please: field "name" must be filled.')

    @staticmethod
    def is_corporate_profile_fileds_filled(dto: ProfileDTO):
        if dto.company_name == None or dto.company_legal_name == None or dto.company_address == None or dto.company_email == None or dto.company_ein == None or dto.company_business_type == None or  dto.company_business_website == None or dto.company_phone_number == None:
            raise NotFullFilledError(
                detail=f'Fill requared data please: company name, company legal name, address, email, phone number, ein number, business_website and business type.')
        if dto.company_name == '' or dto.company_legal_name == '' or dto.company_address == '' or dto.company_email == '' or dto.company_ein == '' or dto.company_business_type == '' or  dto.company_business_website == '' or  dto.company_phone_number == '':
            raise NotFullFilledError(
                detail=f'Fill requared data please: company name, company legal name, address, email, phone number, ein number, business_website and business type.')


    @staticmethod
    def create_personal_profile(user: CustomUser, dto: ProfileDTO) -> Profile:
        """Method for create personal profile in db, returns profile object """

        try:
            profile = Profile.objects.create(
                user=user, profile_type=dto.profile_type, user_name=dto.user_name, user_email=dto.user_email, user_social_media_link=dto.user_social_media_link , user_info=dto.user_info, user_image=dto.user_image,
                user_cover_image=dto.user_cover_image, is_verified=True, latitude=dto.latitude, longitude=dto.longitude, city=dto.city)
        except IntegrityError as e:
            raise UniqueConstraint(
                detail='Duplicate unique data. Duplicate value in one of unique field: "User email", "User nickname"')
        return profile

    @staticmethod
    def create_corporate_profile(user: CustomUser, dto: ProfileDTO) -> Profile:
        """Method for create corporate profile in db, returns profile object """

        try:
            profile = Profile.objects.create(user=user, profile_type=dto.profile_type, company_email=dto.company_email,
                                             company_name=dto.company_name, company_legal_name=dto.company_legal_name,
                                             company_ein=dto.company_ein, company_address=dto.company_address,
                                             company_business_website=dto.company_business_website,
                                             company_info=dto.company_info, company_image=dto.company_image,
                                             company_cover_image=dto.company_cover_image, company_business_type=dto.company_business_type,
                                             company_phone_number=dto.company_phone_number, latitude=dto.latitude,
                                             longitude=dto.longitude, city=dto.city)
        except IntegrityError:
            raise UniqueConstraint(
                detail="Duplicate unique data. Duplicate value in one of unique field: 'Company legal name', 'Company email', 'Company ein', 'Company bisiness website'")
        return profile

    @staticmethod
    def partial_update_personal_profile(profile: Profile, dto: ProfileDTO) -> Profile:
        """Method for update existing profile"""

        profile_id: int = profile.pk

        # if Profile.objects.filter(user_nickname=dto.user_nickname).exclude(id=profile_id).exists() and dto.user_nickname != None:
        #     raise UniqueConstraint(
        #         f"User with {dto.user_nickname}  nickname already exists")

        if Profile.objects.filter(user_email=dto.user_email).exclude(id=profile_id).exists() and dto.user_email != None:
            raise UniqueConstraint(
                f"User with {dto.user_email} email already exists")

        user_name = dto.user_name if dto.user_name else profile.user_name
        user_email = None if dto.user_email == '' else dto.user_email or profile.user_email
        user_info = None if dto.user_info  == '' else dto.user_info or profile.user_info
        user_social_media_link = None if dto.user_social_media_link == '' else dto.user_social_media_link or profile.user_social_media_link
        user_image = dto.user_image if dto.user_image else profile.user_image
        user_cover_image = dto.user_cover_image if dto.user_cover_image else profile.user_cover_image
        latitude=dto.latitude if dto.latitude else profile.latitude
        longitude=dto.longitude if dto.longitude else profile.longitude
        city=dto.city if dto.city else profile.city
        
        
        profile.update_fields(user_name=user_name, user_email=user_email, user_social_media_link=user_social_media_link, user_info=user_info, user_image=user_image,
                              user_cover_image=user_cover_image, latitude=latitude, longitude=longitude, city=city)
        return profile

    @staticmethod
    def partial_update_corporate_profile(profile: Profile, dto: ProfileDTO) -> Profile:
        """Method for update existing profile"""

        profile_id: int = profile.pk

        if Profile.objects.filter(company_legal_name=dto.company_legal_name).exclude(id=profile_id).exists() and dto.company_legal_name != None:
            raise UniqueConstraint(
                f"Company {dto.company_legal_name} already exists")

        if Profile.objects.filter(company_email=dto.company_email).exclude(id=profile_id).exists() and dto.company_email != None:
            raise UniqueConstraint(
                f"Company with {dto.company_email} email already exists")

        if Profile.objects.filter(company_ein=dto.company_ein).exclude(id=profile_id).exists() and dto.company_ein != None:
            raise UniqueConstraint(f"Such EIN number already registered")
        
        if Profile.objects.filter(company_business_website=dto.company_business_website).exclude(id=profile_id).exists() and dto.company_business_website != None:
            raise UniqueConstraint(f"Such business website already registered")

        company_email = dto.company_email if dto.company_email else profile.company_email
        company_name = dto.company_name if dto.company_name else profile.company_name
        company_legal_name = dto.company_legal_name if dto.company_legal_name else profile.company_legal_name
        company_business_type = profile.company_business_type
        company_ein = dto.company_ein if dto.company_ein else profile.company_ein
        company_address = dto.company_address if dto.company_address else profile.company_address
        company_business_website = dto.company_business_website if dto.company_business_website else profile.company_business_website
        company_info = None if dto.company_info == '' else dto.company_info or profile.company_info
        company_image = dto.company_image if dto.company_image else profile.company_image
        company_cover_image = dto.company_cover_image if dto.company_cover_image else profile.company_cover_image
        latitude=dto.latitude if dto.latitude else profile.latitude
        longitude=dto.longitude if dto.longitude else profile.longitude
        city=dto.city if dto.city else profile.city

        profile.update_fields(company_email=company_email, company_name=company_name, 
                              company_legal_name=company_legal_name, company_ein=company_ein,
                              company_address=company_address, company_business_website=company_business_website, 
                              company_business_type=company_business_type, company_info=company_info,
                              company_image=company_image, company_cover_image=company_cover_image,
                              latitude=latitude, longitude=longitude, city=city)
        return profile

    @staticmethod
    def create_user_interests(user: CustomUser, dto: CreateUserInterestsDTO) -> list[UserInterests]:
        """Method for create user interests"""

        iterests_list = [UserInterests(
            user=user, interests=interest) for interest in dto.interests]
        objs: list[UserInterests] = UserInterests.objects.bulk_create(
            iterests_list)

        return objs

    @staticmethod
    def delete_interests(user_interest: UserInterests) -> None:
        """Delete interests"""
        user_interest.delete()
    
    @staticmethod
    def delete_profile_images(user: CustomUser) -> None:
        """Method for delete profile image and/or cover_image"""

        profile = Profile.objects.get(user=user)
        
        if profile.profile_type == 'Personal':
            profile.user_image = None
            profile.save()
        
        elif profile.profile_type == 'Corporate':
            profile.company_image = None
            profile.save()

    @staticmethod
    def delete_profile_cover_images(user: CustomUser) -> None:
        """Method for delete profile image and/or cover_image"""

        profile = Profile.objects.get(user=user)
        
        if profile.profile_type == 'Personal':
            profile.user_cover_image = None
            profile.save()
        
        elif profile.profile_type == 'Corporate':
            profile.company_cover_image = None
            profile.save()
