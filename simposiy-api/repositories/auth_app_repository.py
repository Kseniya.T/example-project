import jwt
from typing import Optional

from django.utils import timezone
from django.db import transaction
from sentry_sdk import capture_exception, capture_message

from apps.auth_app.models import PushToken, Session
from apps.user_app.models import CustomUser
from apps.auth_app.dto import PushTokenDTO, UserAuthenticationDTO, UserResponseAuthDTO, RawUserRegistrationDTO
from core.settings import CAN_SEND_SMS, OTP_SMS_TTL
from utils.dto import UserDTO
from utils.sms_sender import send_sms
from .repository import Repository
from utils.exceptions import (OTPAlreadySentException, OTPExpiredError, OTPNotValidError, SessionNotFoundError,
                              UserDeletedError, UserNotFoundError, BlockedSessionError, DeactivatedSessionError, UserDeactivatedError)
from utils.otp import OTPCheckResultDTO, OTP, OTPInCacheDTO


class AuthAppRepository(Repository):
    """Repository of auth_app application"""

    @staticmethod
    def authenticate_user(user_auth_dto: UserAuthenticationDTO) -> UserDTO:
        user_dto: UserDTO = __class__.get_user_and_session(
            phone_number=user_auth_dto.phone_number,
            device_agent_id=user_auth_dto.device_agent_id,
            device_agent_represent=user_auth_dto.device_agent_represent,
        )
        if not user_dto.user:
            capture_message(
                'Account not found error', level='error')
            raise UserNotFoundError('Account not found')

        # checking user
        __class__.validate_user(user=user_dto.user)

        # checking user session
        __class__.validate_session(session=user_dto.session)

        # checking user OTP
        checkout_result: OTPCheckResultDTO = __class__.checkout_code(
            phone_number=user_auth_dto.phone_number,
            session_pk=user_dto.session.pk,
            otp_code=user_auth_dto.code,
        )

        if checkout_result.is_expired:
            raise OTPExpiredError
        elif not checkout_result.is_valid:
            raise OTPNotValidError

        # after authorization, confirm the session
        user_dto.session.confirm()
        return user_dto

    @staticmethod
    def validate_user(user: CustomUser) -> bool:
        if not user.is_active:
            capture_message('User deactivated error', level='error')
            raise UserDeactivatedError
        return True

    @staticmethod
    def validate_session(session: Session) -> bool:
        if session.is_deactivated:
            capture_message('Session deactivated error', level='error')
            raise DeactivatedSessionError

        if session.is_blocked:
            if not session.unlock_date < timezone.now():
                capture_message('Session locked error', level='error')
                raise BlockedSessionError(unlock_dt=session.unlock_date,)

            # If the unlock date is less than the current date, unlock the session
            session.activate()
        return True

    @staticmethod
    def get_user_and_session(phone_number: str, device_agent_id: str, device_agent_represent: str) -> UserDTO:
        """Method for getting the user and his session"""
        try:
            user = CustomUser.objects.get(phone_number=phone_number)
        except CustomUser.DoesNotExist as e:
            # capture_exception(e)
            return UserDTO(user=None, session=None)

        session_filter_kwargs = dict(
            user=user,
            device_agent_id=device_agent_id,
            device_agent_represent=device_agent_represent,
            is_deactivated=False,
        )

        session, created = Session.objects.get_or_create(
            **session_filter_kwargs)

        if not created and session.is_confirmed:
            # deactivate the previously created session and create a new one
            session.deactivate()
            session = Session.objects.create(**session_filter_kwargs)
        return UserDTO(user=user, session=session)

    @staticmethod
    def get_user_and_session_by_id(user_id: Optional[int], session_id: Optional[int]) -> UserDTO:
        """Method for getting user and session by id"""
        user = CustomUser.objects.filter(pk=user_id).last()
        if not user:
            capture_message('User is not found error', level='error')
            raise UserNotFoundError
        session = Session.objects.filter(pk=session_id, user_id=user_id).last()
        if not session:
            capture_message('Session is not found error', level='error')
            raise SessionNotFoundError
        return UserDTO(user=user, session=session)

    @staticmethod
    def generate_otp(phone_number: str, session_pk: int) -> OTP:
        """Returned already generated otp"""
        otp = OTP(phone_number=phone_number,
                  session_pk=session_pk, expiration=OTP_SMS_TTL)
        otp.generate_otp()
        return otp

    @staticmethod
    def sign_in_user(dto: RawUserRegistrationDTO) -> UserResponseAuthDTO:
        """
            Searching user by dto data
                If exists -> raise exception UserAlreadyRegistred
            Creating user by dto data
            Generating otp
            Returning otp key with HTTP_201_CREATED response
        """

        with transaction.atomic():
            user_dto: UserDTO = __class__.get_user_and_session(
                phone_number=dto.phone_number,
                device_agent_id=dto.device_agent_id,
                device_agent_represent=dto.device_agent_represent,
            )

            # If the user is not in the system
            if not user_dto.user:
                created_user_dto = __class__.create_user_and_session(
                    registration_dto=dto)
                otp: OTP = __class__.generate_otp(
                    phone_number=dto.phone_number,
                    session_pk=created_user_dto.session.pk)

                __class__.send_otp_to_phone_number(
                    phone_number=dto.phone_number, otp_code=otp.otp_code)
                return UserResponseAuthDTO(
                    otp_code=otp.otp_code if not CAN_SEND_SMS else None,
                    resend_by=otp.datetime_resend,
                    resend_by_seconds=(otp.datetime_resend -
                                       timezone.now()).seconds,
                    timeout_resend=OTP_SMS_TTL,
                    new_user=True
                )

            # If user already registered but deleted
            if user_dto.user and user_dto.user.is_deleted == True:
                raise UserDeletedError

            # If user already registered
            if user_dto.user and user_dto.user.is_deleted == False:
                otp_in_cache_dto: OTPInCacheDTO = OTP.find_otp_in_cache(
                    session_pk=user_dto.session.pk,
                    phone_number=dto.phone_number,
                )
                if otp_in_cache_dto.is_exists:
                    raise OTPAlreadySentException(
                        obj=UserResponseAuthDTO(
                            resend_by=otp_in_cache_dto.expired_at,
                            resend_by_seconds=(
                                otp_in_cache_dto.expired_at - timezone.now()).seconds,
                            timeout_resend=OTP_SMS_TTL, new_user=False
                        ).get_dict()
                    )

                # If OTP is not exists in Cache
                otp: OTP = __class__.generate_otp(
                    phone_number=dto.phone_number,
                    session_pk=user_dto.session.pk)
                __class__.send_otp_to_phone_number(
                    phone_number=dto.phone_number, otp_code=otp.otp_code)

                return UserResponseAuthDTO(
                    otp_code=otp.otp_code if not CAN_SEND_SMS else None,
                    resend_by=otp.datetime_resend,
                    resend_by_seconds=(otp.datetime_resend -
                                       timezone.now()).seconds,
                    timeout_resend=OTP_SMS_TTL, new_user=False
                )

    @staticmethod
    def create_user_and_session(registration_dto: RawUserRegistrationDTO):
        """Create user and session"""

        with transaction.atomic():
            user = CustomUser.objects.create_user(
                phone_number=registration_dto.phone_number)
            session = Session.objects.create(user=user, device_agent_id=registration_dto.device_agent_id,
                                             device_agent_represent=registration_dto.device_agent_represent)

            return UserDTO(user=user, session=session)

    @staticmethod
    def send_otp_to_phone_number(phone_number: str, otp_code: str):
        MESSAGE: str = f'Your one-time login code {otp_code}'
        return send_sms(phone_number=phone_number, message=MESSAGE)

    @staticmethod
    def checkout_code(phone_number: str, session_pk: int, otp_code: str) -> OTPCheckResultDTO:
        """Checking the code for validity"""
        otp = OTP(phone_number=phone_number,
                  session_pk=session_pk, expiration=OTP_SMS_TTL)
        result = otp.verify(user_otp=otp_code)
        return result

    @staticmethod
    def deactivate_user_sessions(user: CustomUser):
        """Deactivating all user's sessions"""
        Session.objects.filter(user=user).update(
            is_deactivated=True)

    @staticmethod
    def activate_user_sessions(user: CustomUser):
        """Deactivating all user's sessions"""
        Session.objects.filter(user=user).update(
            is_deactivated=False)

    @staticmethod
    def create_push_token(user: CustomUser, session: Session, dto: PushTokenDTO) -> PushToken:
        """Method for creating push token"""
        return PushToken.objects.create(user=user, session=session, token=dto.push_token)
    
    @staticmethod
    def remove_push_tokens(push_token: str) -> None:
        """Method for removing push token"""
        PushToken.objects.filter(token=push_token).delete()
