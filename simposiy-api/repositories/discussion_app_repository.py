import datetime
import random
import time

from django.utils import timezone
from django.db.models import Q, Count
from django.core.exceptions import ObjectDoesNotExist
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from apps.notification_app.models import ApplicationConfig

from apps.payment_app.dto import DiscussionAuthorizePaymentDTO
from apps.payment_app.models import Order
from apps.user_app.models import CustomUser, Profile
from services.stripe_service.dto import CreateCheckoutSessionDTO
from services.stripe_service.stripe_service import StripeService
from utils.exceptions import (AcceptDenyDiscussionError, DiscussionIsFullError, DiscussionIsOver,
                              DiscussionUpdateError, LimitNumberOfSeatsPurchased,
                              ObjectNotFoundError, ParticipantWithoutPayment, PermissionDeniedError,
                              SeleniumWebdriverError, WrongDiscussionDate)
from .repository import Repository
from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion, UserThemes
from apps.discussion_app.dto import (AutoPaymentDTO, CreateDiscussionDTO,
                                     CreateUserThemesDTO, FilterDiscussionsDTO,
                                     RetrieveDiscussionDTO, UpdateDiscussionDTO,
                                     PaidDetailsDTO)
from apps.place_app.models import PlaceOpeningHours


class DiscussionAppRepository(Repository):
    """Repository for discussion_app"""

    @staticmethod
    def checks_before_create_discussion(initiator: CustomUser, discussion_dto: CreateDiscussionDTO) -> bool:
        """Methos for checks conditions before create new discussion"""

        # Check if initiator has profile
        profile: Profile = Profile.objects.filter(
            user=initiator).exists()
        if not profile:
            raise ObjectNotFoundError(
                "Please fill out your profile to create discussion")

        # Restrict to create discussion later than 14 days or for passed date or left less than 4 hr befor it starts
        now = timezone.make_naive(timezone.now())
        discussion_start_date: datetime.datetime = discussion_dto.dt_start.replace(
            tzinfo=None)
        difference: datetime.timedelta = discussion_start_date - now
        days, hours, minutes = difference.days, difference.seconds // 3600, difference.seconds % 3600 // 60

        # temporary outcomment for tests: or days > 14 or (days == 0 and hours < 24):
        if now > discussion_start_date:
            raise WrongDiscussionDate(
                detail='You can create a discussion no earlier than 24 hours and no later than 2 weeks from the start date')

        # Check if creating discussion fit into place opening hours
        discussion_weekday: int = discussion_start_date.isoweekday()
        discussion_time = discussion_start_date.time()
        try:
            place_opening_hours: PlaceOpeningHours = PlaceOpeningHours.objects.get(
                place=discussion_dto.place, weekday=discussion_weekday)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError(
                'The selected place for discussion does not have opening hours')

        from_hour = place_opening_hours.from_hour
        to_hour = place_opening_hours.to_hour

        if from_hour.hour == 0 and from_hour.minute == 0 and to_hour.hour == 0 and to_hour.minute == 0:
            """Case when place open 24 hr"""
            pass
        elif not from_hour and not to_hour or from_hour > discussion_time or discussion_time > to_hour:
            raise WrongDiscussionDate(
                detail=f'The selected time of the discussion does not coincide with the opening hours of the "{discussion_dto.place.name}"')

        initiator_is_place_owner = True if initiator == discussion_dto.place.owner else False

        return initiator_is_place_owner

    @staticmethod
    def create_discussion(initiator: CustomUser, dto: CreateDiscussionDTO, initiator_is_place_owner: bool) -> Discussion:
        """Method for creating fields of Discussion model"""

        app_config: ApplicationConfig = ApplicationConfig.objects.last()
        service_fee = app_config.app_service_fee

        if initiator_is_place_owner:
            new_discussion: Discussion = Discussion.objects.create(initiator=initiator, place=dto.place, type=dto.type,
                                                                   category=dto.category, theme=dto.theme, topic=dto.topic, description=dto.description,
                                                                   dt_start=dto.dt_start,  number_of_participants=dto.number_of_participants, image=dto.image,
                                                                   service_fee=service_fee, is_active=True, paid=True, holding_agreed=True, on_review=False)
            DiscussionAppRepository.create_participant_of_discussion(
                user=initiator, discussion=new_discussion, is_initiator=True, was_paid=True)
            return new_discussion

        new_discussion: Discussion = Discussion.objects.create(initiator=initiator, place=dto.place, type=dto.type,
                                                               category=dto.category, theme=dto.theme, topic=dto.topic, description=dto.description,
                                                               dt_start=dto.dt_start,  number_of_participants=dto.number_of_participants, image=dto.image,
                                                               service_fee=service_fee
                                                               )
        DiscussionAppRepository.create_participant_of_discussion(
            user=initiator, discussion=new_discussion, is_initiator=True, was_paid=False)
        return new_discussion

    @staticmethod
    def check_initiator(initiator: CustomUser, discussion: Discussion) -> bool:
        """Method for check if initiator of discussion is also place owner"""

        if initiator == discussion.place.owner:
            return True
        return False

    @staticmethod
    def check_discussion_for_update(user: CustomUser, discussion: Discussion):
        """Method to check if user is initiator of discussion. And if he is, check if discussion not active and not paid"""

        now = timezone.now()
        discussion_updated_at = discussion.updated_at
        time_diff = now - discussion_updated_at

        if user != discussion.initiator:
            raise DiscussionUpdateError(
                detail='Only the initiator can edit their discussions.')

        elif time_diff.days < 1 and (not discussion.holding_agreed and discussion.on_review):
            raise DiscussionUpdateError(
                detail='You can not update discussion wich is on review')

        elif user == discussion.place.owner:
            raise DiscussionUpdateError

        elif discussion.is_active or discussion.holding_agreed:
            raise DiscussionUpdateError(
                detail='Active discussions or discussion wich were approved by place owner are uneditable.')

        elif now > discussion.dt_start:
            raise DiscussionUpdateError(
                detail='Complited discussions can not be edited.')

    @staticmethod
    def partial_update_discussion(discussion: Discussion, dto: UpdateDiscussionDTO) -> tuple[Discussion, bool]:
        """Method for partial update of discussion"""

        place_updated = False
        if dto.place != None:
            place_updated = True

        theme = dto.theme if dto.theme != None else discussion.theme
        topic = dto.topic if dto.topic != None else discussion.topic
        description = dto.description if dto.description != None else discussion.description
        image = dto.image if dto.image != None else discussion.image
        place = dto.place if dto.place != None else discussion.place
        dt_start = dto.dt_start if dto.dt_start != None else discussion.dt_start

        discussion.update_fields(theme=theme,
                                 topic=topic,
                                 description=description,
                                 image=image,
                                 place=place,
                                 dt_start=dt_start,
                                 )
        return discussion, place_updated

    @staticmethod
    def create_participant_of_discussion(user: CustomUser, discussion: Discussion, is_initiator: bool, was_paid: bool) -> ParticipantsOfDiscussion:
        """Method for creating fields of ParticipantsOfDiscussion model.
            If user is initiator but did't pay yet, we add him as participants with number_of_paid_seats=0 and initiator=True.
            If user is initiator and paid for place in his own discussion so we add him as usual participant with flag is_initiator=True.
            If user not initiator we juat add him as participant after payment.
            If user not initiator and didn't pay(for test cases only) raise exception.
        """

        if is_initiator and was_paid:
            return ParticipantsOfDiscussion.objects.create(user=user, discussion=discussion, is_initiator=True)
        if is_initiator and not was_paid:
            return ParticipantsOfDiscussion.objects.create(user=user, discussion=discussion, is_initiator=True, number_of_paid_seats=0)
        if not is_initiator and not was_paid:
            raise ParticipantWithoutPayment
        return ParticipantsOfDiscussion.objects.create(user=user, discussion=discussion)

    @staticmethod
    def get_discussion_queryset(user: CustomUser, wide_filter: bool):

        if wide_filter:
            discussions_id_queryset = ParticipantsOfDiscussion.objects.select_related(
                'discussion').filter(user=user).values('discussion')
            result = (
                Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                    'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
                .select_related('place', 'initiator', 'type', 'category', 'theme')
                .filter(id__in=discussions_id_queryset)
                .annotate(confirmed_participant_number=Count('participant_of_discussion')))
            return result

        discussions_id_queryset = ParticipantsOfDiscussion.objects.select_related(
            'discussion').filter(user=user, is_initiator=False, number_of_paid_seats=1).values('discussion')
        result = (
            Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
            .select_related('place', 'initiator', 'type', 'category', 'theme')
            .filter(id__in=discussions_id_queryset)
            .annotate(confirmed_participant_number=Count('participant_of_discussion')))
        # Sum(F('participant_of_discussion__number_of_paid_seats'))))

        return result

    @staticmethod
    def get_active_tickets_queryset(user: CustomUser):
        """Methos retrun active tickets"""
        discussions_id_list: list[int] = ParticipantsOfDiscussion.objects.select_related(
            'discussion').filter(user=user, number_of_paid_seats=1).values_list('discussion', flat=True)

        result = (
            Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
            .select_related('place', 'initiator', 'type', 'category', 'theme')
            .filter(Q(id__in=discussions_id_list) & Q(is_active=True))
            .annotate(confirmed_participant_number=Count('participant_of_discussion')))
        return result

    @staticmethod
    def get_past_tickets_queryset(user: CustomUser):
        """Method returns past tickets"""

        discussions_id_list: list[int] = ParticipantsOfDiscussion.objects.select_related(
            'discussion').filter(user=user, number_of_paid_seats=1).values_list('discussion', flat=True)
        result = (
            Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile', 'initiator__user_interests__interests',
                                                'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
            .select_related('place', 'initiator', 'type', 'category', 'theme')
            .filter(Q(id__in=discussions_id_list) & Q(is_active=False))
            .annotate(confirmed_participant_number=Count('participant_of_discussion')))
        return result

    @staticmethod
    def retrieve_discussion(pk: int, user: CustomUser | None, filter_time: datetime) -> RetrieveDiscussionDTO:
        """Method for retrieve discussion and return DTO with calculated fields"""

        paid_detail_dto: PaidDetailsDTO = None
        payment_link: str = None
        order: Order = None

        try:
            discussion: Discussion = Discussion.objects.select_related(
                'place', 'initiator', 'type', 'category', 'theme'
            ).prefetch_related('initiator__user_interests', 'initiator__profile', 'initiator__user_interests__interests',
                               'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes'
                               ).annotate(
                confirmed_participants_number=Count(
                    'participant_of_discussion')
            ).get(pk=pk)

        except ObjectDoesNotExist:
            raise ObjectNotFoundError

        participants_of_discussion_queryset = discussion.participant_of_discussion.all()

        if user:
            try:
                # в выборку попадет один заказ 'paid' либо без статуса не подтвержденный(т.к. по дной дискуссии больше быть не может, только в статусе 'unpaid')
                order = Order.objects.select_related('charge').filter(user=user, discussion=discussion).exclude(
                    payment_status='unpaid').first()
            except ObjectDoesNotExist:
                pass

        # payment link for orders without status
        if order and order.created_at >= filter_time and order.payment_status == None:
            payment_link = order.payment_link

        # paid details
        elif order and order.payment_status == 'paid':
            pdf_url = None
            if order.pdf_check:
                pdf_url = order.pdf_check.url
            paid_detail_dto: PaidDetailsDTO = PaidDetailsDTO(check_number=order.order_representation_id,
                                                             payment_status=order.payment_status,
                                                             payment_time=order.updated_at,
                                                             total_payment=order.charge.amount_captured,
                                                             pdf=pdf_url)

        return RetrieveDiscussionDTO(id=discussion.id,
                                     type=discussion.type,
                                     topic=discussion.topic,
                                     theme=discussion.theme,
                                     created_at=discussion.created_at,
                                     description=discussion.description,
                                     place=discussion.place,
                                     dt_start=discussion.dt_start,
                                     category=discussion.category,
                                     number_of_participants=discussion.number_of_participants,
                                     is_active=discussion.is_active,
                                     is_full=discussion.is_full,
                                     paid=discussion.paid,
                                     on_review=discussion.on_review,
                                     holding_agreed=discussion.holding_agreed,
                                     initiator=discussion.initiator,
                                     confirmed_participants_number=discussion.confirmed_participants_number,
                                     participant_of_discussion=participants_of_discussion_queryset,
                                     image=discussion.image,
                                     payment_link=payment_link,
                                     paid_details=paid_detail_dto)

    @staticmethod
    def discussion_check(user: CustomUser, discussion_dto: DiscussionAuthorizePaymentDTO) -> tuple[int, bool]:
        """Method for check if discussion is valid for payment"""

        discussion_id: int = discussion_dto.discussion.pk
        discussion = Discussion.objects.get(id=discussion_id)

        # Check if date of discussion has already passed
        if discussion.dt_start.replace(tzinfo=None) < timezone.make_naive(timezone.now()):
            discussion.update_fields(is_active=False)
            raise DiscussionIsOver

        # Check if user who pay is initiator of discussion
        try:
            participant = ParticipantsOfDiscussion.objects.get(
                user=user, discussion=discussion_dto.discussion)
        except ObjectDoesNotExist:
            participant = None

        is_initiator: bool = False
        if user == discussion.initiator:
            is_initiator: bool = True

        if participant:

            if is_initiator and participant.number_of_paid_seats == 1:
                raise LimitNumberOfSeatsPurchased

            # Check if user is initiator without payment but with active payment link
            try:
                order = Order.objects.get(
                    user=user, discussion=discussion, payment_status=None)
            except ObjectDoesNotExist:
                pass
            else:
                if is_initiator and participant.number_of_paid_seats == 0 and order:
                    raise LimitNumberOfSeatsPurchased(
                        detail='You already took link for payment. You can find it in ...')

        # Check if discussion didn't pass but still inactive and user who pay is not initiator raise exception
        if discussion.is_active == False and not is_initiator:
            raise DiscussionIsOver(detail='This discussion not active yet')

        # Check if discussion is full with participants
        if discussion_dto.discussion.is_full == True:
            raise DiscussionIsFullError

        # Checking if this user has already purchased a seat in this discussion
        participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.filter(
            discussion_id=discussion_id, user=user).exists()
        if participant and not is_initiator:
            raise LimitNumberOfSeatsPurchased

        # Check if user has profile
        profile: Profile = Profile.objects.filter(user_id=user.id).exists()
        if not profile:
            raise ObjectNotFoundError(
                "Please fill out your profile to participate in the discussion")

        # Checking the number of seats a user buys
        confirmed_participants: int = ParticipantsOfDiscussion.objects.filter(
            discussion_id=discussion_id).count()
        number_of_available_seats: int = discussion_dto.discussion.number_of_participants - \
            confirmed_participants

        if number_of_available_seats == 0:
            Discussion.objects.filter(id=discussion_id).update(is_full=True)
            raise DiscussionIsFullError

        return number_of_available_seats, is_initiator

    @staticmethod
    def remove_user_from_participants(user: CustomUser, discussion: Discussion):
        """Method for remove user from participants of particular discussion"""

        participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.filter(
            user=user, discussion=discussion)
        participant.delete()

    @staticmethod
    def imitate_payment_with_selenium(dto: AutoPaymentDTO, user: CustomUser, discussion: Discussion, is_initiator: bool) -> bool | None:
        """Method for auto payment for create test participant"""

        amount: int = int(dto.discussion.place.deposit.amount*100)
        name: str = dto.discussion.topic

        # Saving data in Order model, add data to CreateCheckoutSessionDTO and create checkout session
        order: Order = Order.objects.create(user=user, discussion=discussion)
        order_representation_id: str = order.order_representation_id

        checkout_session_dto: CreateCheckoutSessionDTO = StripeService.create_checkout_session_dto(
            amount=amount, name=name, dicsussion_id=discussion.id, user_id=user.id, order_representation_id=order_representation_id, is_initiator=is_initiator)

        checkout_session_object = StripeService.create_checkout_session(
            dto=checkout_session_dto)
        order.update_fields(
            checkout_session_id=checkout_session_object.stripe_id)

        try:
            user_name = user.profile.user_name.replace(" ", "").lower()
            company_name = None
        except Exception:
            company_name = user.profile.company_name.replace(" ", "").lower()
            user_name = None

        # settings for SElenium webdriver for Windows/Chrome
        options = webdriver.ChromeOptions()
        options.add_experimental_option("detach", True)

        driver = webdriver.Chrome(service=ChromeService(
            ChromeDriverManager().install()), options=options)

        # open window in chrome browser with Stripe payment page
        driver.get(checkout_session_object.url)
        time.sleep(5)  # sleep for 5 sec to load all fields

        # Filling fields on payment page
        try:
            email_field = driver.find_element(By.NAME, 'email')
            if user_name:
                email_field.send_keys(f'{user_name}'+'@gmail.com')
            if company_name:
                email_field.send_keys(f'{company_name}'+'@gmail.com')

            card_number_field = driver.find_element(By.NAME, 'cardNumber')
            card_number_field.send_keys('4242424242424242')

            card_expiration_field = driver.find_element(By.NAME, 'cardExpiry')
            card_expiration_field.send_keys(
                f'{random.randrange(1, 13)}/{random.randrange(25, 35)}')

            card_cvc_field = driver.find_element(By.NAME, 'cardCvc')
            card_cvc_field.send_keys(f'{random.randrange(100, 1000)}')

            name_on_card_field = driver.find_element(By.NAME, 'billingName')
            if user_name:
                name_on_card_field.send_keys(f'{user.profile.user_name}')
            if company_name:
                name_on_card_field.send_keys(f'{user.profile.company_name}')

            # After click button we pay for order and get webhooks for create participant
            driver.find_element(By.CSS_SELECTOR, '.SubmitButton').click()
        except Exception as e:
            # If sth  went wrong we remove created order
            order.delete()
            # close window with payment
            driver.quit()
            raise SeleniumWebdriverError

        time.sleep(13)  # sleep for 15 sec to finish payment
        driver.quit()
        return order_representation_id

    @staticmethod
    def create_user_themes(user: CustomUser, dto: CreateUserThemesDTO) -> list[UserThemes]:
        """Method for create user themes"""

        themes_list = [UserThemes(user=user, themes=themes)
                       for themes in dto.themes]
        objs: list[UserThemes] = UserThemes.objects.bulk_create(themes_list)

        return objs

    @staticmethod
    def delete_user_themes(user_themes: UserThemes) -> None:
        """Delete user themes"""
        user_themes.delete()

    @staticmethod
    def filter_search_discussions(dto: FilterDiscussionsDTO):
        """Method for filter discussions by date, time, type, category, theme  or cost"""

        # search
        search: str | None = dto.search

        if search:
            queryset = (Discussion.objects
                        .prefetch_related('initiator__user_interests', 'initiator__profile',
                                          'participant_of_discussion',
                                          'participant_of_discussion__user__profile',
                                          'place__type', 'place__dishes')
                        .select_related('place', 'type', 'category', 'theme')
                        .filter(Q(topic__icontains=search) & Q(is_active=True)))
            return queryset

        # filtration
        date_from: str | None = dto.date_from
        date_to: str | None = dto.date_to
        time_from: str | None = dto.time_from
        time_to: str | None = dto.time_to
        type_ids: str | None = dto.type_ids
        category_ids: str | None = dto.category_ids
        theme_ids: str | None = dto.theme_ids
        cost_from: str | None = dto.cost_from
        cost_to: str | None = dto.cost_to

        if type_ids:
            type_ids = [int(x)
                        for x in dto.type_ids] if type_ids[0] != '' else []

        if category_ids:
            category_ids = [
                int(x) for x in dto.category_ids] if category_ids[0] != '' else []

        if theme_ids:
            theme_ids = [int(x)
                         for x in dto.theme_ids] if theme_ids[0] != '' else []

        queryset = Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                       'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes').select_related(
            'place', 'type', 'category', 'theme').filter(is_active=True)

        if date_from and date_to:
            queryset = queryset.filter(
                Q(dt_start__date__gte=date_from) & Q(dt_start__date__lte=date_to))
        elif date_from:
            queryset = queryset.filter(dt_start__date__gte=date_from)
        elif date_to:
            queryset = queryset.filter(dt_start__date__lte=date_to)

        if time_from and time_to:
            queryset = queryset.filter(
                Q(dt_start__time__gte=time_from) & Q(dt_start__time__lte=time_to))
        elif time_from:
            queryset = queryset.filter(dt_start__time__gte=time_from)
        elif time_to:
            queryset = queryset.filter(dt_start__time__lte=time_to)

        if type_ids:
            queryset = queryset.filter(type__id__in=type_ids)

        if category_ids:
            queryset = queryset.filter(category__id__in=category_ids)

        if theme_ids:
            queryset = queryset.filter(theme__id__in=theme_ids)

        if cost_from and cost_to:
            queryset = queryset.filter(
                place__deposit__range=(cost_from, cost_to))
        elif cost_from:
            queryset = queryset.filter(place__deposit__gte=cost_from)
        elif cost_to:
            queryset = queryset.filter(place__deposit__lte=cost_to)
        return queryset

    @staticmethod
    def activate_discussion(id: int) -> Discussion:
        """Method for activate discussion after it host were approved by place owner"""

        discussion = Discussion.objects.select_related('place').get(id=id)
        discussion.update_fields(holding_agreed=True, on_review=False)

        return discussion

    @staticmethod
    def check_discussion_place_owner(id: int, user: CustomUser) -> Discussion:
        """
            Method for check if auth user who accept/deny host request is discussion place owner
        """
        discussion = Discussion.objects.select_related('place').get(id=id)
        place_owner = discussion.place.owner

        if user != place_owner:
            raise PermissionDeniedError(
                detail='Only place owner specified in discussion can perfom this action')
        return discussion

    @staticmethod
    def check_before_accept_deny(discussion: Discussion):
        """Method to check if discussion before accept or deny to host it"""

        now = timezone.now()

        discussion_created_at = discussion.created_at
        time_passed = now - discussion_created_at

        if time_passed.days >= 1:
            raise AcceptDenyDiscussionError(
                detail='The time to decide whether to hold a discussion in your location has expired.')

        elif discussion.holding_agreed:
            raise AcceptDenyDiscussionError

        elif not discussion.holding_agreed and not discussion.on_review:
            raise AcceptDenyDiscussionError

    # @staticmethod
    # def check_before_refund(user: CustomUser, discussion_pk: int) -> tuple[ChargeData, Discussion] | None:
    #     """Method for check user and discussion when user request refund and return charge_id if check was succesfull"""

    #     discussion: Discussion = Discussion.objects.get(id=discussion_pk)

    #     # Check if user is participant of discussion with paid seat
    #     is_participant: bool = ParticipantsOfDiscussion.objects.filter(
    #         user=user, discussion=discussion, number_of_paid_seats=1).exists()
    #     if not is_participant:
    #         raise RefundError(
    #             detail='Only participant of discussion can request for refund')

    #     # Check discussion(is active, dt_start >= 24 hr)
    #     is_active = discussion.is_active
    #     if is_active and is_participant:
    #         now = timezone.make_naive(timezone.now())
    #         discussion_start_date: datetime.datetime = discussion.dt_start.replace(
    #             tzinfo=None)
    #         difference: datetime.timedelta = discussion_start_date - now
    #         days, hours = difference.days, difference.seconds // 3600

    #         if days < 1:
    #             raise RefundError(
    #                 detail=f'{hours} hours left before the start of the discussion. A request for a refund is accepted no later than 24 hours before the start of the discussion.')

    #         order: Order = Order.objects.get(user=user, discussion=discussion)
    #         charge_data: ChargeData = ChargeData.objects.get(
    #             payment_intent_id=order.payment_intent_id)

    #         return charge_data, discussion
