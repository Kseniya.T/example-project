import datetime

from django.db.models import Q
from sentry_sdk import capture_message, set_context
import stripe
from django.core.exceptions import ObjectDoesNotExist

from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion
from apps.payment_app.dto import ReservationCalculationDTO
from apps.payment_app.models import ChargeData, Order
from apps.place_app.models import Place
from apps.user_app.models import CustomUser
from core.settings import STRIPE_SECRET_KEY
from repositories.discussion_app_repository import DiscussionAppRepository
from utils.exceptions import ObjectNotFoundError
from django.utils import timezone


class PaymentAppRepository():
    """Repository for payment_app"""
    
    def get_reservation_calculation_dto(pk, payment_url: str, checkout_session_id: str, number_of_available_seats: int) -> ReservationCalculationDTO:
        """Method returns ReservationCalculationDTO"""
        
        discussion = Discussion.objects.select_related('place').get(id=pk)

        place: Place = discussion.place
        payment_later: str = discussion.place.deposit

        # calculating time before the start of the discussion
        difference: datetime.timedelta = discussion.dt_start.replace(
            tzinfo=None) - timezone.make_naive(timezone.now())
        days, hours, minutes = difference.days, difference.seconds // 3600, difference.seconds % 3600//60
        seconds = difference.seconds - hours*3600 - minutes*60
        time_until_the_end: str = f'Days:{days}, Hours:{hours}, Min:{minutes}, Sec:{seconds}'       

        return ReservationCalculationDTO(
            discussion=discussion,
            place=place,
            number_of_available_seats=number_of_available_seats,
            time_until_the_end=time_until_the_end,
            payment_later=payment_later,
            reservation=payment_url,
            checkout_session_id=checkout_session_id
        )
    
    @staticmethod
    def check_user_orders(user: CustomUser, filter_time: datetime) -> bool:
        """Method for check user orders by request Stripe server"""
        
        orders = (Order.objects.select_related('user', 'discussion')
                  .filter(Q(user=user) & Q(is_confirmed=False) & Q(checkout_session_id__isnull=False))
                  )
        if not orders:
            return False
        for order in orders:
            try:
                stripe_checkout_session = stripe.checkout.Session.retrieve(
                    api_key=STRIPE_SECRET_KEY, id=order.checkout_session_id,  expand=['customer_details'])
            except Exception as e:
                set_context('retrieve_checkout_session_case', value=e.__dict__)
                capture_message(
                    'Retrieve checkout session error', level='error')
                raise ObjectNotFoundError(detail=f'Order N {order.id} has checkout seesion id {order.checkout_session_id} wich can not be fount in Stripe')

            if stripe_checkout_session.payment_status == 'paid' and order.created_at >= filter_time:
                user_email: str = stripe_checkout_session.customer_details.email
                try:
                    charge: ChargeData = ChargeData.objects.get(
                        payment_intent_id=stripe_checkout_session.payment_intent)
                except ObjectDoesNotExist as e:
                    set_context('chargedata_is_not_found_case',
                                value=e.__dict__)
                    capture_message(
                        f'Charge data object wasn\'t created for order {order.pk}', level='error')
                    raise ObjectNotFoundError(
                        detail=f'Charge data object wasn\'t created for order {order.pk}')

                order.update_fields(
                    user_email=user_email,
                    charge_id=charge.pk,
                    payment_intent_id=stripe_checkout_session.payment_intent,
                    payment_status=stripe_checkout_session.payment_status,
                    is_confirmed=True,
                )

            if order.created_at < filter_time:
                # remove user from participants and update order
                order.update_fields(
                    payment_status=stripe_checkout_session.payment_status,
                )
                try:
                    unconfirmed_participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.get(
                        Q(user=user) & Q(discussion=order.discussion) & Q(number_of_paid_seats=0))
                except ObjectDoesNotExist as e:
                    set_context('participant_not_found_case', value=e.__dict__)
                    capture_message(
                        f'ParticipantOfDiscussion object wasn\'t found for order {order.pk}', level='error')
                    unconfirmed_participant = None

                if unconfirmed_participant and not unconfirmed_participant.is_initiator:
                    unconfirmed_participant.delete()
        return True
