import json
from typing import Any, List, Optional

from redis import StrictRedis
from sentry_sdk import capture_exception, capture_message, set_context

from apps.auth_app.models import PushToken
from apps.notification_app.models import Notification
from apps.user_app.models import CustomUser
from firebase.main import FirebaseMessage
from repositories.redis_repository import FirebaseRdbRepository
from repositories.repository import Repository


class PushNotificationRepository(Repository):
    _REDIS_TRANSACTION_KEY = 'tr'
    _REDIS_NOTIFICATION_KEY = 'n'
    _REDIS_USER_KEY = 'u'

    def __init__(self, rdb_repository: FirebaseRdbRepository) -> None:
        self.firebase_rdb_repository = rdb_repository

    @property
    def rdb(self) -> StrictRedis:
        return self.firebase_rdb_repository.rdb

    def _send_push_notification(self, title: str, message: str, token_list: List[str], data: dict[Any, Any]):
        """Method for sending firebase push notification"""
        firebase_message = FirebaseMessage(
            title=title, message=message,
            token_list=token_list, data=data,
        )
        try:
            firebase_message.send_push()
        except Exception as e:
            capture_exception(e)
            raise e

    def send_push_notifications(self, with_buttons):
        """
            Send push notifications
            Used in Celery task
        """

        # receiving all users, which need send push notifications
        user_pk_list: list = self._get_users_pk_list()
        notifications_pk_list: list[int] = []

        for user_pk in user_pk_list:
            push_tokens: List = self._get_user_push_tokens(pk=int(user_pk[2:]))
            # if not push_tokens:
            #     continue

            # receiving all data from rdb
            key_dict = self.rdb.hgetall(user_pk)
            for key, value in key_dict.items():
                data = json.loads(value)
                if with_buttons:
                    data["actions"] = [{"action": "accept",
                                        "title": "Accept"},
                                       {"action": "cancel",
                                        "title": "Cancel"}
                                       ]

                self.handle_key(
                    token_list=push_tokens,
                    notifications_pk_list=notifications_pk_list,
                    data=data,
                )

            # removing irrelevant push notifications data
            self.rdb.hdel(user_pk, *key_dict.keys())

        if notifications_pk_list:
            # Update data
            Notification.objects.select_for_update().filter(
                pk__in=notifications_pk_list
            ).update(is_sent=True)

    def handle_key(self, data, notifications_pk_list: list[int], token_list: List[str], ):
        """Handling NotificationType.info"""

        pk: int = data.get('id')
        title: str = data.get('title')
        body: str = data.get('body')
        payload: str = data.get('payload')
        notifications_pk_list.append(pk)
        self._send_push_notification(
            title=title,
            message=body,
            token_list=token_list,
            data=payload,
        )

    def add_notification_data_to_rdb(self, user_pk: int, notification: Notification) -> None:
        """Add notifications data to redis"""

        self.rdb.hset(
            name=f'{self._REDIS_USER_KEY}:{user_pk}',
            key=f'{self._REDIS_NOTIFICATION_KEY}:{notification.pk}',
            value=json.dumps(
                dict(
                    id=notification.pk,
                    title=notification.title,
                    body=notification.body,
                    payload=notification.payload,
                )
            )
        )

    def _get_users_pk_list(self):
        """Returned push notifications data before sending to Firebase"""

        return self.firebase_rdb_repository.scan_keys(
            pattern=f'{self._REDIS_USER_KEY}:*'
        )

    def _get_user_push_tokens(self, pk: int) -> Optional[List]:
        try:
            push_token = PushToken.objects.filter(
                user_id=pk).values_list('token', flat=True)
            return list(push_token)
        except CustomUser.DoesNotExist as e:
            set_context('Django exception', value=e.__dict__)
            capture_message(
                'The requested object does not exist error', level='error')
            return []
