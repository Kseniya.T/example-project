import datetime
from typing import List
from django.db.models import Sum, F, DecimalField, ExpressionWrapper, Func, QuerySet, Case, When, Value
from django.core.exceptions import ObjectDoesNotExist
from apps.admin_pages_app.dto import TemplateContextDTO

from django.utils import timezone
from apps.discussion_app.models import Discussion
from apps.notification_app.models import ApplicationConfig
from apps.payment_app.models import PayoutData
from apps.place_app.models import Place
from apps.user_app.models import Profile
from utils.exceptions import ObjectNotFoundError


class Round(Func):
    function = 'ROUND'
    arity = 2


def get_discussions_for_payment() -> List[Discussion]:
    """Method wich returns list of discussions wich riched minimum number of participants needed for transfer payment"""
    
    all_discussions_with_participants = (
        Discussion.objects.prefetch_related(
            'participant_of_discussion', 'participant_of_discussion__user__profile'
        )
        .select_related('place', 'initiator', 'type', 'category', 'theme', 'place__owner')
        .filter(participant_of_discussion__isnull=False, deposit_transferred=False)
        .annotate(
            confirmed_participant_number=Sum('participant_of_discussion__number_of_paid_seats'),
            total_amount=ExpressionWrapper(
                Case(
                    When(initiator=F('place__owner'), then=(Sum(F('participant_of_discussion__number_of_paid_seats')) * F('place__deposit')) - F('place__deposit')),
                    default=(Sum(F('participant_of_discussion__number_of_paid_seats')) * F('place__deposit')),
                ),
                output_field=DecimalField(),
            ),
            service_fee_amount=ExpressionWrapper(
                Round((F('total_amount') * F('service_fee')) / 100, 2),
                output_field=DecimalField(),
            ),
            amount_payable=ExpressionWrapper(
                Round(F('total_amount') - F('service_fee_amount'), 2),
                output_field=DecimalField(),
            ),
        )
        .distinct()
    )
    now = timezone.now()

    discussion_riched_min_participants: list[Discussion] = []

    for discussion in all_discussions_with_participants:
        total_paid_seats: int = discussion.confirmed_participant_number
        place: Place = discussion.place
        discussion_date_start: datetime.datetime = discussion.dt_start
        time_left:  datetime.timedelta = discussion_date_start - now
        total_seconds = time_left.total_seconds()
        total_minutes = int(total_seconds / 60)

        if (total_paid_seats >= place.minimum_available_seats and total_minutes <= 30) or discussion.is_full:
            discussion_riched_min_participants.append(discussion)

    return discussion_riched_min_participants


def discussions_to_confirm(id_list: list[int]):
    """Method wich returns list of discussions with particular id"""

    discussions_to_confirm: QuerySet = (Discussion.objects.prefetch_related(
        'participant_of_discussion', 'participant_of_discussion__user__profile')
        .select_related('place', 'initiator', 'type', 'category', 'theme', 'place__owner')
        .filter(id__in=id_list)
        .annotate(
            confirmed_participant_number=Sum('participant_of_discussion__number_of_paid_seats'),
            total_amount=ExpressionWrapper(
                Case(
                    When(initiator=F('place__owner'), then=(Sum(F('participant_of_discussion__number_of_paid_seats')) * F('place__deposit')) - F('place__deposit')),
                    default=(Sum(F('participant_of_discussion__number_of_paid_seats')) * F('place__deposit')),
                ),
                output_field=DecimalField(),
            ),
            service_fee_amount=ExpressionWrapper(
                Round((F('total_amount') * F('service_fee')) / 100, 2),
                output_field=DecimalField(),
            ),
            amount_payable=ExpressionWrapper(
                Round(F('total_amount') - F('service_fee_amount'), 2),
                output_field=DecimalField(),
            ),
        )
        .distinct()
    )
    total_amount_payable = discussions_to_confirm.aggregate(
        total_amount_payable=Sum('amount_payable'))['total_amount_payable']

    return discussions_to_confirm, total_amount_payable


def get_context_dto(discussions_id_list: list[int], owner_profile: Profile) -> TemplateContextDTO:

    discussions, total_amount_payable = discussions_to_confirm(
        discussions_id_list)
    dto = TemplateContextDTO(
        title='Transfer details',
        objects=list(discussions),
        total_amount_payable=total_amount_payable,
        owner_profile=owner_profile,
        table_header_row=(
        ),
    )
    return dto


def get_payout_context_dto(discussions_id_list: list[int], owner_profile: Profile, payout: PayoutData) -> TemplateContextDTO:

    discussions, total_amount_payable = discussions_to_confirm(
        discussions_id_list)
    dto = TemplateContextDTO(
        title='Payout details',
        objects=list(discussions),
        total_amount_payable=total_amount_payable,
        payout=payout,
        owner_profile=owner_profile,
        table_header_row=(
        ),
    )
    return dto


def get_payouts() -> list[PayoutData]:
    """Method returns list of payouts data objects"""
    payouts_list: list = []
    payouts = PayoutData.objects.all()
    for payout in payouts:
        payouts_list.append(payout)
    return payouts_list
