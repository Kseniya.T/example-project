from abc import ABC
from typing import Any, Sequence, Union
from django.core.exceptions import ObjectDoesNotExist
from sentry_sdk import capture_exception, capture_message, set_context


class Repository(ABC):
    """Class-parent for all child repositories classes"""

    @staticmethod
    def get_object_or_none(
        cls: Any,
        pk: int = None,
        select_related_fields: Sequence[str] = None,
        prefetch_related_fields: Sequence[str] = None,
        **kwargs
    ) -> Any:
        """Returned a model object"""
        if not kwargs and pk:
            kwargs = dict(pk=pk)
        elif kwargs and pk:
            kwargs['pk'] = pk
        try:
            return cls.objects.select_related(
                *select_related_fields or tuple()
            ).prefetch_related(
                *prefetch_related_fields or tuple()
            ).get(**kwargs)
        except ObjectDoesNotExist as e:
            set_context('User doesn\'t exists', value=e.__dict__)
            capture_message('User doesn\'t exists error', level='error')
            return None

    @staticmethod
    def validate_number(value: Any):
        """Method for standart type validating"""
        if isinstance(value, int):
            return True
        elif isinstance(value, str):
            try:
                int(value)
                return True
            except ValueError as e:
                capture_exception(e)
                return False
        return False

    @staticmethod
    def get_kwargs(data: dict, keys: Union[list, tuple]) -> dict:
        """Finding values in a dict with passed fields"""
        kwargs = dict()
        for key in keys:
            value = data.get(key)
            if value:
                kwargs.update({key: value})
        return kwargs
