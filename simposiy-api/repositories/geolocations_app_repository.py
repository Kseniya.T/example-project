import requests
from apps.geolocations_app.models import USACitiesAndCoordinates
from repositories.repository import Repository

class GeolocationAppReposiroty(Repository):
    """Repository for geolocations_app"""

    @staticmethod
    def get_usa_cities_and_coordinates():
        """Method to get data from GeoNames"""

        import requests

        url = "https://wft-geo-db.p.rapidapi.com/v1/geo/countries/US/regions/CA/cities"

        headers = {
            "X-RapidAPI-Key": "897e813811msh62fc216168cd44fp14426ajsnabaec17691e4",
            "X-RapidAPI-Host": "wft-geo-db.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        print(response.json())
