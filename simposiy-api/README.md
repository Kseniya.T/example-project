# simposiy-api
## Бэк "Simposiy"

Бэкенд и API для приложения Simposiy

Обеспечивает хранение данных пользователей, авторизацию и т.д.

### Состав:
* Python 3.10.8
* Django 4.1.2
* Стандартная админка джанги
* API на обычных Django Rest Framework
* Остальные вспомогательные библиотеки смотреть в pyproject.toml

### Локальное развертывание

1. Клонируйте проект `git clone https://gitlab.com/itfox-web/simposiy/simposiy-api.git`;

2. Отредактируйте переменные среды в `core/.env` для настройки параметров postgres (пример вы можете найти в файле `.env.example`);

3. Создайте venv `python -m venv venv`, активируйте его и установите poetry `pip install poetry`;

4. Установите зависимости `poetry install`;

5. Создайте базу данных в PostgreSQL и сохраните ее имя и пароль в файле `.env`;

7. Создайте суперпользователя `manage.py createsuperuser` и укажите имя пользователя и пароль;

8. Запустите приложение `manage.py runserver`

### env.example - переменные конфигурации:

```
# режим для разработки см. документацию django
DEBUG=True

# Database configuration

POSTGRES_DB_NAME=db_name
POSTGRES_DB_USER=username
POSTGRES_DB_PASSWORD=password
POSTGRES_DB_HOST=db_address
POSTGRES_DB_PORT=5432

```

### Дополнительные требования для работы проекта:

- postgresql не ниже 9.6
- Для админки используется собственная база и модель джанги пользователей

### Бизнес логика

- Авторизация пользователей через СМС


## Generate diagrams

```bash
python manage.py graph_models -a -o myapp_models.png
```
