from dataclasses import dataclass
from typing import Any, Dict, List
from pydantic import BaseModel

from apps.discussion_app.models import Discussion


# DTO for stripe checkout session

class MetadataDTO(BaseModel):
    product_id: int
    customer_id: int
    order_representation_id: str
    is_initiator: bool


@dataclass
class ProductDataDTO:
    name: str


@dataclass(frozen=True)
class ModeDTO:
    mode: str


class PriceDataDTO(BaseModel):
    currency: str
    unit_amount: int
    product_data: Dict


class LineItemsDTO(BaseModel):
    price_data: PriceDataDTO
    quantity: int


class CreateCheckoutSessionDTO(BaseModel):
    payment_method_types: List[str]
    line_items: List[LineItemsDTO]
    metadata: MetadataDTO
    mode: str
    expires_at: Any
    success_url: str
    cancel_url: str


class CapabilitiesDTO(BaseModel):
    transfers: Dict


class BusinessProfileDTO(BaseModel):

    name: str
    support_email: str
    support_phone: Any
    url: str = None


@dataclass
class UpdateBusinessProfileDTO:

    url: str
    name: str
    support_email: str


class TosAcceptanceDTO(BaseModel):

    ip: str
    date: Any


class AccountSettingsDTO(BaseModel):
    payouts: Dict


class StripeAccountDTO(BaseModel):
    type: str
    country: str
    business_profile: BusinessProfileDTO
    business_type: str
    capabilities: CapabilitiesDTO
    settings = AccountSettingsDTO
    tos_acceptance: Dict = None

class BankTokenDTO(BaseModel):
    country: str
    currency: str
    account_holder_name: str
    account_holder_type: str
    routing_number: str
    account_number: str


class CreateBankTokenDTO(BaseModel):
    bank_account: BankTokenDTO


class CreateRefundDTO(BaseModel):
    charge: str
