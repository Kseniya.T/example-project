import time
from typing import Tuple
from sentry_sdk import capture_message, set_context
import stripe
from stripe.error import StripeError
from django.contrib import messages
import phonenumbers as pn

from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone

from apps.place_app.choices import CurrencyChoices
from apps.user_app.models import CustomUser, Profile
from core.settings import DOMAIN_NAME, STRIPE_SECRET_KEY
from services.stripe_service.dto import (BusinessProfileDTO, CapabilitiesDTO,
                                         CreateCheckoutSessionDTO, CreateRefundDTO,
                                         LineItemsDTO, MetadataDTO, ModeDTO,
                                         PriceDataDTO, ProductDataDTO,
                                         StripeAccountDTO, UpdateBusinessProfileDTO)
from utils.exceptions import StripeRefundsError


class StripeService:
    """Stripe service class"""

    @staticmethod
    def create_checkout_session_dto(amount: int, name: str, dicsussion_id: int, user_id: str, order_representation_id: str, is_initiator: bool) -> CreateCheckoutSessionDTO:
        """Returns CreateCheckoutSessionDTO"""

        DOMAIN = f"https://{DOMAIN_NAME}"

        if DOMAIN_NAME == '127.0.0.1':
            DOMAIN = f"http://{DOMAIN_NAME}:8000"

        product_data_dto: ProductDataDTO = ProductDataDTO(name=name)
        price_data_dto: PriceDataDTO = PriceDataDTO(
            currency=CurrencyChoices.USD,
            unit_amount=amount,
            product_data=product_data_dto.__dict__
        )
        line_items_dto: LineItemsDTO = LineItemsDTO(
            price_data=price_data_dto.__dict__, quantity=1)
        metadata_dto: MetadataDTO = MetadataDTO(
            product_id=dicsussion_id, customer_id=user_id, order_representation_id=order_representation_id, is_initiator=is_initiator)
        mode_dto: ModeDTO = ModeDTO(mode='payment')
        create_checkout_session_dto: CreateCheckoutSessionDTO = CreateCheckoutSessionDTO(
            payment_method_types=['card'],
            line_items=[line_items_dto],
            metadata=metadata_dto,
            mode=mode_dto.mode,
            # Configured to expire after 30 min
            expires_at=int(time.time() + (1800)),
            success_url=DOMAIN + '/api/v1/payment/success/',
            cancel_url=DOMAIN + '/api/v1/payment/cancel/'
        )
        return create_checkout_session_dto

    @staticmethod
    def create_checkout_session(dto: CreateCheckoutSessionDTO) -> stripe.checkout.Session:
        """ Returns Stripe CheckoutSession object
        """
        try:
            return stripe.checkout.Session.create(api_key=STRIPE_SECRET_KEY, **dto.dict())
        except Exception as e:
            set_context('create_checkout_session_case', value=e.__dict__)
            capture_message('Create checkout session error', level='error')
            raise e

    @staticmethod
    def create_stripe_account_dto(profile: Profile, test: bool=False) -> StripeAccountDTO:
        """Returns StripeAccountDTO"""

        type = 'express'
        
        support_phone=profile.company_phone_number.raw_input
        phone_number = pn.parse(support_phone)
        country = pn.region_code_for_country_code(phone_number.country_code)
        tos_acceptance = {"service_agreement": "recipient"}
        
        if not test:
            business_type = profile.company_business_type
            business_profile = BusinessProfileDTO(
                url=profile.company_business_website, name=profile.company_legal_name, support_email=profile.company_email, support_phone=profile.company_phone_number)  # работает только с номерами США, пример +18004699269
            transfers = {"requested": True}
        if test:
            business_type = 'company'
            business_profile = BusinessProfileDTO(
                url=profile.company_business_website, name=profile.company_legal_name, support_email=profile.company_email, support_phone='+18004699269')  # работает только с номерами США, пример +18004699269
            transfers = {"requested": True}
        capabilities_dto = CapabilitiesDTO(transfers=transfers)

        if country == 'US':
            stripe_account_dto: StripeAccountDTO = StripeAccountDTO(
                type=type, country=country, business_profile=business_profile,
                business_type=business_type, capabilities=capabilities_dto,
            )
        if country != 'US':
            stripe_account_dto: StripeAccountDTO = StripeAccountDTO(
                type=type, country=country, business_profile=business_profile,
                business_type=business_type, capabilities=capabilities_dto,
                tos_acceptance=tos_acceptance,
            )
        return stripe_account_dto

    @staticmethod
    def create_stripe_account(dto: StripeAccountDTO) -> stripe.Account:
        """Create account and return stripe account object"""

        try:
            return stripe.Account.create(api_key=STRIPE_SECRET_KEY, **dto.dict())
        except Exception as e:
            set_context('create_stripe_account_case',
                        value=e.__dict__)
            capture_message(
                'Create Stripe account error', level='error')
            return e

    def refresh_account_link(request):
        account_id = request.GET.get('account')
        account_link = StripeService.create_account_link(account_id=account_id)
        return HttpResponseRedirect(account_link.url)

    @staticmethod
    def create_account_link(account_id: str) -> stripe.AccountLink:
        """Return account Stripe AccountLink object"""

        DOMAIN = f"https://{DOMAIN_NAME}"
        if DOMAIN_NAME == '127.0.0.1':
            DOMAIN = f"http://{DOMAIN_NAME}:8000"
        return_url = DOMAIN + '/api/v1/user/profile/'
        refresh_url = DOMAIN + '/api/v1/refresh-account-link/?account=' + account_id
        try:
            return stripe.AccountLink.create(api_key=STRIPE_SECRET_KEY,
                                             account=account_id,
                                             refresh_url=refresh_url,
                                             return_url=return_url,
                                             type="account_onboarding",
                                             )
        except Exception as e:
            set_context('create_stripe_account_link_case',
                        value=e.__dict__)
            capture_message(
                'Create Stripe account link error', level='error')

            raise e

    @staticmethod
    def modify_payout_schedule(account_id: str):
        """Method wich modify payouts schedule of created account byt setup it to manual"""

        try:
            stripe.Account.modify(api_key=STRIPE_SECRET_KEY, id=account_id,
                                  settings={"payouts": {
                                      "schedule": {"interval": "manual"}}},
                                  )
        except Exception as e:
            set_context('modify_stripe_account_payouts_schedule_case',
                        value=e.__dict__)
            capture_message(
                'Modify Stripe account payouts schedule error', level='error')
            raise e


    @staticmethod
    def update_account(profile: Profile):
        """Method for modify Stripe connected account after profile update"""

        account_id = profile.company_stripe_account
        business_profile = UpdateBusinessProfileDTO(
            url=profile.company_business_website, name=profile.company_legal_name, support_email=profile.company_email)
        try:
            stripe.Account.modify(api_key=STRIPE_SECRET_KEY, id=account_id, business_profile=business_profile.__dict__)
        except Exception as e:
            set_context('modify_stripe_account_case',
                        value=e.__dict__)
            capture_message(
                'Modify Stripe account error', level='error')
            raise e

    @staticmethod
    def create_refund_dto(charge_id: str) -> CreateRefundDTO:
        """Method for create CreateRefundDTO"""
        return  CreateRefundDTO(charge=charge_id)

    @staticmethod
    def create_refund(dto: CreateRefundDTO) -> stripe.Refund:
        """Method for create refund. Returns refund obj"""
        try:
            refund = stripe.Refund.create(api_key=STRIPE_SECRET_KEY, **dto.dict())
            return refund
        except Exception as e:
            set_context('create_refund_case', value=e.__dict__)
            capture_message('Create refund error', level='error')
            if e.code == 'charge_already_refunded':
                raise StripeRefundsError(detail=e._message)
            raise e
