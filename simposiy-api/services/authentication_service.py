import jwt

from dataclasses import dataclass
from datetime import timedelta
from sentry_sdk import capture_message, set_context
from django.utils import timezone

from core.settings import JWT_SECRET, JWT_TTL, REFRESH_TOKEN_TTL
from utils.exceptions import JWTNotValidError, RefreshTokenNotValidError
from .base_service import BaseService


@dataclass(frozen=True)
class JWTSchema:
    is_refresh_token: bool
    user_id: int
    session_id: int
    exp: int


class AuthenticationService(BaseService):
    """Authentication Service based on JWT tokens"""

    @staticmethod
    def get_cleaned_jwt(raw_jwt: str) -> str:
        """Return JWT without `Bearer ` prefix"""
        try:
            return raw_jwt.split()[1]
        except (IndexError, AttributeError) as e:
            set_context('Failed to correctly decode user token',
                        value=e.__dict__)
            capture_message(
                'Failed to correctly decode user token error', level='error')
            raise JWTNotValidError

    @staticmethod
    def decode_token(cleaned_token: str, is_refresh_token: bool = False) -> JWTSchema:
        """
            Декодирует JWT и распаковывает содержимое токена в объект `JWTSchema`
        """
        try:
            decoded_token = jwt.decode(
                cleaned_token, JWT_SECRET, algorithms=['HS256'])
            return JWTSchema(**decoded_token)
        except (jwt.InvalidTokenError, TypeError) as e:
            if is_refresh_token:
                raise RefreshTokenNotValidError
            raise JWTNotValidError

    @staticmethod
    def generate_jwt(user_id: int, session_id: int, is_refresh_token: bool = False) -> bytes:
        """
            This token will be used in http header
            :return: b''
        """
        _timedelta = timedelta(
            minutes=JWT_TTL if not is_refresh_token else REFRESH_TOKEN_TTL)
        expiration_datetime = timezone.now() + _timedelta
        schema: JWTSchema = JWTSchema(
            is_refresh_token=is_refresh_token,
            user_id=user_id,
            session_id=session_id,
            exp=int(expiration_datetime.timestamp())
        )
        token = jwt.encode(schema.__dict__, JWT_SECRET, algorithm='HS256')
        return token.encode('utf-8')
