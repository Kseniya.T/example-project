from django.db.models import TextChoices


class ProfileTypeChoices(TextChoices):
    personal = 'Personal', 'Personal'
    corporate = 'Corporate', 'Corporate'


class BusinessTypeChoices(TextChoices):
    individual = 'individual', 'individual'
    company = 'company', 'company'
