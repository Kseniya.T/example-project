from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    """Custom user model manager"""

    def create_user(self, phone_number, password=None, email=None, **extra_fields):
        email = self.normalize_email(email) if email else None
        if phone_number != None:
            user = self.model(phone_number=phone_number,
                              email=email, **extra_fields)
            user.set_password(password)
            user.save()
            return user
        raise ValueError('Please enter correct data.')

    def create_superuser(self, email: str, password: str = None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        user = self.create_user(
            phone_number='', email=email, password=password, **extra_fields)
        return user
