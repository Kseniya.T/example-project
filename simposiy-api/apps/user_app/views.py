from rest_framework import views
from django.db import transaction
from utils.exceptions import AlreadyHasProfileError, StripeUnsupportedPhoneNumberError, ObjectNotFoundError
from utils.generics.base_generics import BaseGenericAPIView, BaseListAPIView
from django.core.exceptions import ObjectDoesNotExist
from apps.user_app.dto import CreateUserInterestsDTO, DeleteUserInterestsDTO, ProfileDTO, UserIdDTO, UserInterestsDTO
from repositories.auth_app_repository import AuthAppRepository
from rest_framework.permissions import AllowAny

from utils.permissions import IsAuthenticated, IsCorporate
from utils.response import SuccessResponse

from apps.user_app.serializers import AccountLinkSerializer, CreateProfileSerializer, CreateUserInterestsSerializer, DeleteUserInterestsSerializer, InterestsSerializer, ListUserInterestsSerializer, ListUserSerializer, UserInterestsSerializer, UserProfileSerializer
from repositories.user_app_repository import ProfileAppRepository, UserAppRepository
from .models import CustomUser, Interests, Profile, UserInterests
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED
from services.stripe_service.stripe_service import StripeService


class CreateProfile(BaseGenericAPIView):
    """Endpoint for create profile"""
    serializer_class = CreateProfileSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        try:
            if user.profile:
                raise AlreadyHasProfileError
        except ObjectDoesNotExist:
            serializer: CreateProfileSerializer = self.serializer_class(
                data=request.data)
            serializer.is_valid(raise_exception=True)
            profile_dto: ProfileDTO = serializer.dto
            if profile_dto.profile_type == 'Personal':
                ProfileAppRepository.is_personal_profile_fileds_filled(
                    dto=profile_dto)
                new_personal_profile: Profile = ProfileAppRepository.create_personal_profile(
                    user=user, dto=profile_dto)
                _serializer = self.serializer_class(new_personal_profile)
            elif profile_dto.profile_type == 'Corporate':
                ProfileAppRepository.is_corporate_profile_fileds_filled(
                    dto=profile_dto)
                new_corporate_profile: Profile = ProfileAppRepository.create_corporate_profile(
                    user=user, dto=profile_dto)
                
                # Create connected account and update profile field company_stripe_account
                stripe_account_dto = StripeService.create_stripe_account_dto(profile=new_corporate_profile)
                account = StripeService.create_stripe_account(dto=stripe_account_dto)
                
                try:
                    account_id = account.id
                except AttributeError:
                    new_corporate_profile.delete()
                    raise StripeUnsupportedPhoneNumberError(detail=f"You can not create corporate account for your registered phone number, because {account.user_message}")
                
                new_corporate_profile.update_fields(company_stripe_account=account_id)
                _serializer = self.serializer_class(new_corporate_profile)

            return SuccessResponse(
                status=HTTP_201_CREATED,
                detail=f'New profile was successfully created',
                obj=_serializer.data
            )


class CreateRetrieveAccountLinkView(BaseGenericAPIView):
    """View wich create account link from Stripe and return it. Also modify stripe account to manual payout scheduling."""

    serializer_class = AccountLinkSerializer
    permission_classes = [IsAuthenticated, IsCorporate ]

    def get(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        profile = Profile.objects.get(user=user)
        account_id = profile.company_stripe_account
        account_link  = StripeService.create_account_link(account_id=account_id)
        StripeService.modify_payout_schedule(account_id=account_id)
        return SuccessResponse(status=HTTP_201_CREATED,
                obj=account_link)


class CreateStripeConnectedAccountView(BaseGenericAPIView):
    """View for create Stripe connected account for existing corporate profiles."""

    permission_classes = [IsAuthenticated, IsCorporate ]
    
    def get(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        profile = Profile.objects.get(user=user)
        stripe_account_dto = StripeService.create_stripe_account_dto(profile=profile, test=True)
        account = StripeService.create_stripe_account(dto=stripe_account_dto)
        profile.update_fields(company_stripe_account=account.id)        
        return SuccessResponse(status=HTTP_201_CREATED, detail="Stripe connected account was created succesfull")


class UpdateProfileView(BaseGenericAPIView):
    """Endpoint for update profile details"""

    serializer_class = CreateProfileSerializer
    permission_classes = [IsAuthenticated, ]

    def patch(self, request, *args, pk=None, **kwargs):
        user: CustomUser = request.user.user
        try:
            profile: Profile = user.profile
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        serializer: CreateProfileSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        profile_dto: ProfileDTO = serializer.dto
        if profile.profile_type == 'Personal':
            updated_profile: Profile = ProfileAppRepository.partial_update_personal_profile(
                profile=profile, dto=profile_dto)
            _serializer = self.serializer_class(updated_profile)
        elif profile.profile_type == 'Corporate':
            updated_profile: Profile = ProfileAppRepository.partial_update_corporate_profile(
                profile=profile, dto=profile_dto)
            StripeService.update_account(profile=profile)
            _serializer = self.serializer_class(updated_profile)
        return SuccessResponse(status=HTTP_200_OK, detail='Profile information was successfully updated', obj=_serializer.data)


class ProfileDetailView(BaseGenericAPIView):
    """View for get details about own profile"""
    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated, ]

    def get(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        serializer = self.serializer_class(user)
        return SuccessResponse(status=HTTP_200_OK, obj=serializer.data)


class DeleteProfileImageApiView(BaseGenericAPIView):
    """View for delete profile image"""
    
    permission_classes = [IsAuthenticated, ]
    
    def delete(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        ProfileAppRepository.delete_profile_images(user=user)
        return SuccessResponse(detail=f"Image was deleted successfully", status=HTTP_200_OK)


class DeleteProfileCoverImageApiView(BaseGenericAPIView):
    """View for delete profile cover_image"""
    
    permission_classes = [IsAuthenticated, ]
    
    def delete(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        ProfileAppRepository.delete_profile_cover_images(user=user)
        return SuccessResponse(detail=f"Cover image was deleted successfully", status=HTTP_200_OK)


class OtherUserProfileView(BaseGenericAPIView):
    """View for get deils about someone's else profile"""

    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated, ]

    def get(self, request, *args, **kwargs):
        pk: int = self.kwargs['pk']
        try:
            profile: Profile = Profile.objects.select_related('user').get(id=pk)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        
        user: CustomUser = CustomUser.objects.get(id=profile.user_id)
        _serializer = self.serializer_class(user)
        return SuccessResponse(status=HTTP_200_OK, obj=_serializer.data)


class DeleteUserView(BaseGenericAPIView):
    """View for delete user"""
    permission_classes = [IsAuthenticated, ]

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        UserAppRepository.delete_user(user=user)
        AuthAppRepository.deactivate_user_sessions(user=user)
        return SuccessResponse(detail=f"Profile of user was deleted successfully", status=HTTP_200_OK)


class ListOfInterestsView(BaseListAPIView):
    """View for interests list"""

    serializer_class = InterestsSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return Interests.objects.all()


class CreateUserInterestsAPIView(BaseGenericAPIView):
    """Endpoint for create user's interests"""
    serializer_class = CreateUserInterestsSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        serializer: CreateUserInterestsSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: CreateUserInterestsDTO = serializer.dto

        # Before create interests delete old one
        UserInterests.objects.filter(user=user).delete()

        interests_list: list[UserInterests] = ProfileAppRepository.create_user_interests(
            user=user, dto=dto)
        user_interests_dto: UserInterestsDTO = UserInterestsDTO(
            user=user, interests=interests_list)
        user_interests_serializer: UserInterestsSerializer = UserInterestsSerializer(
            user_interests_dto)
        return SuccessResponse(status=HTTP_201_CREATED, detail='Interests were added successfully.', obj=user_interests_serializer.data)


class ListOfUserInterestsView(BaseListAPIView):
    """View for user interests list"""

    serializer_class = ListUserInterestsSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user: CustomUser = self.request.user.user
        return UserInterests.objects.filter(user=user)


class DeleteUserInterestsApiView(views.APIView):
    """View for delete user interests"""
    
    serializer_class = DeleteUserInterestsSerializer
    permission_classes = [IsAuthenticated, ]

    def delete(self, request, **kwargs):
        """View for delete interests"""
        
        user: CustomUser = self.request.user.user
        serializer: DeleteUserInterestsSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        interest_id = serializer.data.get('id')
        try:
            user_interest: UserInterests = UserInterests.objects.get(
                user=user, interests_id=interest_id)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        
        ProfileAppRepository.delete_interests(user_interest=user_interest)

        return SuccessResponse(detail=f"Interest \'{user_interest.interests}' was successfully deleted", status=HTTP_200_OK)


class TestListAllUsersApiView(BaseListAPIView):
    """Test view for list all users with profile"""
    
    serializer_class = ListUserSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return CustomUser.objects.filter(profile__isnull=False)
