from datetime import datetime
import os
from uuid import NAMESPACE_DNS, uuid4

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import FileExtensionValidator

from phonenumber_field.modelfields import PhoneNumberField
from apps.user_app.choices import BusinessTypeChoices, ProfileTypeChoices

from utils.base_model import AppBaseModel
from core.storage_backends import PublicMediaStorage
from .manager import CustomUserManager


class CustomUser(AppBaseModel, AbstractUser):
    """CustomUser model"""

    def update_image_name(self, filename):
        if self.email:
            login_field = self.email
        elif self.phone_number:
            login_field = str(self.phone_number)
        f_name, file_extension = os.path.splitext(filename)
        file_name = str(uuid4(NAMESPACE_DNS, login_field +
                        str(datetime.now()))) + file_extension
        return file_name

    avatar = models.ImageField(
        upload_to=update_image_name, verbose_name='User photo', storage=PublicMediaStorage(),
        max_length=256, blank=True, null=True, validators=[FileExtensionValidator(
            allowed_extensions=['jpeg', 'jpg', 'png']
        )]
    )
    email = models.EmailField(
        db_index=True, unique=True, verbose_name='Email', null=True, blank=True)
    phone_number = PhoneNumberField(
        blank=False, verbose_name='Phone Number', default=None,
        unique=True, db_index=True,
    )
    is_deleted = models.BooleanField(default=False)

    objects = CustomUserManager()
    first_name = None
    last_name = None
    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self) -> str:
        if self.phone_number:
            return f'ID: {self.pk}, {str(self.phone_number)}'
        if self.email:
            return self.email

    def activate(self):
        if not self.is_active:
            self.update_fields(is_active=True)

    def deactivate(self):
        if self.is_active:
            self.update_fields(is_active=False)

    class Meta:
        verbose_name_plural = 'Users'
        verbose_name = 'User'


class Profile(AppBaseModel):
    """Profile model"""

    user = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, related_name='profile', verbose_name='User')
    profile_type = models.CharField(
        max_length=10, verbose_name='Profile type', choices=ProfileTypeChoices.choices)
    is_verified = models.BooleanField('Is verified', default=False)
    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True, verbose_name="Latitude"
    )
    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True, verbose_name="Longitude"
    )
    city = models.CharField(verbose_name='City', max_length=500, null=True)

    user_name = models.CharField(
        max_length=255, verbose_name='User name', blank=True, null=True)
    user_nickname = models.CharField(
        max_length=20, verbose_name='Nickname', unique=True, blank=True, null=True)
    user_email = models.EmailField(
        verbose_name='User email', unique=True,  blank=True, null=True)
    user_info = models.CharField(
        max_length=150, verbose_name='User information', blank=True, null=True)
    user_social_media_link = models.URLField(verbose_name='Link', max_length=500, null=True, blank=True)
    user_image = models.ImageField(verbose_name='User image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
        'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())
    user_cover_image = models.ImageField(verbose_name='User cover image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
        'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())

    company_name = models.CharField(
        max_length=50, verbose_name='Company name',  blank=True, null=True)
    company_legal_name = models.CharField(
        max_length=50, verbose_name='Company legal name', unique=True, null=True)
    company_business_type = models.CharField(
        max_length=10, verbose_name='Business type', choices=BusinessTypeChoices.choices, null=True)
    company_address = models.CharField(
        max_length=200, verbose_name='Address',  blank=True, null=True)
    company_email = models.EmailField(
        verbose_name='Company email', unique=True, null=True)
    company_phone_number = PhoneNumberField(verbose_name='Phone Number', unique=True, null=True)
    company_ein = models.CharField(
        max_length=50, verbose_name='EIN number', unique=True, null=True)
    company_business_website = models.URLField(verbose_name='Business website', unique=True, max_length=500, null=True)
    company_info = models.CharField(
        max_length=150, verbose_name='Information', blank=True, null=True)
    company_image = models.ImageField(verbose_name='Company image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
        'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())
    company_cover_image = models.ImageField(verbose_name='Company cover image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
        'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())
    company_stripe_account = models.CharField(max_length=250, verbose_name='Company Stripe account', unique=True, null=True)


    def save(self, *args, **kwargs):
        for field in self._meta.fields:
            value = getattr(self, field.name)
        if isinstance(value, str) and not value.strip():
            setattr(self, field.name, None)
        return super().save(*args, **kwargs)

    def __str__(self) -> str:
        if self.profile_type == 'Personal':
            return f'ID: {self.id} - {self.user_name}'
        if self.profile_type == 'Corporate':
            return f'ID: {self.id} - {self.company_name}'

    class Meta:
        verbose_name_plural = 'Profiles'
        verbose_name = 'Profile'


class Interests(AppBaseModel):
    """Interests model"""

    name = models.CharField(
        max_length=50, verbose_name='Name', null=True, blank=True)

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name_plural = 'Interests'
        verbose_name = 'Interest'


class UserInterests(AppBaseModel):
    """UserInterests model"""

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='User', related_name='user_interests')
    interests = models.ForeignKey(
        Interests, on_delete=models.CASCADE, verbose_name='Interests', related_name='user_interests')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created at')

    class Meta:
        unique_together = ('user', 'interests')
        verbose_name_plural = 'User Interests'
        verbose_name = 'User Interest'

