# Generated by Django 4.1.3 on 2022-12-15 09:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_app', '0005_profile_phone_number'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='cover_image',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='name',
        ),
    ]
