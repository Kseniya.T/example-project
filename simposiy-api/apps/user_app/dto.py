from cProfile import Profile
from dataclasses import dataclass
from decimal import Decimal
from typing import Any, Optional
from apps.place_app.models import Place

from apps.user_app.models import CustomUser, Interests, UserInterests


@dataclass(frozen=True)
class UserDetailDTO:
    """DTO for user"""

    phone_number: str
    profile: Profile = None
    user_interests: UserInterests = None
    id: int = None


@dataclass
class ProfileDTO:
    """DTO for profile"""
    
    profile_type: str = None

    user_name: str = None
    user_email: str = None
    user_info: str = None
    user_social_media_link: Any = None
    user_image: Any = None
    user_cover_image: Any = None

    company_name: str = None
    company_legal_name: str = None
    company_business_type: str = None
    company_address: str = None
    company_email: str = None
    company_phone_number: Any = None
    company_ein: str = None
    company_business_website: str = None
    company_info: str = None
    company_image: Any = None
    company_cover_image: Any = None
    user: Optional[CustomUser] = None
    id: int = None
    latitude: Decimal = None
    longitude: Decimal = None
    city: str = None


@dataclass
class UserIdDTO:
    """DTO for user id"""
    
    user_id: int


@dataclass
class CreateUserInterestsDTO:
    """DTO for create user interests"""
    
    interests: list[int]
    user: Optional[CustomUser] = None
    id: int = None


@dataclass
class UserInterestsDTO:
    """DTO for user interests"""
    
    interests: list[Interests]
    user: int


@dataclass
class DeleteUserInterestsDTO:
    """DTO for delete interests"""
    
    id: int
