from django.contrib import admin

from apps.user_app.models import Profile


class ProfileStackedInline(admin.StackedInline):
    """ProfileStackedInline for CustomUserAdmin"""

    model = Profile
    verbose_name = 'profile'
    verbose_name_plural = 'profiles'
    extra = 0
    max_num = 1
    show_change_link = True

    def get_queryset(self, request):
        """Queryset for attach profiles to user"""

        qs = super().get_queryset(request)
        return qs
