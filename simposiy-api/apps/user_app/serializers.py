from rest_framework import serializers
from phonenumber_field.serializerfields import PhoneNumberField
from django.core.validators import FileExtensionValidator
from drf_extra_fields.fields import Base64ImageField

from apps.user_app.choices import BusinessTypeChoices, ProfileTypeChoices

from apps.user_app.dto import CreateUserInterestsDTO, DeleteUserInterestsDTO, ProfileDTO, UserDetailDTO, UserInterestsDTO
from apps.user_app.models import CustomUser, Interests, Profile, UserInterests
from utils.custom_serializer import MainSerializer


class CreateProfileSerializer(MainSerializer):
    """Serializer use in CreateProfile, UpdateProfileView, CreateUserInterestsAPIView"""
    
    dto_class = ProfileDTO

    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    profile_type = serializers.ChoiceField(
        required=False, choices=ProfileTypeChoices.choices)
    is_verified = serializers.BooleanField(required=False, read_only=True)
    latitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    city = serializers.CharField(required=False)

    user_name = serializers.CharField(required=False, allow_null=True)
    user_email = serializers.EmailField(required=False, allow_null=True, allow_blank=True)
    user_social_media_link = serializers.URLField(required=False, allow_null=True, allow_blank=True)
    user_info = serializers.CharField(
        required=False, allow_null=True, allow_blank=True)
    user_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], allow_null=True)
    user_cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], allow_null=True)

    company_name = serializers.CharField(required=False, allow_null=True)
    company_legal_name = serializers.CharField(required=False, allow_null=True)
    company_business_type = serializers.ChoiceField(
        required=False, choices=BusinessTypeChoices.choices)
    company_address = serializers.CharField(required=False, allow_null=True)
    company_email = serializers.EmailField(required=False, allow_null=True)
    company_phone_number = PhoneNumberField(required=False, allow_null=True)
    company_ein = serializers.CharField(required=False, allow_null=True)
    company_business_website = serializers.URLField(required=False, allow_null=True)
    company_info = serializers.CharField(
        required=False, allow_null=True, allow_blank=True)
    company_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], allow_null=True)
    company_cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], allow_null=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation

    class Meta:
        fields = '__all__'
        depth = 1


class InterestsSerializer(serializers.Serializer):
    """Serializer use in ListOfInterestsView"""

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)


class BoundedInterestsSerializer(serializers.Serializer):
    """BoundedInterestsSerializer use in UserInterestsSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_id')
    name = serializers.SerializerMethodField(
        read_only=True, method_name='get_name')

    def get_name(self, obj: UserInterests) -> str|None:
        if obj.interests.name:
            return obj.interests.name
        return None

    def get_id(self, obj: UserInterests) -> int|None:
        if obj.interests.id:
            return obj.interests.id
        return None


class CreateUserInterestsSerializer(MainSerializer):
    """Serializer use in CreateUserInterestsAPIView"""

    dto_class = CreateUserInterestsDTO

    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    interests = serializers.ListField(child=serializers.PrimaryKeyRelatedField(
        required=False, queryset=Interests.objects.all()))


class UserInterestsSerializer(MainSerializer):
    """Srializer use in CreateUserInterestsAPIView for response"""

    dto_class = UserInterestsDTO

    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    interests = serializers.ListField(child=BoundedInterestsSerializer())


class ListUserInterestsSerializer(MainSerializer):
    """Serializer use in UserProfileSerializer and ListOfUserInterestsView"""

    dto_class = UserInterestsDTO

    id = serializers.IntegerField(read_only=True)
    interests = InterestsSerializer(read_only=True)


class DeleteUserInterestsSerializer(serializers.Serializer):
    """Serializer use in DeleteUserInterestsApiView"""
    
    id = serializers.IntegerField()


class UserProfileSerializer(MainSerializer):
    """Custom user serializer.
    Use in DiscussionRetrieveSerializer, DiscussionListSerializer, ProfileDetailView, OtherUserProfileView"""
    
    dto_class = UserDetailDTO

    id = serializers.IntegerField(read_only=True)
    phone_number = PhoneNumberField(read_only=True)
    profile = CreateProfileSerializer(read_only=True)
    user_interests = ListUserInterestsSerializer(read_only=True, many=True)


class AccountLinkSerializer(serializers.Serializer):
    """Serializer for CreateRetrieveAccountLinkView"""
    
    link = serializers.URLField(read_only=True)


class ListUserSerializer(serializers.Serializer):
    """Serializer for TestListAllUsersApiView"""
    
    id = serializers.IntegerField(read_only=True)
    phone_number = PhoneNumberField(read_only=True)
