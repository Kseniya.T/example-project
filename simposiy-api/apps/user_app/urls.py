from django.urls import path

from apps.user_app.views import (CreateStripeConnectedAccountView, CreateProfile, CreateUserInterestsAPIView, DeleteProfileImageApiView, DeleteProfileCoverImageApiView, DeleteUserInterestsApiView, DeleteUserView,
                                 ListOfInterestsView, ListOfUserInterestsView, OtherUserProfileView, TestListAllUsersApiView, UpdateProfileView, ProfileDetailView, CreateRetrieveAccountLinkView)
from services.stripe_service.stripe_service import StripeService



app_name = 'user_app'

urlpatterns = [
    # profile
    path('user/profile/create/', CreateProfile.as_view(),
         name='user-create-profile'),
    path('user/profile/update/', UpdateProfileView.as_view(),
         name='user-update-profile'),
    path('user/delete/', DeleteUserView.as_view(), name='user-delete'),
    path('user/profile/', ProfileDetailView.as_view(), name='user-profile'),
    path('user/profile/<int:pk>', OtherUserProfileView.as_view(),
         name='other-user-profile'),
    path('user/profile/image/delete/', DeleteProfileImageApiView.as_view(), name='profile-image-delete'),
    path('user/profile/coverimage/delete/', DeleteProfileCoverImageApiView.as_view(), name='profile-coverimage-delete'),
    
    # stripe account link
    path('account-link/', CreateRetrieveAccountLinkView.as_view(), name='account-link'),
    path('refresh-account-link/', StripeService.refresh_account_link, name='refresh_account_link'),

    # interests
    path('interests/', ListOfInterestsView.as_view(), name='interests'),
    path('user/create-interests/', CreateUserInterestsAPIView.as_view(),
         name='user-create-interests'),
    path('user/list-interests/', ListOfUserInterestsView.as_view(),
         name='user-list-interests'),
    path('user/delete-interests/', DeleteUserInterestsApiView.as_view(),
         name='user-delete-interests'),
    
    # Stripe connected account
    path('user/connected-account/create/', CreateStripeConnectedAccountView.as_view(), name='stripe-connected_account'),
    
    # test endpoints
    path('users/list/', TestListAllUsersApiView.as_view(), name='list-users'),
]
