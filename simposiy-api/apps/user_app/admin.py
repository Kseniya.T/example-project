from typing import Type
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from apps.user_app.inlines import ProfileStackedInline

from apps.user_app.models import CustomUser, Interests, Profile, UserInterests
from .forms import CustomUserChangeForm, CustomUserCreationForm


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    model: Type[CustomUser] = CustomUser
    inlines: tuple = (ProfileStackedInline, )

    list_display: tuple = ('id', 'phone_number', 'profile', 'email',
                           'is_active', 'is_superuser')
    ordering: tuple = ('-pk',)

    add_fieldsets: tuple = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'phone_number',
                'email',
                'password1',
                'password2',
                'is_active',
            )}
         ),
    )

    fieldsets: tuple = (
        (
            None, {
                'classes': ('wide',),
                'fields': (
                    'password',)
            }
        ),
        (
            'Personal information', {
                'classes': ('wide',),
                'fields': (
                    'phone_number',
                    'email',
                )
            }),
        (
            'Permissions', {
                'classes': ('wide',),
                'fields': (
                    'is_active',
                    'is_staff',
                    'groups',
                    'user_permissions', ),
            }),
        (
            'Important dates', {
                'classes': ('wide',),
                'fields': ('last_login', 'date_joined', )
            }),
    )
    readonly_fields: tuple = ('date_joined', 'last_login', )
    search_fields: tuple = ('phone_number',)
    list_filter: tuple = ()

    form = CustomUserChangeForm
    add_form = CustomUserCreationForm


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    model: Type[Profile] = Profile
    # list_display: list = [field.name for field in Profile._meta.get_fields()]
    list_display: tuple = ('id',
                           'user',
                           'profile_type',
                           'is_verified',
                           'user_name',
                           'user_email',
                           'company_name',
                           'company_legal_name',
                           'company_address',
                           'company_email',
                           'company_ein',
                           'company_business_website',
                           'company_stripe_account'
                           )
    list_filter: tuple = ('profile_type',)
    exclude = ('user_nickname',)


@admin.register(Interests)
class InterestsAdmin(admin.ModelAdmin):
    model: Type[Profile] = Interests
    list_display: tuple = ('id', 'name')


@admin.register(UserInterests)
class UserInterestsAdmin(admin.ModelAdmin):
    model: Type[Profile] = UserInterests
    list_display: tuple = ('id',
                           'user',
                           'interests',
                           'created_at',
                           )
