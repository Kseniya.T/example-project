from django.db import models

from apps.user_app.models import CustomUser
from utils.base_model import AppBaseModel
from django.core.validators import EmailValidator
from phonenumber_field.modelfields import PhoneNumberField


class Notification(AppBaseModel):
    """Notification model used for `CustomUser` notification"""

    related_user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                                     verbose_name='Related user', related_name='notification')
    title = models.CharField(max_length=128, verbose_name='Title', )
    body = models.TextField(verbose_name='Body')
    payload = models.JSONField(verbose_name='Payload', null=True, blank=True, )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created at', )
    is_viewed = models.BooleanField(default=False, verbose_name='Is viewed', )
    is_sent = models.BooleanField(default=False, verbose_name='Is sent', )

    def __str__(self) -> str:
        return f'Notification for {self.related_user_id}: {self.title} - {self.body}'

    def make_viewed(self):
        self.update_fields(save=True, is_viewed=True)

    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'


class ApplicationConfig(AppBaseModel):
    """ApplicationConfig model"""
    
    app_support_email = models.EmailField(verbose_name='App support email',
                                         max_length=256,
                                         validators=[EmailValidator])
    app_support_email_password = models.CharField(max_length=50, verbose_name='App support email password')
    app_phone_number = PhoneNumberField(verbose_name='App phone number',  null=True, unique=True, db_index=True)
    app_service_fee = models.SmallIntegerField(verbose_name='Service fee, %')
    
    class Meta:
        verbose_name = 'Application config'
        verbose_name_plural = 'Application configs'
