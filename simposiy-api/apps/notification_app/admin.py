from typing import Any
from django.contrib import admin

from apps.notification_app.models import ApplicationConfig, Notification
from repositories.init import push_notification_repository as pn_repository


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display: tuple = ('id', 'title', 'created_at',
                           'is_viewed', 'is_sent', )

    def save_model(self,  request: Any, obj: Notification, form: Any, change: Any) -> None:
        pn_repository.add_notification_data_to_rdb(
            user_pk=obj.related_user_id,
            notification=obj,
        )
        return super().save_model(request, obj, form, change)


@admin.register(ApplicationConfig)
class ApplicationConfigAdmin(admin.ModelAdmin):
    list_display: tuple = ('app_support_email',
                           'app_support_email_password',
                           'app_phone_number',
                           'app_service_fee',
                           )
    
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False if ApplicationConfig.objects.exists() else True
