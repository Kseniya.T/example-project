from django.urls import path

from .views import *

app_name = 'notification_app'

urlpatterns = [
    # Retrieving a list of Notification objects
    path('notifications/list/', NotificationListApiView.as_view(),
         name='notifications-list'),
    path("notification/<int:pk>/details/", RetrieveNotificationPostResponseApiView.as_view(), name="notification-details-post-response"),
    
    # Path for test push notification
    path('notifications/test/', TestView.as_view(), name='notifications-test'),
    
    # application config
    path('service-fee/', RetrieveServiceChargePercetageApiView.as_view(), name='service-fee')
]
