from rest_framework.permissions import AllowAny
from apps.discussion_app.enums import NotificationEventEnum
from apps.rabbitmq_app.enums import QueuesEnum
from apps.rabbitmq_app.producer import ProducerRabbitMQ
from core.settings import EXCHANGE_PUSH_NOTIFICATION_INBOUND, RABBITMQ_HOST, RABBITMQ_PRODUCER_PASSWORD, RABBITMQ_PRODUCER_USER
from repositories.notification_repository import NotificationRepository
from repositories.discussion_app_repository import DiscussionAppRepository
from repositories.init import push_notification_repository
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from utils.exceptions import ObjectNotFoundError
from django.core.exceptions import ObjectDoesNotExist

from utils.generics.base_generics import BaseGenericAPIView, BaseListAPIView, BaseRetrieveCreateAPIView
from utils.response import SuccessResponse

from .serializers import HostDiscussionAllowSerializer, NotificationSerializer, ServiceChargeRetrieveSerializer
from utils.permissions import IsAuthenticated
from django.db import transaction
from .models import ApplicationConfig, Notification
from apps.notification_app.tasks import send_push_notifications
from apps.user_app.models import CustomUser
from apps.discussion_app.models import Discussion


class NotificationListApiView(BaseListAPIView):
    """View for retrieving a list `Notification` objects"""
    
    permission_classes = (IsAuthenticated, )
    serializer_class = NotificationSerializer

    def get_queryset(self):
        return Notification.objects.filter(
            related_user=self.request.user.user,
        ).order_by('-pk')


class TestView(BaseGenericAPIView):
    """View for test notifications. Use after create test push token"""
    @transaction.atomic
    def get(self, request, *args, **kwargs):
        test_user = CustomUser.objects.first()
        title = "Test"
        body = "Test notification"
        payload = {'test_discussion':f'{test_user.pk}'}
        notification = NotificationRepository.create_notification(
            related_user=test_user, title=title, body=body, payload=payload)

        push_notification_repository.add_notification_data_to_rdb(
            user_pk=test_user.pk,
            notification=notification,
        )
        send_push_notifications()
        return Response(status=HTTP_200_OK)


class RetrieveServiceChargePercetageApiView(BaseRetrieveCreateAPIView):
    """View for retrieve service charge percentage"""
    
    serializer_class = ServiceChargeRetrieveSerializer
    permission_classes = (IsAuthenticated, )
    
    def get(self, request, *args, **kwargs):
        app_config: ApplicationConfig = ApplicationConfig.objects.order_by('-id').first()
        serializer: ServiceChargeRetrieveSerializer = self.serializer_class(app_config)
        return SuccessResponse(
            status=HTTP_200_OK,
            obj=serializer.data
        )


class RetrieveNotificationPostResponseApiView(BaseRetrieveCreateAPIView):
    """View for retrieve notification details and post response"""
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)
    
    def get(self, request, *args, **kwargs):
        notification_pk = self.kwargs['pk']
        user: CustomUser = request.user.user
        try:
            notification = Notification.objects.get(id=notification_pk)
            discussion_id = int(notification.payload.get('discussion_id'))
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        DiscussionAppRepository.check_discussion_place_owner(id=discussion_id, user=user)
        serializer = self.serializer_class(notification)
        return SuccessResponse(
            status=HTTP_200_OK,
            obj=serializer.data
        )
    
    def post(self, request, *args, **kwargs):
        notification_pk = self.kwargs['pk']
        user: CustomUser = request.user.user
        try:
            notification = Notification.objects.get(id=notification_pk)
            discussion_id = int(notification.payload.get('discussion_id'))
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        discussion: Discussion = DiscussionAppRepository.check_discussion_place_owner(id=discussion_id, user=user)
        DiscussionAppRepository.check_before_accept_deny(discussion=discussion)
        serializer: HostDiscussionAllowSerializer = HostDiscussionAllowSerializer(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        allow = serializer.validated_data.get('allow')
        if allow == True:
            discussion = DiscussionAppRepository.activate_discussion(id=discussion_id)
            DiscussionAppRepository
            data = {'event': NotificationEventEnum.HOST_ACCEPT_RESPONSE, 'discussion': discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)
            return SuccessResponse(status=HTTP_200_OK, detail=f'The request to have a discussion on topic {discussion.topic} in your location {discussion.place.name} has been successfully approved')
        if allow == False:
            discussion = Discussion.objects.select_related('initiator').get(id=discussion_id)
            discussion.update_fields(on_review=False)
            data = {'event': NotificationEventEnum.HOST_DENY_RESPONSE, 'discussion': discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)
            return SuccessResponse(status=HTTP_200_OK, detail=f'The request to have a discussion on topic {discussion.topic} in your location {discussion.place.name} has been successfully denied')
