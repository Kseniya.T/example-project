from rest_framework import serializers


class NotificationSerializer(serializers.Serializer):
    """Serializer for retrieving `Notification` object"""

    pk = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    body = serializers.CharField(read_only=True)
    payload = serializers.JSONField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    is_viewed = serializers.BooleanField(read_only=True)
    is_sent = serializers.BooleanField(read_only=True)


class ServiceChargeRetrieveSerializer(serializers.Serializer):
    """Serializer for RetrieveServiceChargePercetageApiView"""

    app_service_fee = serializers.IntegerField(read_only=True)


class HostDiscussionAllowSerializer(serializers.Serializer):
    """Serializer for RetrieveNotificationPostResponseApiView post method"""
    
    allow = serializers.BooleanField()
