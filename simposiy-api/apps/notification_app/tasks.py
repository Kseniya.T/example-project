from repositories.init import push_notification_repository
from core.celery import app


@app.task
def send_push_notifications(with_buttons: bool = False):
    """Sending push notifications"""

    push_notification_repository.send_push_notifications(with_buttons=with_buttons)
