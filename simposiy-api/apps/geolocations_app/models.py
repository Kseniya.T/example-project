from django.db import models

from utils.base_model import AppBaseModel



class USACitiesAndCoordinates(AppBaseModel):
    """USACitiesAndCoordinates model"""
    
    city = models.CharField(verbose_name='City', max_length=500)
    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, verbose_name="Latitude"
    )
    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, verbose_name="Longitude"
    )
    
    class Meta:
        verbose_name = 'USA city and coordinate'
        verbose_name_plural = 'USA cities and coordinates'

    def __str__(self) -> str:
        return f'ID: {self.pk} - "{self.name}"'
