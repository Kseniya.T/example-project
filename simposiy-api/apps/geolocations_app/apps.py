from django.apps import AppConfig


class GeolocationsAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.geolocations_app'
