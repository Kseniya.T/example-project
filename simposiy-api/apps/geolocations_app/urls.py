from django.urls import path

from apps.geolocations_app.views import ListUSACitiesAndCoordinatesApiView

app_name = 'geolocations_app'


urlpatterns = [
    path('list-usa-cities/', ListUSACitiesAndCoordinatesApiView.as_view(), name='list-usa-cities'),
]
