from django.shortcuts import render
from apps.geolocations_app.models import USACitiesAndCoordinates
from rest_framework.permissions import AllowAny
from apps.geolocations_app.serializers import ListUSACitiesAndCoordinatesSerializer
from repositories.geolocations_app_repository import GeolocationAppReposiroty


from utils.generics.base_generics import BaseListAPIView


class ListUSACitiesAndCoordinatesApiView(BaseListAPIView):
    """View for list USACitiesAndCoordinates objects"""
    serializer_class = ListUSACitiesAndCoordinatesSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        GeolocationAppReposiroty.get_usa_cities_and_coordinates()
        return USACitiesAndCoordinates.objects.all()
