from rest_framework import serializers


class ListUSACitiesAndCoordinatesSerializer(serializers.Serializer):
    """Serializer for ListUSACitiesAndCoordinatesApiView"""
    
    city = serializers.CharField(read_only=True)
    latitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation
