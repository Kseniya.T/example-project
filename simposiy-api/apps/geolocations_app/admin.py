from typing import Type
from django.contrib import admin

from apps.geolocations_app.models import USACitiesAndCoordinates


@admin.register(USACitiesAndCoordinates)
class USACitiesAndCoordinatesAdmin(admin.ModelAdmin):
    model: Type[USACitiesAndCoordinates] = USACitiesAndCoordinates
    list_display: tuple = ('id',
                           'city',
                           'latitude',
                           'longitude'
                           )
