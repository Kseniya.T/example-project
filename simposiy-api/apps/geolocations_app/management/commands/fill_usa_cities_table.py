import logging
from django.core.management import BaseCommand
from django.apps import apps
from django.db import transaction


class Command(BaseCommand):
    """Comand for prepopulating  table USACitiesAndCoordinates with data"""

    help = "Prepopulating USACitiesAndCoordinates table with data"

    @transaction.atomic
    def handle(self, *args, **options):
        self.fill_usa_cities_and_cordinatese()
        logging.info('Work done!')
    
    def fill_usa_cities_and_cordinatese(self):
        """Creating the USACitiesAndCoordinates objects"""
        USACitiesAndCoordinates = apps.get_model('geolocations_app', 'USACitiesAndCoordinates')
        
        logging.info('USACitiesAndCoordinates objects were created successfully')