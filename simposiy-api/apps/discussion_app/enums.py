from enum import Enum


class NotificationEventEnum(Enum):
    """
        MIN_PARTICIPANTS_RICHED: 'min_participats_riched'
        MAX_PARTICIPANTS_RICHED: 'max_participats_riched'
        NEW_DISCUSSION_CREATED: 'new_discussion_created'
        HOST_REQUEST: 'host_request'
        HOST_DENY_RESPONSE: 'host_deny_response'
        HOST_ACCEPT_RESPONSE: 'host_deny_response'
        PENDING_HOST_TIMED_OUT: 'pending_host_timed_out'
    """
    MIN_PARTICIPANTS_RICHED = 'min_participats_riched'
    MAX_PARTICIPANTS_RICHED = 'max_participats_riched'
    NEW_DISCUSSION_CREATED = 'new_discussion_created'
    HOST_REQUEST = 'host_request'
    HOST_DENY_RESPONSE = 'host_deny_response'
    HOST_ACCEPT_RESPONSE = 'host_accept_response'
    PENDING_HOST_TIMED_OUT = 'pending_host_timed_out'
