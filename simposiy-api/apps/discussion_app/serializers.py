from decimal import Decimal
from rest_framework import serializers
from django.core.validators import FileExtensionValidator
from django.db.models import Sum
from djmoney.contrib.django_rest_framework import MoneyField

from apps.discussion_app.models import (Discussion, DiscussionCategory,
                                        DiscussionTheme, DiscussionType,
                                        ParticipantsOfDiscussion, UserThemes)
from apps.place_app.choices import CurrencyChoices
from apps.place_app.models import Place
from apps.place_app.serializers import BoundedPlaceSerializer, PlaceSerializer
from apps.user_app.models import CustomUser
from apps.user_app.serializers import UserProfileSerializer

from utils.custom_serializer import MainSerializer
from utils.fields import Base64ImageField
from apps.discussion_app.dto import (AutoPaymentDTO, CreateDiscussionDTO,
                                     CreateUserThemesDTO, RetrieveDiscussionDTO,
                                     UpdateDiscussionDTO, UserThemesDTO)


class BoundedUserProfileSerializer(serializers.Serializer):
    """Serializer for ListActiveDiscussionView,
        ListActiveDiscussionOfPlaceView,
        ListInitiatedDiscussionOfUserView,
        ListUserParticipationDiscussionsView,
        ListSomeUserParticipationDiscussionsView
    """

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_profile_id')
    user_id = serializers.IntegerField(read_only=True)
    image_url = serializers.SerializerMethodField(
        read_only=True, method_name='get_image_url')
    is_initiator = serializers.BooleanField(read_only=True)
    participation_paid = serializers.SerializerMethodField(
        read_only=True, method_name='get_paid')

    def get_image_url(self, obj: ParticipantsOfDiscussion) -> str | None:
        if obj.user.profile.user_image:
            return obj.user.profile.user_image.url
        if obj.user.profile.company_image:
            return obj.user.profile.company_image.url
        return None

    def get_profile_id(self, obj: ParticipantsOfDiscussion) -> int | None:
        if obj.user.profile.id:
            return obj.user.profile.id
        return None

    def get_paid(self, obj: ParticipantsOfDiscussion) -> bool:
        if obj.number_of_paid_seats == 1:
            return True
        return False


class DiscussionTypeSerializer(serializers.Serializer):
    """Serializer for DiscussionTypeAPIView"""

    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])


class DiscussionCategorySerializer(serializers.Serializer):
    """Serializer for DiscussionCategoryAPIView"""

    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])


class DiscussionThemeSerializer(serializers.Serializer):
    """Serializer for DiscussionThemeAPIView"""

    id = serializers.IntegerField()
    name = serializers.CharField()
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    is_golden = serializers.BooleanField(required=False)
    is_top = serializers.BooleanField(required=False)


class CreateDiscussionSerializer(MainSerializer):
    """Serializer for CreateDiscussionAPIView"""

    dto_class = CreateDiscussionDTO

    id = serializers.IntegerField(required=False)
    initiator = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    place = serializers.PrimaryKeyRelatedField(queryset=Place.objects.all())
    type = serializers.PrimaryKeyRelatedField(
        queryset=DiscussionType.objects.all())
    category = serializers.PrimaryKeyRelatedField(
        queryset=DiscussionCategory.objects.all())
    theme = serializers.PrimaryKeyRelatedField(
        queryset=DiscussionTheme.objects.all())
    topic = serializers.CharField()
    description = serializers.CharField(required=False)
    dt_start = serializers.DateTimeField()
    number_of_participants = serializers.IntegerField()
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    is_active = serializers.BooleanField(required=False)
    is_full = serializers.BooleanField(required=False)
    on_review = serializers.BooleanField(required=False)
    holding_agreed = serializers.BooleanField(required=False)
    paid = serializers.BooleanField(required=False)


class UpdateDiscussionSerializer(MainSerializer):
    """Serializer for UpdateDiscussionAPIView"""

    dto_class = UpdateDiscussionDTO

    theme = serializers.PrimaryKeyRelatedField(
        required=False, queryset=DiscussionTheme.objects.all())
    topic = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    place = serializers.PrimaryKeyRelatedField(required=False, queryset=Place.objects.all())
    dt_start = serializers.DateTimeField(required=False)


class BoundedPaidDetailsSerializer(serializers.Serializer):
    """Bounded serializer for DiscussionRetrieveSerializer"""

    check_number = serializers.CharField(read_only=True)
    payment_status = serializers.CharField(read_only=True)
    payment_time = serializers.DateTimeField(read_only=True)
    total_payment =  MoneyField(max_digits=14, decimal_places=2,)
    pdf = serializers.URLField(read_only=True)


class DiscussionRetrieveSerializer(MainSerializer):
    """Serializer for retrieve dicussion"""

    dto_class = RetrieveDiscussionDTO

    id = serializers.IntegerField(read_only=True)
    initiator = UserProfileSerializer(read_only=True)
    place = PlaceSerializer()
    type = DiscussionTypeSerializer(read_only=True)
    category = DiscussionCategorySerializer(read_only=True)
    theme = DiscussionThemeSerializer(read_only=True)
    topic = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    dt_start = serializers.DateTimeField(read_only=True)
    number_of_participants = serializers.IntegerField(read_only=True)
    image = Base64ImageField(read_only=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    per_person = serializers.SerializerMethodField(method_name='to_decimal')
    per_person_currency = serializers.ChoiceField(
        read_only=True, source='place.deposit_currency', choices=CurrencyChoices.choices)
    created_at = serializers.DateTimeField(read_only=True)
    is_active = serializers.BooleanField(read_only=True)
    is_full = serializers.BooleanField(read_only=True)
    on_review = serializers.BooleanField(read_only=True)
    holding_agreed = serializers.BooleanField(read_only=True)
    paid = serializers.BooleanField(read_only=True)
    confirmed_participants_number = serializers.SerializerMethodField(
        method_name='zero_confirmed_participants_number')
    participant_of_discussion = serializers.ListField(
        child=BoundedUserProfileSerializer())
    payment_link = serializers.CharField(read_only=True)
    paid_details = BoundedPaidDetailsSerializer()

    def to_decimal(self, obj: Discussion) -> str:
        deposit: Decimal = obj.place.deposit.amount
        return str(deposit)

    def zero_confirmed_participants_number(self, obj: Discussion) -> int:
        if obj.confirmed_participants_number == None:
            return 0
        return obj.confirmed_participants_number


class DiscussionListSerializer(serializers.Serializer):
    """Serializer for Lists of dicussions"""

    id = serializers.IntegerField(read_only=True)
    initiator = UserProfileSerializer(read_only=True)
    place = PlaceSerializer()
    type = DiscussionTypeSerializer(read_only=True)
    category = DiscussionCategorySerializer(read_only=True)
    theme = DiscussionThemeSerializer(read_only=True)
    topic = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    dt_start = serializers.DateTimeField(read_only=True)
    number_of_participants = serializers.IntegerField(read_only=True)
    image = Base64ImageField(read_only=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    per_person = serializers.SerializerMethodField(method_name='to_decimal')
    per_person_currency = serializers.ChoiceField(
        read_only=True, source='place.deposit_currency', choices=CurrencyChoices.choices)
    created_at = serializers.DateTimeField(read_only=True)
    is_active = serializers.BooleanField(read_only=True)
    is_full = serializers.BooleanField(read_only=True)
    on_review = serializers.BooleanField(read_only=True)
    holding_agreed = serializers.BooleanField(read_only=True)
    paid = serializers.BooleanField(read_only=True)

    def to_decimal(self, obj: Discussion) -> Decimal:
        return str(obj.place.deposit.amount)


class ResponseCreateDiscussionSerializer(DiscussionListSerializer):
    """Serializer for CreateDiscussionView"""

    confirmed_participant_number = serializers.SerializerMethodField(
        method_name='get_confirmed_participant_number')
    participant_of_discussion = BoundedUserProfileSerializer(many=True)

    def get_confirmed_participant_number(self, obj: Discussion) -> int:
        return obj.participant_of_discussion.count()


class ActiveDiscussionSerializer(DiscussionListSerializer):
    """Serializer for ListActiveDiscussionView"""

    confirmed_participants_number = serializers.SerializerMethodField(
        method_name='zero_confirmed_participants_number')
    participant_of_discussion = BoundedUserProfileSerializer(many=True)

    def zero_confirmed_participants_number(self, obj: ParticipantsOfDiscussion) -> int:
        if obj.confirmed_participant_number == None:
            return 0
        return obj.confirmed_participant_number


class ActiveDiscussionSerializerFiltered(DiscussionListSerializer):
    """Serializer for response in FilterSearchDiscussionApiView """

    confirmed_participants_number = serializers.SerializerMethodField(
        method_name='zero_confirmed_participants_number')
    participant_of_discussion = BoundedUserProfileSerializer(many=True)

    def zero_confirmed_participants_number(self, obj: Discussion) -> int:
        confirmed_participants = obj.participant_of_discussion.filter(
            number_of_paid_seats__gt=0).aggregate(Sum('number_of_paid_seats'))['number_of_paid_seats__sum']
        if confirmed_participants is None:
            return 0
        return confirmed_participants


class BoundedDiscussionThemesSerializer(serializers.Serializer):
    """Bounded serializer for UserThemesSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_id')
    name = serializers.SerializerMethodField(
        read_only=True, method_name='get_name')

    def get_name(self, obj: UserThemes) -> str | None:
        if obj.themes.name:
            return obj.themes.name
        return None

    def get_id(self, obj: UserThemes) -> int | None:
        if obj.themes.id:
            return obj.themes.id
        return None


class CreateUserThemesSerializer(MainSerializer):
    """Serializer use in CreateUserThemesAPIView"""

    dto_class = CreateUserThemesDTO

    id = serializers.IntegerField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    themes = serializers.ListField(child=serializers.PrimaryKeyRelatedField(
        required=False, queryset=DiscussionTheme.objects.all()))


class UserThemesSerializer(MainSerializer):
    """Srializer use in CreateUserThemesAPIView for response"""

    dto_class = UserThemesDTO

    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    themes = serializers.ListField(child=BoundedDiscussionThemesSerializer())


class ListUserThemesSerializer(MainSerializer):
    """Serializer use in UserProfileSerializer and ListOfUserThemesView"""

    dto_class = UserThemesDTO

    id = serializers.IntegerField(read_only=True)
    themes = DiscussionThemeSerializer(read_only=True)


class DeleteUserThemesSerializer(serializers.Serializer):
    """Serializer use in DeleteUserThemesApiView"""

    id = serializers.IntegerField()


class AutoPaymentSerializer(MainSerializer):
    """Serializer for AutoPaymentView. Use ONLY for TESTS"""

    dto_class = AutoPaymentDTO

    user = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    discussion = serializers.PrimaryKeyRelatedField(
        queryset=Discussion.objects.all())


class ParticipantOfDiscussionSerializer(serializers.Serializer):
    """Serializer use in response for AutoPaymentView"""

    user = UserProfileSerializer(read_only=True)
    discussion = CreateDiscussionSerializer(read_only=True)
    number_of_paid_seats = serializers.IntegerField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    is_initiator = serializers.BooleanField(read_only=True)


class MyTicketsSerializer(serializers.Serializer):
    """Serializer for MyTicketsView"""

    id = serializers.IntegerField(read_only=True)
    type = DiscussionTypeSerializer(read_only=True)
    topic = serializers.CharField(read_only=True)
    image = Base64ImageField(read_only=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    dt_start = serializers.DateTimeField(read_only=True)
    number_of_participants = serializers.IntegerField(read_only=True)
    place = BoundedPlaceSerializer()
