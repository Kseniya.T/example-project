from django.db import models
from django.core.validators import FileExtensionValidator

from apps.place_app.models import Place
from apps.user_app.models import CustomUser
from core.storage_backends import PublicMediaStorage
from utils.base_model import AppBaseModel
from utils.exceptions import DiscussionIsFullError, MaxLimitOfSeatsError, MinLimitOfSeatsError


class DiscussionType(AppBaseModel):
    """DiscussionType model"""

    name = models.CharField(verbose_name='Type of discussion', max_length=150)
    description = models.TextField(
        verbose_name='Description', blank=True, null=True)
    image = models.ImageField(verbose_name='Image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
                              'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())

    class Meta:
        verbose_name = 'Discussion type'
        verbose_name_plural = 'Discussion types'

    def __str__(self) -> str:
        return f'ID: {self.pk} - {self.name}'


class DiscussionCategory(AppBaseModel):
    """DiscussionCategory model"""

    name = models.CharField(
        verbose_name='Category of discussion', max_length=150)
    description = models.TextField(
        verbose_name='Description', blank=True, null=True)
    image = models.ImageField(verbose_name='Image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
                              'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())

    class Meta:
        verbose_name = 'Discussion category'
        verbose_name_plural = 'Discussion categories'

    def __str__(self) -> str:
        return f'ID: {self.pk} - {self.name}'


class DiscussionTheme(AppBaseModel):
    """DiscussionTheme model"""

    name = models.CharField(
        verbose_name='Theme of discussion', max_length=150)
    image = models.ImageField(verbose_name='Image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
                              'jpeg', 'jpg', 'png', 'webp'])], storage=PublicMediaStorage(), blank=True, null=True)
    is_golden = models.BooleanField(
        verbose_name='Golden', default=False)
    is_top = models.BooleanField(verbose_name='Top', default=False)

    class Meta:
        verbose_name = 'Discussion theme'
        verbose_name_plural = 'Discussion themes'

    def __str__(self) -> str:
        return f'ID: {self.pk} - {self.name}'


class UserThemes(AppBaseModel):
    """UserThemes model"""

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='User', related_name='user_themes')
    themes = models.ForeignKey(
        DiscussionTheme, on_delete=models.CASCADE, verbose_name='Themes', related_name='user_themes')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created at')

    class Meta:
        unique_together = ('user', 'themes')
        verbose_name_plural = 'User Themes'
        verbose_name = 'User Theme'


class Discussion(AppBaseModel):
    """Discussion model"""

    initiator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, verbose_name='Initiator', related_name='discussion')
    place = models.ForeignKey(
        Place, on_delete=models.CASCADE, verbose_name='Place', related_name='discussion')
    type = models.ForeignKey(DiscussionType, verbose_name='Type of discussion',
                             on_delete=models.CASCADE, related_name='discussion')
    category = models.ForeignKey(DiscussionCategory, on_delete=models.CASCADE,
                                 related_name='discussion', verbose_name='Category of discussion')
    theme = models.ForeignKey(DiscussionTheme, on_delete=models.CASCADE,
                              related_name='discussion', verbose_name='Theme')
    topic = models.CharField(
        max_length=256, verbose_name='Topic of discussion')
    description = models.TextField(
        verbose_name='Description', blank=True, null=True)
    dt_start = models.DateTimeField(verbose_name='DateTime start')
    number_of_participants = models.IntegerField(
        verbose_name="Number of participants", default=0)
    image = models.ImageField(verbose_name='Image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
                              'jpeg', 'jpg', 'png', 'webp'])], storage=PublicMediaStorage(), blank=True, null=True,)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated at')
    service_fee = models.SmallIntegerField(verbose_name='Service fee percentage', null=True) # field to save service fee on the moment when discussion creating
    is_active = models.BooleanField(default=False, verbose_name='Is active')
    is_full = models.BooleanField(default=False, verbose_name='Is full') # flag to tag discussons wich riched max num of participants
    is_checked = models.BooleanField(default=False, verbose_name='Is checked') # flag to tag discussion for wich push notifications were sent
    paid = models.BooleanField(default=False, verbose_name='Paid') # flag to tag discussion wich were paid by initiator
    deposit_transferred = models.BooleanField(default=False, verbose_name='Deposit transferred') # flag to tag discussions wich rich min part and sum were paid to place owner
    holding_agreed = models.BooleanField(default=False, verbose_name='Holding agreed') # flag to tag discussion wich were approved by place owner
    on_review = models.BooleanField(default=True, verbose_name='On review') # flag to tag discussion wich are on review of place owner
    
    def save(self, *args, **kwargs):
        if self.id == None:
            if self.number_of_participants < self.place.minimum_available_seats:
                raise MinLimitOfSeatsError
            elif self.number_of_participants > self.place.maximum_available_seats:
                raise MaxLimitOfSeatsError
        if not self.image:
            self.image = self.theme.image
        return super(Discussion, self).save(*args, **kwargs)

    def __str__(self) -> str:
        return f'ID: {self.pk}  Topic: {self.topic}'

    class Meta:
        verbose_name = 'Discussion'
        verbose_name_plural = 'Discussions'


class ParticipantsOfDiscussion(AppBaseModel):
    """ParticipantsOfDiscussion model"""

    user = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, verbose_name='User', related_name='participant_of_discussion')
    discussion = models.ForeignKey(
        Discussion, on_delete=models.CASCADE, verbose_name='Discussion', related_name='participant_of_discussion')
    number_of_paid_seats = models.SmallIntegerField(
        default=1, verbose_name="Number of paid seats")
    created_at = models.DateTimeField(
        verbose_name='Created at', auto_now_add=True)
    is_initiator = models.BooleanField(
        verbose_name="Is initiator", default=False)

    def save(self, *args, **kwargs):

        if self.id == None:
            discussions: list = list(ParticipantsOfDiscussion.objects.filter(
                discussion_id=self.discussion.id).values())
            confirmed_participants = 0
            for discussion in discussions:
                confirmed_participants += discussion.get(
                    'number_of_paid_seats')
            confirmed_participants + self.number_of_paid_seats
            if confirmed_participants > self.discussion.number_of_participants:
                Discussion.objects.filter(
                    id=self.discussion).update(is_full=True)
                raise DiscussionIsFullError
            elif confirmed_participants == self.discussion.number_of_participants:
                discussion.is_full == True
        return super(ParticipantsOfDiscussion, self).save(*args, **kwargs)

    def __str__(self) -> str:
        return f'pk: {self.pk}, user_pk:{self.user_id}, discussion_pk: {self.discussion_id}'

    class Meta:
        verbose_name = 'Participants Of Discussion'
        verbose_name_plural = 'Participant Of Discussion'
