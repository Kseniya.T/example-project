# Generated by Django 4.1.3 on 2022-11-21 11:26

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('discussion_app', '0004_remove_discussion_date_discussion_created_at_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='discussion',
            name='image',
            field=models.ImageField(blank=True, max_length=250, null=True, upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], verbose_name='Image'),
        ),
    ]
