# Generated by Django 4.1.3 on 2022-11-17 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('discussion_app', '0002_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='discussion',
            name='theme',
            field=models.CharField(choices=[('Other', 'Other'), ('Cinema', 'Cinema'), ('Science', 'Science'), ('Art', 'Art'), ('Music', 'Music'), ('Theatre', 'Theatre'), ('Food', 'Food')], default='Other', max_length=1500, verbose_name='Theme'),
        ),
    ]
