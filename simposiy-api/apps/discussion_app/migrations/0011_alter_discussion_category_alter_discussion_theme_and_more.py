# Generated by Django 4.1.3 on 2022-12-02 07:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('discussion_app', '0010_discussiontheme_alter_discussion_theme'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discussion',
            name='category',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to='discussion_app.discussioncategory', verbose_name='Category of discussion'),
        ),
        migrations.AlterField(
            model_name='discussion',
            name='theme',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to='discussion_app.discussiontheme', verbose_name='Theme'),
        ),
        migrations.AlterField(
            model_name='discussion',
            name='type',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to='discussion_app.discussiontype', verbose_name='Type of discussion'),
        ),
    ]
