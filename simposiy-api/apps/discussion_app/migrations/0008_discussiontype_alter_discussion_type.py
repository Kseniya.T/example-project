# Generated by Django 4.1.3 on 2022-12-01 14:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('discussion_app', '0007_discussion_is_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiscussionType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Type of discussion')),
            ],
            options={
                'verbose_name': 'Discussion type',
                'verbose_name_plural': 'Discussion types',
            },
        ),
        migrations.AlterField(
            model_name='discussion',
            name='type',
            field=models.OneToOneField(default='Group meeting', on_delete=django.db.models.deletion.CASCADE, related_name='place', to='discussion_app.discussiontype', verbose_name='Type of discussion'),
        ),
    ]
