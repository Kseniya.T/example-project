# Generated by Django 4.1.2 on 2022-11-16 11:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('place_app', '0001_initial'),
        ('discussion_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='discussion',
            name='initiator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to=settings.AUTH_USER_MODEL, verbose_name='Initiator'),
        ),
        migrations.AddField(
            model_name='discussion',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discussion', to='place_app.place', verbose_name='Location'),
        ),
    ]
