from django.apps import AppConfig


class DiscussionAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.discussion_app'

    def ready(self):
        import apps.discussion_app.signals
