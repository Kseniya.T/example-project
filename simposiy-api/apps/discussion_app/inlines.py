from django.contrib import admin

from apps.discussion_app.models import ParticipantsOfDiscussion


class ParticipantsOfDiscussionStackedInline(admin.StackedInline):
    """ParticipantsOfDiscussionStackedInline of Discussion"""
    model = ParticipantsOfDiscussion
    verbose_name = 'participant of discussion'
    verbose_name_plural = 'participants of discussion'
    extra = 0
    readonly_fields = ('user', 'number_of_paid_seats', 'is_initiator',)
    show_change_link = True
    can_delete = False
    
    def has_add_permission(self, request, obj=None):
        return False