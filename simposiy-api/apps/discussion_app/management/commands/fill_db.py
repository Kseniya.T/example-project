import logging
from django.core.management import BaseCommand
from django.apps import apps
from django.db import transaction


class Command(BaseCommand):
    """Comand for prepopulating discussiontype, discussioncategory, discussiontheme and placetype tables with data"""

    help = "Prepopulating discussiontype, discussioncategory, discussiontheme and placetype tables with data"

    @transaction.atomic
    def handle(self, *args, **options):
        self.fill_place_type()
        self.fill_discussion_type()
        self.fill_discussion_category()
        self.fill_discussion_theme()
        logging.info('Work done!')

    def fill_place_type(self):
        """Creating the PlaceType objects"""

        PlaceType = apps.get_model('place_app', 'PlaceType')
        PlaceType.objects.create(
            name='Cafe',
        )
        PlaceType.objects.create(
            name='Anti Cafe',

        )
        PlaceType.objects.create(
            name='Restaurant',

        )
        PlaceType.objects.create(
            name='Bar',

        )
        PlaceType.objects.create(
            name='Cigar Lounge',

        )
        logging.info('Place types were added successfully')

    def fill_discussion_type(self):
        """Creating the DiscussionType objects"""

        DiscussionType = apps.get_model('discussion_app', 'DiscussionType')
        DiscussionType.objects.create(
            name='Group meeting',
        )
        DiscussionType.objects.create(
            name='One-on-one conversation',

        )
        logging.info('Discussion types were added successfully')

    def fill_discussion_category(self):
        """Creating the DiscussionCategory objects"""

        DiscussionCategory = apps.get_model(
            'discussion_app', 'DiscussionCategory')
        DiscussionCategory.objects.create(
            name='Business meeting',
        )
        DiscussionCategory.objects.create(
            name='Formal meeting',
        )
        DiscussionCategory.objects.create(
            name='Test ideas',
        )
        DiscussionCategory.objects.create(
            name='Support group',
        )
        DiscussionCategory.objects.create(
            name='Game club',
        )
        logging.info('Discussion categories were added successfully')

    def fill_discussion_theme(self):
        """Creating the DiscussionTheme objects"""

        DiscussionTheme = apps.get_model('discussion_app', 'DiscussionTheme')
        DiscussionTheme.objects.create(
            name='Psychology',
        )
        DiscussionTheme.objects.create(
            name='Family',
        )
        DiscussionTheme.objects.create(
            name='Education',
        )
        DiscussionTheme.objects.create(
            name='Sports and health',
        )
        DiscussionTheme.objects.create(
            name='Religion and spirituality',
        )
        DiscussionTheme.objects.create(
            name='Mystic',
        )
        DiscussionTheme.objects.create(
            name='Festive meetings',
        )
        DiscussionTheme.objects.create(
            name='Movies and Series',
        )
        DiscussionTheme.objects.create(
            name='Quests, intellectual games',
        )
        DiscussionTheme.objects.create(
            name='Art',
        )
        logging.info('Discussion themes were added successfully')
