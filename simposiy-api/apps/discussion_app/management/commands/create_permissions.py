from sys import stdout
from typing import Dict, List

from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.db import transaction

from apps.discussion_app.choices import GroupsEnum


GROUPS = {
    GroupsEnum.ADMINISTRATION.value: {
        # Permissions for discussion_app
        "discussiontype": ["view", "add"],
        "discussioncategory": ["view", "add", "change", "delete"],
        "discussiontheme": ["view", "add", "change", "delete"],
        "discussion": ["view"],
        "participantsofdiscussion": ["view"],

        # Permissions for notification_app
        "notification": ["view"],
        "applicationconfig":["view", "add", "change"],

        # Permissions for payment_app
        "chargedata": ["view"],
        "order": ["view"],
        "refund": ["view"],

        # Permissions for place_app
        "placetype": ["view", "add", "change", "delete"],
        "place": ["view"],
        "placeopeninghours": ["view"],
        "dishes": ["view"],
        "placeimages": ["view"],

        # Permissions for user_app
        "customuser": ["view"],
        "profile": ["view"],
        "interests": ["view", "add", "change", "delete"],
        "userinterests": ["view"],
    },
}


class Command(BaseCommand):
    help = "Creating permissions for groups"

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--force-create',
            action='store_true',
            # type=bool,
            default=False,
            required=False,
            help='Force creation'
        )

    def handle(self, *args, **options):
        if options.get('force_create'):
            self.delete_existing_permissions()
        self.create_permissions()
        self.stdout.write('Work done!')

    def delete_existing_permissions(schema_editor):
        for group_name in GROUPS:
            new_group, created = Group.objects.get_or_create(name=group_name)
            new_group.permissions.clear()

    @transaction.atomic
    def create_permissions(schema_editor):
        for group_name in GROUPS:
            new_group, created = Group.objects.get_or_create(name=group_name)
            models: Dict[str, List[str]] = GROUPS.get(group_name)
            for app_model in models:
                permissions: List[str] = models.get(app_model)
                for permission_name in permissions:
                    # Generate permission name as Django would generate it
                    codename = f"{permission_name}_{app_model}"
                    name = f"Can {permission_name} {app_model}"
                    content_type = ContentType.objects.get(app_label__in=(
                        'discussion_app',
                        'notification_app',
                        'payment_app',
                        'place_app',
                        'user_app',
                    ), model=app_model)

                    try:
                        model_add_perm, created = Permission.objects.get_or_create(
                            codename=codename, content_type=content_type)
                    except Permission.DoesNotExist:
                        stdout.write.warning(
                            f"Permission not found with name '{name}'")
                        continue
                    new_group.permissions.add(model_add_perm)
