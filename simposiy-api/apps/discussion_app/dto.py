from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, Optional
from moneyed import Money

from apps.discussion_app.models import DiscussionTheme
from apps.user_app.models import CustomUser


@dataclass
class CreateDiscussionDTO:
    """DTO for creating discussion case"""

    place: int
    type: int
    category: int
    theme: int
    topic: str
    dt_start: datetime
    number_of_participants: int
    description: str | None = None
    is_active: bool = False
    is_full: bool = False
    on_review: bool = True
    holding_agreed: bool = False
    paid: bool = False
    image: Any | None = None
    id: int| None  = None


@dataclass
class UpdateDiscussionDTO:
    """DTO for updating discussion case"""

    theme: int | None = None
    topic: str | None = None
    description: str | None = None
    image: Any | None = None
    place: int | None = None
    dt_start: datetime | None = None


@dataclass
class PaidDetailsDTO:
    """DTO for RetrieveDiscussionDTO"""

    check_number: str
    payment_status: str
    payment_time: datetime
    total_payment: Money
    pdf: Any | None = None


@dataclass
class RetrieveDiscussionDTO:
    """DTO for RetrieveDiscussionSerializer"""

    id: int
    type: int
    topic: str
    theme: int
    created_at: datetime
    description: str
    place: int
    dt_start: datetime
    category: int
    number_of_participants: int
    is_active: bool
    is_full: bool
    paid: bool
    on_review: bool
    holding_agreed: bool
    initiator: CustomUser
    confirmed_participants_number: int
    participant_of_discussion: list[Dict]
    image: Any | None = None
    payment_link: str | None = None
    paid_details: PaidDetailsDTO | None = None


@dataclass
class FilterDiscussionsDTO():
    """DTO for FilterDiscussionsApiView"""

    def __init__(self, **kwargs):
        # filter params
        self.date_from = kwargs.get('date_from', [None])[0]
        self.date_to = kwargs.get('date_to', [None])[0]
        self.time_from = kwargs.get('time_from', [None])[0]
        self.time_to = kwargs.get('time_to', [None])[0]

        type_ids = kwargs.get('type', [None])[0]
        category_ids = kwargs.get('category', [None])[0]
        theme_ids = kwargs.get('theme', [None])[0]
        self.type_ids = list(map(int, type_ids.split(','))) if type_ids else []
        self.category_ids = list(
            map(int, category_ids.split(','))) if category_ids else []
        self.theme_ids = list(
            map(int, theme_ids.split(','))) if theme_ids else []

        self.cost_from = kwargs.get('cost_from', [None])[0]
        self.cost_to = kwargs.get('cost_to', [None])[0]
        
        # search param
        self.search = kwargs.get('search', [None])[0]

@dataclass
class CreateUserThemesDTO:
    """DTO for create user themes"""

    themes: list[int]
    user: Optional[CustomUser] | None = None
    id: int | None = None


@dataclass
class UserThemesDTO:
    """DTO for user themes"""

    themes: list[DiscussionTheme]
    user: int


@dataclass
class DeleteUserThemesDTO:
    """DTO for delete user themes"""

    id: int


# ONLY FOR TESTS

@dataclass
class AutoPaymentDTO:
    """DTO for CreateParticipantOfDiscussionSerializer"""

    discussion: int
