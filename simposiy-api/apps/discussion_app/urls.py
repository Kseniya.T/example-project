from django.urls import path
from .views import (CreateDiscussionApiView, AutoPaymentView,
                    CreateUserThemesAPIView, DeleteUserThemesApiView,
                    DiscussionCategoryDetailRetrieveAPIView, DiscussionDetailRetrieveAPIView,
                    DiscussionTypeDetailRetrieveAPIView, FilterSearchDiscussionApiView, ListActiveDiscussionOfPlaceView,
                    ListActiveDiscussionView, ListDiscussionCategoryView,
                    ListOfUserThemesView, MyTicketsView, ListDiscussionTypeView,
                    ListUserParticipationInitiationDiscussionsView,
                    ListDiscussionThemeView, DiscussionThemeDetailRetrieveAPIView,
                    ListInitiatedDiscussionOfUserView, ListPopularDiscussions,
                    ListSomeUserParticipationDiscussionsView, ListUserParticipationDiscussionsView, TestCreateParticipant,
                    UpdateDiscussionApiView, TestUpdateDiscussionHoldingAgreedApiView)


app_name = 'discussion_app'

urlpatterns = [
    # Types, categories and themes of discussions
    path('discussion/type-list/', ListDiscussionTypeView.as_view(),
         name='discussion-types-list'),
    path('discussion/type/<int:pk>',
         DiscussionTypeDetailRetrieveAPIView.as_view(), name='discussion-type-details'),
    path('discussion/category-list/', ListDiscussionCategoryView.as_view(),
         name='discussion-category-list'),
    path('discussion/category/<int:pk>',
         DiscussionCategoryDetailRetrieveAPIView.as_view(), name='discussion-category-details'),
    path('discussion/theme-list/', ListDiscussionThemeView.as_view(),
         name='discussion-theme-list'),
    path('discussion/theme/<int:pk>',
         DiscussionThemeDetailRetrieveAPIView.as_view(), name='discussion-theme-details'),

    # discussion create, update, get details
    path('discussion/create/', CreateDiscussionApiView.as_view(),
         name='discussion-create'),
    path('discussion/<int:pk>/update/', UpdateDiscussionApiView.as_view(),
         name='discussion-update'),
    path('discussion/details/<int:pk>',
         DiscussionDetailRetrieveAPIView.as_view(), name='discussion-details'),

    # filters and lists
    path('activediscussions/list/', ListActiveDiscussionView.as_view(),
         name='active-discussions-list'),
    path('populardiscussions/list/', ListPopularDiscussions.as_view(),
         name='popular-discussions-list'),
    path('place/<int:pk>/discussions/list/',
         ListActiveDiscussionOfPlaceView.as_view(), name='place-discussions-list'),
    path('user/discussions/list/', ListUserParticipationInitiationDiscussionsView.as_view(),
         name='participant-initiator-of-discussions-list'),
    path('user/discussions/participation/list/', ListUserParticipationDiscussionsView.as_view(),
         name='participant-of-discussions-list'),
    path('profile/<int:pk>/discussions/list/', ListSomeUserParticipationDiscussionsView.as_view(),
         name='other_user_participant-of-discussions-list'),
    path('discussions/iniciated/list/', ListInitiatedDiscussionOfUserView.as_view(),
         name='initiator-of-discussions-list'),
    path('discussion/filter/', FilterSearchDiscussionApiView.as_view(), name='discussion-filter'),

    # tickets
    path('user/mytickets/', MyTicketsView.as_view(), name='my-tickets'),
    
    # user themes
    path('user/add-themes/', CreateUserThemesAPIView.as_view(),
         name='user-add-themes'),
    path('user/list-themes/', ListOfUserThemesView.as_view(),
         name='user-list-themes'),
    path('user/delete-themes/', DeleteUserThemesApiView.as_view(),
         name='user-delete-themes'),
    
    # url for test auto payment with Selenium
    path('autopayment/create/', AutoPaymentView.as_view(), name='create-autopayment'),
    
    # url for test update holding_agreed  field to True
    path('discussion/<int:pk>/update-holding-agreed/', TestUpdateDiscussionHoldingAgreedApiView.as_view(), name='update-holding-agreed '),
    path('discussion/test_participant/create/', TestCreateParticipant.as_view(), name='create-test-participant'),
]
