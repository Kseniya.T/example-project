from typing import Tuple, Type
from django.contrib import admin
from apps.discussion_app.inlines import ParticipantsOfDiscussionStackedInline

from apps.discussion_app.models import (
    Discussion, DiscussionCategory, DiscussionTheme, DiscussionType, ParticipantsOfDiscussion, UserThemes)


@admin.register(DiscussionType)
class DiscussionTypeAdmin(admin.ModelAdmin):
    model_class: Type[DiscussionType] = DiscussionType
    list_display: Tuple[str] = ('id',
                                'name',
                                'description',
                                'image',)


@admin.register(DiscussionCategory)
class DiscussionCategoryAdmin(admin.ModelAdmin):
    model_class: Type[DiscussionCategory] = DiscussionCategory
    list_display: Tuple[str] = ('id',
                                'name',
                                'description',
                                'image',)


@admin.register(DiscussionTheme)
class DiscussionThemeAdmin(admin.ModelAdmin):
    model_class: Type[DiscussionTheme] = DiscussionTheme
    list_display: Tuple[str] = ('id',
                                'name',
                                'image',
                                'is_golden',
                                'is_top',)


@admin.register(Discussion)
class DiscussionAdmin(admin.ModelAdmin):
    model_class: Type[Discussion] = Discussion
    list_display: Tuple[str] = ('id',
                                'initiator',
                                'place',
                                'type',
                                'category',
                                'theme',
                                'topic',
                                'get_place_deposit',
                                'dt_start',
                                'created_at',
                                'updated_at',
                                'on_review',
                                'holding_agreed',
                                'is_active',
                                'paid',
                                'is_full',
                                'deposit_transferred',
                                'is_checked',
                                'image',
                                )

    def get_place_deposit(self, obj):
        return obj.place.deposit
    get_place_deposit.short_description = 'Deposit'

    inlines = (ParticipantsOfDiscussionStackedInline, )
    search_fields: Tuple[str] = ('id', 'topic',)
    list_filter: Tuple[str] = ('type',
                               'category',
                               'theme',
                               'holding_agreed',
                               'is_active',
                               'paid',
                               'is_full',
                               'deposit_transferred',
                               )


@admin.register(ParticipantsOfDiscussion)
class ParticipantsOfDiscussionAdmin(admin.ModelAdmin):
    model_class: Type[ParticipantsOfDiscussion] = ParticipantsOfDiscussion
    list_display: Tuple[str] = ('id',
                                'user_id',
                                'discussion_id',
                                'number_of_paid_seats',
                                'created_at',
                                'is_initiator'
                                )
    list_filter: Tuple[str] = ('discussion_id',)


@admin.register(UserThemes)
class UserThemesAdmin(admin.ModelAdmin):
    model_class: Type[UserThemes] = UserThemes
    list_display: Tuple[str] = ('id',
                                'user',
                                'themes',
                                'created_at'
                                )
