from django.db.models import Sum, Q
from django.utils import timezone
from apps.discussion_app.enums import NotificationEventEnum

from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion
from apps.notification_app.models import ApplicationConfig, Notification
from apps.payment_app.models import ChargeData
from apps.rabbitmq_app.enums import QueuesEnum
from apps.rabbitmq_app.producer import ProducerRabbitMQ
from apps.user_app.models import CustomUser
from core.celery import app
from core.settings import EXCHANGE_PUSH_NOTIFICATION_INBOUND, RABBITMQ_HOST, RABBITMQ_PRODUCER_PASSWORD, RABBITMQ_PRODUCER_USER
from repositories.notification_repository import NotificationRepository
from services.stripe_service.stripe_service import StripeService
from repositories.init import push_notification_repository


@app.task
def check_discussion_start_date():
    """Function for check discussion start date and if it has passed already change flag is_active to False"""

    now = timezone.now()
    discussions = Discussion.objects.filter(is_active=True)
    for discussion in discussions:
        if discussion.dt_start < now:
            discussion.update_fields(is_active=False)
    return True


# TODO remove this task and check if discussion is full only after user add as participant
@app.task
def check_discussion_is_full():
    """Function for checking if active discussions if full"""

    discussions = Discussion.objects.filter(
        Q(is_active=True) & Q(is_full=False))
    for discussion in discussions:
        confirmed_participants: int = ParticipantsOfDiscussion.objects.filter(
            discussion_id=discussion.id).count()
        number_of_available_seats: int = discussion.number_of_participants - \
            confirmed_participants

        if number_of_available_seats == 0:
            discussion.update_fields(is_full=True)
            # notify initiator
            data = {'event': NotificationEventEnum.MAX_PARTICIPANTS_RICHED,
                    'discussion': discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)


@app.task
def check_minimum_number_participants():
    """Function for checking if is minimum number of participants of discussions were reached"""

    all_discussions_with_participants = Discussion.objects.prefetch_related('participant_of_discussion').select_related('place').filter(
        Q(is_active=True) & Q(is_checked=False)).distinct()
    now = timezone.now()

    discussion_riched_min_participants = []

    for discussion in all_discussions_with_participants:
        participants_of_discussion = discussion.participant_of_discussion.filter(
            number_of_paid_seats=1)
        total_paid_seats = participants_of_discussion.aggregate(
            total_paid_seats=Sum('number_of_paid_seats'))
        place = discussion.place
        discussion_date_start = discussion.dt_start
        time_left = discussion_date_start - now
        total_seconds = time_left.total_seconds()
        total_minutes = int(total_seconds / 60)
        total_paid_seats = total_paid_seats.get('total_paid_seats')

        if total_paid_seats == None:
            total_paid_seats = 0

        if (total_paid_seats >= place.minimum_available_seats and total_minutes <= 30) or discussion.is_full:
            # TODO send push notification to Sam that nedd to make payout to location owner
            discussion_riched_min_participants.append(discussion)

        if total_paid_seats < place.minimum_available_seats and total_minutes <= 30:

            user = discussion.initiator
            try:
                charge = ChargeData.objects.prefetch_related(
                    'order').get(user=user, discussion=discussion)
            except ChargeData.DoesNotExist:
                charge = None

            if charge and charge.order.refunded == False:
                charge_id = charge.charge_id
                refund_dto = StripeService.create_refund_dto(
                    charge_id=charge_id)
                StripeService.create_refund(dto=refund_dto)
                # TODO send push notification abt refund

            discussion.update_fields(is_acive=False)
            # TODO send push notification abt deactivation of discusstion

        if total_minutes < 0:
            discussion.update_fields(is_acive=False)

    # Sending notifications(check if user has push_token)
    if discussion_riched_min_participants:
        data = {'event': NotificationEventEnum.MIN_PARTICIPANTS_RICHED,
                'discussions': discussion_riched_min_participants}
        ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                 host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)


@app.task
def check_response_from_place_owner():
    """
        Task to check If within 24 hours the place owner has not responded to the initiator's request,
        then send a Notification to the initiator
    """
    discussions_pending_for_host = Discussion.objects.filter(Q(holding_agreed=False) & Q(on_review=True))
    now = timezone.now()

    for discussion in discussions_pending_for_host:
        discussion_updated_at = discussion.updated_at
        time_passed = now - discussion_updated_at
        hours = time_passed.seconds // 3600

        if time_passed.days >= 1 and now < discussion.dt_start:
            # tag that discussion not on review anymore and send notification to initiator
            discussion.update_fields(on_review=False)
            
            data = {'event': NotificationEventEnum.PENDING_HOST_TIMED_OUT,
                    'discussion': discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)
