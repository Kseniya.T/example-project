from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.core.cache import cache

from .models import DiscussionType, DiscussionCategory, DiscussionTheme


@receiver(post_delete, sender=DiscussionType)
@receiver(post_save, sender=DiscussionType)
def invalidate_discussion_types_cache(sender, instance, **kwargs):
    # Invalidate the cache for the given key
    cache.delete('discussion_types')


@receiver(post_delete, sender=DiscussionCategory)
@receiver(post_save, sender=DiscussionCategory)
def invalidate_discussion_categories_cache(sender, instance, **kwargs):
    # Invalidate the cache for the given key
    cache.delete('discussion_categories')


@receiver(post_delete, sender=DiscussionTheme)
@receiver(post_save, sender=DiscussionTheme)
def invalidate_discussion_themes_cache(sender, instance, **kwargs):
    # Invalidate the cache for the given key
    cache.delete('discussion_themes')
