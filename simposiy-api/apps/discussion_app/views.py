import datetime
from django.utils import timezone
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED
from rest_framework.permissions import AllowAny
from django.db.models import Count, Q
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from rest_framework.pagination import LimitOffsetPagination
from rest_framework import views
from apps.discussion_app.enums import NotificationEventEnum

from apps.discussion_app.dto import (AutoPaymentDTO, CreateDiscussionDTO,
                                     CreateUserThemesDTO, FilterDiscussionsDTO,
                                     RetrieveDiscussionDTO, UpdateDiscussionDTO, UserThemesDTO)
from apps.discussion_app.models import (Discussion, DiscussionCategory,
                                        DiscussionTheme, DiscussionType,
                                        ParticipantsOfDiscussion, UserThemes)
from apps.discussion_app.serializers import (
    ActiveDiscussionSerializer, ActiveDiscussionSerializerFiltered,
    AutoPaymentSerializer, CreateDiscussionSerializer,
    CreateUserThemesSerializer, DeleteUserThemesSerializer,
    DiscussionCategorySerializer, DiscussionListSerializer, DiscussionRetrieveSerializer, DiscussionThemeSerializer,
    DiscussionTypeSerializer, ListUserThemesSerializer, MyTicketsSerializer,
    ParticipantOfDiscussionSerializer, ResponseCreateDiscussionSerializer, UpdateDiscussionSerializer, UserThemesSerializer)
from apps.notification_app.models import Notification
from apps.payment_app.dto import DiscussionAuthorizePaymentDTO
from apps.payment_app.models import Order
from apps.payment_app.serializers import DiscussionPaymentSerializer
from apps.payment_app.tasks import check_charge_data, check_unconfirmed_orders
from apps.rabbitmq_app.enums import QueuesEnum
from apps.rabbitmq_app.producer import ProducerRabbitMQ
from apps.user_app.models import CustomUser, Profile
from core.settings import EXCHANGE_PUSH_NOTIFICATION_INBOUND, RABBITMQ_HOST, RABBITMQ_PRODUCER_PASSWORD, RABBITMQ_PRODUCER_USER
from repositories.discussion_app_repository import DiscussionAppRepository
from repositories.payment_app_repository import PaymentAppRepository
from utils.exceptions import DiscussionUpdateError, ObjectNotFoundError
from utils.generics.base_generics import (BaseGenericAPIView, BaseListAPIView,
                                          BaseRetrieveCreateAPIView, CustomRetrieveAPIView)
from utils.permissions import IsAuthenticated
from utils.response import ErrorResponse, SuccessResponse
from repositories.notification_repository import NotificationRepository
from repositories.init import push_notification_repository


class ListDiscussionTypeView(BaseListAPIView):
    """View of discussion types list"""

    serializer_class = DiscussionTypeSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        types = cache.get('discussion_types')
        if types is None:
            types = DiscussionType.objects.all()
            cache.set('discussion_types', types)
        return types


class DiscussionTypeDetailRetrieveAPIView(CustomRetrieveAPIView):
    """View for view discussion type details"""

    serializer_class = DiscussionTypeSerializer
    permission_classes = [AllowAny,]
    lookup_field: int = 'pk'
    not_found_message: str = 'There is no such discussion type registered'
    queryset = DiscussionType.objects.all()


class ListDiscussionCategoryView(BaseListAPIView):
    """View of discussion categories list"""

    serializer_class = DiscussionCategorySerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        categories = cache.get('discussion_categories')
        if categories is None:
            categories = DiscussionCategory.objects.all()
            cache.set('discussion_categories', categories)
        return categories


class DiscussionCategoryDetailRetrieveAPIView(CustomRetrieveAPIView):
    """View for view discussion category details"""

    serializer_class = DiscussionCategorySerializer
    permission_classes = [AllowAny,]
    lookup_field: int = 'pk'
    not_found_message: str = 'There is no such discussion category registered'
    queryset = DiscussionCategory.objects.all()


class ListDiscussionThemeView(BaseListAPIView):
    """View for discussion themes list"""

    serializer_class = DiscussionThemeSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        themes = cache.get('discussion_themes')
        if themes is None:
            themes = DiscussionTheme.objects.all()
            cache.set('discussion_themes', themes)
        return themes


class DiscussionThemeDetailRetrieveAPIView(CustomRetrieveAPIView):
    """View for view discussion theme details"""

    serializer_class = DiscussionThemeSerializer
    permission_classes = [AllowAny,]
    lookup_field: int = 'pk'
    not_found_message: str = 'There is no such discussion theme registered'
    queryset = DiscussionTheme.objects.all()


class CreateDiscussionApiView(BaseGenericAPIView):
    """View for create discussion"""

    serializer_class = CreateDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        initiator: CustomUser = request.user.user
        serializer: CreateDiscussionSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        discussion_dto: CreateDiscussionDTO = serializer.dto

        initiator_is_place_owner = DiscussionAppRepository.checks_before_create_discussion(
            initiator=initiator, discussion_dto=discussion_dto)

        new_discussion: Discussion = DiscussionAppRepository.create_discussion(
            initiator=initiator, dto=discussion_dto, initiator_is_place_owner=initiator_is_place_owner)

        # After initiator created new discussion send host request to place owner
        if not initiator_is_place_owner:
            data = {'event': NotificationEventEnum.HOST_REQUEST, 'discussion': new_discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        _serializer: ResponseCreateDiscussionSerializer = ResponseCreateDiscussionSerializer(
            new_discussion)

        return SuccessResponse(
            status=HTTP_201_CREATED,
            detail=f'New discussion was successfully created',
            obj=_serializer.data
        )


class UpdateDiscussionApiView(BaseGenericAPIView):
    """View for update discussions"""

    serializer_class = UpdateDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def patch(self, request, *args, pk=None, **kwargs):
        discussion_pk: int = self.kwargs['pk']
        user: CustomUser = request.user.user

        try:
            discussion: Discussion = Discussion.objects.select_related(
                'type', 'category', 'theme', 'initiator', 'place').get(pk=discussion_pk)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError

        serializer: UpdateDiscussionSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)

        for field in request.data.keys():
            if field not in serializer.fields:
                raise DiscussionUpdateError(
                    f"Field '{field}' can not be edited.")

        dto: UpdateDiscussionDTO = serializer.dto

        # check discussion and user before update
        DiscussionAppRepository.check_discussion_for_update(
            user=user, discussion=discussion)

        updated_discussion, place_updated = DiscussionAppRepository.partial_update_discussion(
            discussion=discussion, dto=dto)
        
        if place_updated:
            updated_discussion.update_fields(on_review=True)
            data = {'event': NotificationEventEnum.HOST_REQUEST, 'discussion': updated_discussion}
            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND), exchange=EXCHANGE_PUSH_NOTIFICATION_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        
        _serializer = CreateDiscussionSerializer(updated_discussion)
        return SuccessResponse(status=HTTP_200_OK, detail='Discussion information was successfully updated', obj=_serializer.data)


class DiscussionDetailRetrieveAPIView(BaseRetrieveCreateAPIView):
    """View for view details of discussion"""

    serializer_class = DiscussionRetrieveSerializer
    permission_classes = [AllowAny, ]

    def get(self, request, *args, **kwargs):
        try:
            user = request.user.user
        except AttributeError:
            user = None
        pk = self.kwargs['pk']
        filter_time = timezone.now() - datetime.timedelta(minutes=28)
        dto: RetrieveDiscussionDTO = DiscussionAppRepository.retrieve_discussion(
            pk=pk, user=user, filter_time=filter_time)
        serializer: DiscussionRetrieveSerializer = self.serializer_class(dto)
        return SuccessResponse(
            status=HTTP_200_OK,
            obj=serializer.data
        )

    # def post(self, request, *args, **kwargs):
    #     discussion_pk = self.kwargs['pk']
    #     user = request.user.user
    #     # check user and discussion
    #     charge_data, discussion = DiscussionAppRepository.check_before_refund(user=user, discussion_pk=discussion_pk)

    #     # do refund
    #     create_refund_dto: CreateRefundDTO = StripeService.create_refund_dto(
    #             charge_id=charge_data.charge_id)
    #     StripeService.create_refund(dto=create_refund_dto)

    #     # remove user from participants
    #     DiscussionAppRepository.remove_user_from_participants(user=user, discussion=discussion)

    #     return SuccessResponse(
    #         status=HTTP_200_OK, detail='Request for refund was successfull. You will see the refund as a credit approximately 5-10 business days later, depending upon the bank'
    #     )


class FilterSearchDiscussionApiView(BaseGenericAPIView):
    """View for filter place by date, time, cost and type, category, theme and for search discussion by topic"""

    serializer_class = ActiveDiscussionSerializerFiltered
    permission_classes = [AllowAny]
    pagination_class = LimitOffsetPagination

    def get(self, request):
        filter_params = request.query_params
        dto = FilterDiscussionsDTO(**filter_params)

        queryset = DiscussionAppRepository.filter_search_discussions(dto=dto)
        ordered_queryset = queryset.order_by('-created_at')
        page = self.paginate_queryset(ordered_queryset)
        serializer: ActiveDiscussionSerializerFiltered = self.serializer_class(
            page, many=True)
        return self.get_paginated_response(serializer.data)


class ListPopularDiscussions(BaseListAPIView):
    """View for list most popular or last 10 discussions"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        try:
            preresult = (
                Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                    'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
                .select_related('place', 'initiator', 'type', 'category', 'theme')
                .filter(Q(is_active=True))
                .annotate(confirmed_participant_number=Count('participant_of_discussion'))
            )
            result = preresult.order_by(
                '-confirmed_participant_number', '-created_at')
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        return result[:10]


class ListActiveDiscussionView(BaseListAPIView):
    """View for list of all active discussions with participants"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [AllowAny, ]
    pagination_class = LimitOffsetPagination

    def get(self, request):
        theme_ids = request.GET.getlist('theme_ids')

        if theme_ids:
            theme_ids_list = [int(theme_id)
                              for theme_id in theme_ids[0].split(',')]
            preresult = (
                Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                    'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
                .select_related('place', 'initiator', 'type', 'category', 'theme')
                .filter(Q(is_active=True) & Q(theme_id__in=theme_ids_list))
                .annotate(confirmed_participant_number=Count('participant_of_discussion'))
            )
            result = preresult.order_by('-created_at')
            page = self.paginate_queryset(result)
            serializer: ActiveDiscussionSerializer = self.serializer_class(
                page, many=True)
            return self.get_paginated_response(serializer.data)

        try:
            preresult = (
                Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                    'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
                .select_related('place', 'initiator', 'type', 'category', 'theme')
                .filter(is_active=True)
                .annotate(confirmed_participant_number=Count('participant_of_discussion')))
        except Exception as e:
            raise ObjectNotFoundError(e)

        result = preresult.order_by('-created_at')
        page = self.paginate_queryset(result)
        serializer: ActiveDiscussionSerializer = self.serializer_class(
            page, many=True)
        return self.get_paginated_response(serializer.data)


class ListActiveDiscussionOfPlaceView(BaseListAPIView):
    """View for list of all active discussions for particular place"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        pk = self.kwargs['pk']
        try:
            result = (
                Discussion.objects.prefetch_related('initiator__user_interests', 'initiator__profile',
                                                    'participant_of_discussion', 'participant_of_discussion__user__profile', 'place__type', 'place__dishes')
                .select_related('place', 'initiator', 'type', 'category', 'theme')
                .filter(is_active=True, place_id=pk)
                .annotate(confirmed_participant_number=Count('participant_of_discussion')))

        except Exception as e:
            raise ObjectNotFoundError(e)
        return result.order_by('-created_at')


class ListInitiatedDiscussionOfUserView(BaseListAPIView):
    """View for list of all discussions user initiated"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):

        initiator = self.request.user.user
        discussions = (
            Discussion.objects.prefetch_related(
                'participant_of_discussion', 'participant_of_discussion__user__profile')
            .select_related('place', 'initiator', 'type', 'category', 'theme')
            .filter(initiator=initiator)
            .annotate(confirmed_participant_number=Count('participant_of_discussion')))
        return discussions.order_by('-created_at')


class MyTicketsView(BaseListAPIView):
    """View for list of user tickets"""

    serializer_class = MyTicketsSerializer
    permission_classes = [IsAuthenticated, ]
    pagination_class = LimitOffsetPagination

    def get(self, request):
        past: bool = request.GET.get('past')
        user: CustomUser = self.request.user.user
        result = DiscussionAppRepository.get_active_tickets_queryset(user=user)
        if past == 'true':
            result = DiscussionAppRepository.get_past_tickets_queryset(
                user=user)

        page = self.paginate_queryset(result.order_by('-created_at'))
        serializer = self.serializer_class(page, many=True)
        return self.get_paginated_response(serializer.data)


class ListUserParticipationInitiationDiscussionsView(BaseListAPIView):
    """View for list of all discussions where user is partipiciant and initiator"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user: CustomUser = self.request.user.user
        result = DiscussionAppRepository.get_discussion_queryset(
            user=user, wide_filter=True)
        return result.order_by('-created_at')


class ListUserParticipationDiscussionsView(BaseListAPIView):
    """View for list of all discussions where user is partipiciant but not initiator"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user: CustomUser = self.request.user.user
        result = DiscussionAppRepository.get_discussion_queryset(
            user=user, wide_filter=False)
        return result.order_by('-created_at')


class ListSomeUserParticipationDiscussionsView(BaseListAPIView):
    """View for list of all discussions where some another user is partipiciant"""

    serializer_class = ActiveDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        pk: int = self.kwargs['pk']
        try:
            profile: Profile = Profile.objects.select_related(
                'user').get(id=pk)
        except Profile.DoesNotExist:
            raise ObjectNotFoundError
        result = DiscussionAppRepository.get_discussion_queryset(
            user=profile.user, wide_filter=True)
        return result.order_by('-created_at')


class AutoPaymentView(BaseListAPIView):
    """View for create payment automatically. Use ONLY for TESTS"""

    serializer_class = AutoPaymentSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        user = request.user.user
        serializer: AutoPaymentSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: AutoPaymentDTO = serializer.dto
        discussion_dto: DiscussionAuthorizePaymentDTO = DiscussionAuthorizePaymentDTO(
            discussion=dto.discussion)

        with transaction.atomic():
            number_of_available_seats, is_initiator = DiscussionAppRepository.discussion_check(
                user=user, discussion_dto=discussion_dto)

            # create participant with 0 paid seats
            if not is_initiator:
                ParticipantsOfDiscussion.objects.create(
                    user=user, discussion=discussion_dto.discussion, number_of_paid_seats=0)

        # Imitate payment
        order_representation_id: int = DiscussionAppRepository.imitate_payment_with_selenium(
            dto=dto, user=user, discussion=discussion_dto.discussion, is_initiator=is_initiator)

        order = Order.objects.get(
            order_representation_id=order_representation_id)

        if order.is_confirmed:
            # get participant wich was creted after StripeWebhooks came
            participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.get(
                user=user, discussion=discussion_dto.discussion)

            serializer = ParticipantOfDiscussionSerializer(participant)
            return SuccessResponse(status=HTTP_201_CREATED, detail=f'Payment was successfull.New participant wass added to discussion "{discussion_dto.discussion.topic}"', obj=serializer.data)

        if not order.is_confirmed:
            # if order wasn't confirmed check if there is payment if no
            check_unconfirmed_orders()
            check_charge_data()
            if order.payment_status == 'unpaid':
                return ErrorResponse(status=200, detail='Payment failed')
            return SuccessResponse(status=HTTP_201_CREATED, detail=f'Payment was successfull.New participant wass added to discussion "{discussion_dto.discussion.topic}"', obj=serializer.data)


class CreateUserThemesAPIView(BaseGenericAPIView):
    """Endpoint for create user's themes"""

    serializer_class = CreateUserThemesSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        user: CustomUser = request.user.user
        serializer: CreateUserThemesSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: CreateUserThemesDTO = serializer.dto

        # Before create themes delete old one
        UserThemes.objects.filter(user=user).delete()

        themes_list: list[UserThemes] = DiscussionAppRepository.create_user_themes(
            user=user, dto=dto)
        user_themes_dto: UserThemesDTO = UserThemesDTO(
            user=user, themes=themes_list)
        user_themes_serializer: UserThemesSerializer = UserThemesSerializer(
            user_themes_dto)
        return SuccessResponse(status=HTTP_201_CREATED, detail='Themes were added successfully.', obj=user_themes_serializer.data)


class ListOfUserThemesView(BaseListAPIView):
    """View for user themes list"""

    serializer_class = ListUserThemesSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user: CustomUser = self.request.user.user
        return UserThemes.objects.filter(user=user)


class DeleteUserThemesApiView(views.APIView):
    """View for delete user themes"""

    serializer_class = DeleteUserThemesSerializer
    permission_classes = [IsAuthenticated, ]

    def delete(self, request, **kwargs):
        user: CustomUser = self.request.user.user
        serializer: DeleteUserThemesSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        theme_id = serializer.data.get('id')
        try:
            user_themes: UserThemes = UserThemes.objects.get(
                user=user, themes_id=theme_id)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError

        DiscussionAppRepository.delete_user_themes(user_themes=user_themes)

        return SuccessResponse(detail=f"Theme '{user_themes.themes}' was successfully deleted", status=HTTP_200_OK)


class TestUpdateDiscussionHoldingAgreedApiView(BaseGenericAPIView):
    """ TEST ONLY! 
        View for update discussion holding_agreed field
    """

    serializer_class = UpdateDiscussionSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, pk=None, **kwargs):
        discussion_pk: int = self.kwargs['pk']
        try:
            discussion: Discussion = Discussion.objects.get(pk=discussion_pk)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        
        if request.data.get('holding_agreed') == True:
            discussion.update_fields(holding_agreed=True, on_review=False)
        if request.data.get('holding_agreed') == False:
            discussion.update_fields(holding_agreed=False, on_review=False)
        _serializer = CreateDiscussionSerializer(discussion)
        return SuccessResponse(status=HTTP_200_OK, detail=f'Fields holding_agreed and on_review of discussion {discussion_pk} was updated', obj=_serializer.data)


class TestCreateParticipant(BaseGenericAPIView):
    """View for add test participants to discussion without payment"""

    serializer_class = DiscussionPaymentSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        user: CustomUser = self.request.user.user
        serializer: DiscussionPaymentSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: DiscussionAuthorizePaymentDTO = serializer.dto
        number_of_available_seats, is_initiator = DiscussionAppRepository.discussion_check(
            user=user, discussion_dto=dto)

        if is_initiator:
            dto.discussion.update_fields(
                is_active=True, paid=True, holding_agreed=True)
            participant = ParticipantsOfDiscussion.objects.get(
                user=user, discussion=dto.discussion)
            participant.update_fields(number_of_paid_seats=1)
        if not is_initiator:
            ParticipantsOfDiscussion.objects.create(
                user=user, discussion=dto.discussion, number_of_paid_seats=1)
        return SuccessResponse(status=HTTP_201_CREATED, detail=f"You were added successfully as participant of discussion {dto.discussion.pk} - {dto.discussion.topic}")
