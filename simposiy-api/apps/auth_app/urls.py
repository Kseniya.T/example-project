from django.urls import path
from .views import (ApiVersionView, LogoutApiView, PushTokenCreateAPIView,
                    SendSmsView,
                    SignInConfirmView,
                    SignInAuthAPIView,)

app_name = 'auth_app'


urlpatterns = [
    path('auth/send-sms/', SendSmsView.as_view(), name='send-sms'),
    path('auth/sign-in/',
         SignInConfirmView.as_view(), name='sign-in'),
    path('auth/sign-in/update/', SignInAuthAPIView.as_view(), name='sign-in-update'),
    path('auth/logout', LogoutApiView.as_view(), name='logout'),

    # PushToken
    path('user/push-token/create/',
         PushTokenCreateAPIView.as_view(), name='push-token-create'),
    
    # Api version
    path('version/', ApiVersionView.as_view(), name='api-version'),
]
