from abc import ABC
from typing import Optional
from sentry_sdk import capture_message, set_context
from six import text_type

from drf_spectacular.extensions import OpenApiAuthenticationExtension
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured, ObjectDoesNotExist
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication

from apps.auth_app.dto import JWTSchema
from services.authentication_service import AuthenticationService
from utils.dto import UserDTO
from repositories.auth_app_repository import AuthAppRepository
from apps.user_app.models import CustomUser
from .models import Session
from utils.exceptions import JWTNotValidError, SessionNotFoundError, UserNotFoundError


HTTP_HEADER_ENCODING = 'iso-8859-1'


class JWTBaseAuthentication(BaseAuthentication, ABC):

    def get_token(self, request):
        raise NotImplementedError("Implement this method in your subclass")

    def authenticate(self, request):
        refresh_token = self.get_refresh_token(request=request)
        token = self.get_token(request=request)
        return self.authenticate_credentials(token=token, refresh_token=refresh_token)

    def authenticate_credentials(self, token: bytes, refresh_token: Optional[bytes] = None):
        if refresh_token:
            raise JWTNotValidError

        if not token:
            return None

        if isinstance(token, bytes):
            token = token.decode('utf-8')

        secret = getattr(settings, 'JWT_SECRET', None)
        if secret is None:
            capture_message(
                'settings.JWT_SECRET not configured', level='error')
            raise ImproperlyConfigured("settings.JWT_SECRET not configured.")

        jwt_schema: JWTSchema = AuthenticationService.decode_token(
            cleaned_token=token)

        # Если идет попытка авторизоваться через RefreshToken, райзим ошибку
        if jwt_schema.is_refresh_token:
            capture_message(
                'Attempt to log in with refreshToken error', level='error')
            raise JWTNotValidError

        try:
            user: CustomUser = CustomUser.objects.get(id=jwt_schema.user_id)
            AuthAppRepository.validate_user(user=user)
        except ObjectDoesNotExist as e:
            set_context('user_is_not_found_case', value=e.__dict__)
            capture_message('User is not found error', level='error')
            raise UserNotFoundError

        try:
            session: Session = user.session.get(pk=jwt_schema.session_id)
            AuthAppRepository.validate_session(session=session)
        except ObjectDoesNotExist as e:
            set_context('session_is_not_found_case', value=e.__dict__)
            capture_message('Session is not found error', level='error')
            raise SessionNotFoundError

        return UserDTO(user=user, session=session), None


def get_authorization_header(request):
    """
    Return request's 'Authorization:' header, as a bytestring.
    Hide some test client ickyness where the header can be unicode.
    """

    auth_header = request.META.get('HTTP_AUTHORIZATION', b'')
    if isinstance(auth_header, str):
        # Work around django test client oddness
        auth_header = auth_header.encode(HTTP_HEADER_ENCODING)
        return auth_header

    auth_query = request.query_params.get('token', b'')
    if isinstance(auth_query, str):
        # Work around django test client oddness
        auth_query = auth_query.encode(HTTP_HEADER_ENCODING)
        return auth_query

    return b''


class JWTHeaderAuthentication(JWTBaseAuthentication):
    www_authenticate_realm = 'Bearer'

    @staticmethod
    def get_refresh_token(request) -> bytes:
        refresh_token = request.META.get('HTTP_REFRESHTOKEN', b'')
        if isinstance(refresh_token, text_type):
            # Work around django test client oddness
            refresh_token = refresh_token.encode(HTTP_HEADER_ENCODING)
            return refresh_token
        return b''

    def get_token(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b'bearer':
            return None

        if len(auth) == 1:
            # capture_message(
            #     'Authentication Failed: Invalid header. Credentials were not provided', level='exception')
            raise exceptions.AuthenticationFailed(
                "Invalid header. Credentials were not provided")
        elif len(auth) > 2:
            # capture_message(
            #     'Authentication Failed: Invalid header. Credentials must not contain spaces', level='exception')
            raise exceptions.AuthenticationFailed(
                "Invalid header. Credentials must not contain spaces")

        return auth[1]


class MyAuthenticationScheme(OpenApiAuthenticationExtension):
    # full import path OR class ref
    target_class = 'apps.auth_app.backends.JWTHeaderAuthentication'
    name = 'JWTHeaderAuthentication'  # name used in the schema

    def get_security_definition(self, auto_schema):
        return {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization',
            'description': 'Token-based authentication with required prefix "Bearer"'
        }
