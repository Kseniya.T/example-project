from requests import Session
from sentry_sdk import capture_message, set_context
from apps.auth_app.models import CustomUser
from utils.exceptions import UserNotFoundError


def get_user_and_session(email=None, phone_number=None, **kwargs):
    """Function for retrieve user and session"""

    device_agent_id = kwargs.get('device_agent_id')
    device_agent_represent = kwargs.get('device_agent_represent')
    try:
        if phone_number:
            user = CustomUser.objects.get(phone_number=phone_number)
    except CustomUser.DoesNotExist as e:
        set_context('Account not found', value=e.__dict__)
        capture_message(
            'User with such phone number doesn\'t exists', level='error')
        raise UserNotFoundError('Account not found.')

    session, created = Session.objects.get_or_create(
        user=user,
        device_agent_id=device_agent_id,
        device_agent_represent=device_agent_represent,
        is_deactivated=False
    )

    return user, session
