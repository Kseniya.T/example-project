from typing import Tuple
from django.contrib import admin
from apps.auth_app.models import PushToken, Session


@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    model = Session
    list_display: Tuple[str] = (
        'id',
        'user',
        'device_agent_id',
        'device_agent_represent'
    )


@admin.register(PushToken)
class PushTokenAdmin(admin.ModelAdmin):
    model: PushToken
