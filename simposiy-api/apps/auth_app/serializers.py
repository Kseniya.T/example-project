from rest_framework import serializers
from phonenumber_field.serializerfields import PhoneNumberField

from apps.auth_app.dto import PushTokenDTO, RawUserRegistrationDTO, UserAuthenticationDTO
from utils.custom_serializer import MainSerializer


class SignUpSerializer(MainSerializer):
    """SignUpSerializer serializer"""

    dto_class = RawUserRegistrationDTO

    phone_number = PhoneNumberField()
    device_agent_id = serializers.CharField(required=True, max_length=120, )
    device_agent_represent = serializers.CharField(
        required=True, max_length=120)
    new_user = serializers.BooleanField(read_only=True)


class SignInSerializer(MainSerializer):
    """SignInSerializer serializer"""

    dto_class = UserAuthenticationDTO

    phone_number = PhoneNumberField()
    device_agent_id = serializers.CharField(required=True, max_length=128, )
    device_agent_represent = serializers.CharField(
        required=True, max_length=128)


class SignInConfirmSerializer(MainSerializer):
    """SignInConfirmSerializer serializer"""

    dto_class = UserAuthenticationDTO

    phone_number = PhoneNumberField()
    code = serializers.CharField(max_length=6)
    device_agent_id = serializers.CharField(required=True, max_length=128)
    device_agent_represent = serializers.CharField(
        required=True, max_length=128)
    push_token = serializers.CharField(
        max_length=500, required=False, allow_null=True, allow_blank=True)


class SignInAuthSerializer(serializers.Serializer):
    """SignInAuthSerializer serializer"""

    refresh_token = serializers.CharField(max_length=128)


class LogoutSerializer(serializers.Serializer):
    """LogoutSerializer serializer"""

    result = serializers.BooleanField(read_only=True, default=True)
    message = serializers.CharField(
        read_only=True,
        default='The user was logged out successfully.')


class PushTokenCreateSerializer(MainSerializer):
    """PushTokenCreateSerializer serializer"""

    dto_class = PushTokenDTO

    push_token = serializers.CharField(
        max_length=500, required=True, allow_null=False, allow_blank=False)
