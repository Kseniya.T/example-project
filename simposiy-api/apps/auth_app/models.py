from datetime import timedelta
from sentry_sdk import capture_exception

from django.db import models
from django.utils import timezone
from django.db import models

from utils.base_model import AppBaseModel
from core.settings import JWT_TTL
from utils.base_model import AppBaseModel
from apps.user_app.models import CustomUser


class Session(AppBaseModel):
    """
        Session model
    """

    user = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, verbose_name='User', related_name='session')
    device_agent_id = models.CharField(
        verbose_name='ID device', max_length=128, )
    device_agent_represent = models.CharField(
        verbose_name='Name device', max_length=128, )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Datetime create', )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Datetime update', )
    is_confirmed = models.BooleanField(
        default=False, verbose_name='Is confirmed', )
    is_blocked = models.BooleanField(
        default=False, verbose_name='Is blocked', )
    is_deactivated = models.BooleanField(
        default=False, verbose_name='Is deactivated', )
    unlock_date = models.DateTimeField(
        null=True, blank=True, verbose_name='Date to unlock', )
    lock_counter = models.PositiveSmallIntegerField(
        default=0, verbose_name='Lock counter', )
    datetime_expiration = models.DateTimeField(
        verbose_name='Datetime expiration', )

    def __str__(self):
        return f' {self.device_agent_represent}|{self.device_agent_id}'

    def save(self, *args, **kwargs):
        if not self.datetime_expiration:
            self.datetime_expiration = timezone.now() + timedelta(minutes=JWT_TTL)
        super().save(*args, **kwargs)

    def confirm(self):
        if not self.is_confirmed:
            self.update_fields(is_confirmed=True)

    def unconfirm(self):
        if self.is_confirmed:
            self.update_fields(is_confirmed=False)

    def deactivate(self):
        if not self.is_deactivated:
            self.update_fields(is_deactivated=True)
            try:
                self.push_token.last().delete()
            except AttributeError as e:
                pass
                #capture_exception(e)

    def activate(self):
        if self.is_blocked:
            self.update_fields(is_blocked=False)

    def update_datetime_expiration(self):
        """Update session lifetime"""
        now = timezone.now()
        self.update_fields(
            save=False, datetime_expiration=now + timedelta(minutes=JWT_TTL))

    class Meta:
        verbose_name = 'Session'
        verbose_name_plural = 'Sessions'


class PushToken(AppBaseModel):
    """PushToken model"""

    user = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, verbose_name='User',
        null=True, blank=True, related_name='push_token'
    )
    session = models.ForeignKey(
        Session, on_delete=models.CASCADE, verbose_name='Session',
        blank=True, related_name='push_token', null=True
    )
    token = models.CharField(max_length=500, verbose_name='Token')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Created at')

    class Meta:
        verbose_name = 'Push Token'
        verbose_name_plural = 'Push Tokens'

    def __str__(self):
        return f'{self.user_id} - {self.token}'
