from typing import Optional
from utils.api_version import get_api_version
from utils.generics.base_generics import BaseGenericAPIView
from rest_framework.status import HTTP_200_OK
from drf_spectacular.utils import extend_schema
from sentry_sdk import capture_message
from django.http import JsonResponse
from django.views import View
from rest_framework.response import Response
from django.contrib.auth import logout
from rest_framework.permissions import AllowAny

from apps.auth_app.models import Session
from apps.user_app.models import CustomUser
from core.settings import CAN_SEND_SMS
from services.authentication_service import AuthenticationService
from utils.dto import UserDTO
from utils.exceptions import (DeactivatedSessionError,
                              RefreshTokenNotValidError,
                              )
from utils.permissions import IsAuthenticated
from utils.response import ErrorResponse, SuccessResponse
from .dto import (JWTSchema, PushTokenDTO, RawUserRegistrationDTO, UserAuthenticationDTO,
                  UserResponseAuthDTO,
                  TokensDTO, )
from repositories.auth_app_repository import AuthAppRepository
from .serializers import (LogoutSerializer, PushTokenCreateSerializer,
                          SignInConfirmSerializer,
                          SignUpSerializer,)


class SendSmsView(BaseGenericAPIView):
    """View for send sms with code"""

    serializer_class = SignUpSerializer
    permission_classes = [AllowAny, ]

    @extend_schema(request=SignUpSerializer)
    def post(self, request, *args, **kwargs):
        serializer: SignUpSerializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: RawUserRegistrationDTO = serializer.dto
        response_auth_dto: UserResponseAuthDTO = AuthAppRepository.sign_in_user(
            dto=dto)
        return SuccessResponse(
            status=HTTP_200_OK,
            detail=f'A one-time login code has been sent to the specified phone number{f" {response_auth_dto.otp_code}" if not CAN_SEND_SMS else ""}',
            obj=response_auth_dto.get_dict()
        )


class SignInConfirmView(BaseGenericAPIView):
    """View Login confirmations (via OTP) """

    serializer_class = SignInConfirmSerializer
    permission_classes = [AllowAny, ]

    @extend_schema(
        request=SignInConfirmSerializer)
    def post(self, request, *args, **kwargs):
        serializer: SignInConfirmSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)

        push_token: Optional[str] = serializer.validated_data.get('push_token')
        user_auth_dto: UserAuthenticationDTO = serializer.dto
        user_dto = AuthAppRepository.authenticate_user(
            user_auth_dto=user_auth_dto)

        access_token = AuthenticationService.generate_jwt(
            user_id=user_dto.user.pk,
            session_id=user_dto.session.pk, )
        refresh_token = AuthenticationService.generate_jwt(
            user_id=user_dto.user.pk,
            session_id=user_dto.session.pk,
            is_refresh_token=True, )
        tokens_dto = TokensDTO(token=access_token.decode(),
                               refresh_token=refresh_token.decode(),
                               phone_number=user_auth_dto.phone_number.raw_input)
        if push_token:
            # Removing old push token
            AuthAppRepository.remove_push_tokens(push_token=push_token)
            # Creating new push token
            AuthAppRepository.create_push_token(
                user=user_dto.user,
                session=user_dto.session,
                push_token=push_token,
            )
        return SuccessResponse(detail='Login successful', obj=tokens_dto.__dict__)


class SignInAuthAPIView(BaseGenericAPIView):
    """View refresh tokens"""

    permission_classes = [AllowAny, ]

    @extend_schema(responses=None, request=None)
    def post(self, request):
        cleaned_token: str = AuthenticationService.get_cleaned_jwt(
            raw_jwt=request.META.get('HTTP_REFRESHTOKEN'))
        jwt_schema: JWTSchema = AuthenticationService.decode_token(
            cleaned_token=cleaned_token, is_refresh_token=True)

        # проверяем, является ли переданный JWT refresh токеном
        if not jwt_schema.is_refresh_token:
            capture_message(
                'Failed to correctly decode user token', level='error')
            raise RefreshTokenNotValidError

        user_dto: UserDTO = AuthAppRepository.get_user_and_session_by_id(
            user_id=jwt_schema.user_id,
            session_id=jwt_schema.session_id,
        )

        AuthAppRepository.validate_user(user=user_dto.user)
        AuthAppRepository.validate_session(session=user_dto.session)

        tokens_dto = TokensDTO(
            token=AuthenticationService.generate_jwt(
                user_id=user_dto.user.pk,
                session_id=user_dto.session.pk).decode(),
            refresh_token=AuthenticationService.generate_jwt(
                user_id=user_dto.user.pk,
                session_id=user_dto.session.pk,
                is_refresh_token=True).decode()
        )

        # Обновляем время жизни сессии, повторно её активируем
        # объект Session не сохранятся на данном этапе (для оптимизации запросов в БД)
        user_dto.session.update_datetime_expiration()

        # объект Session сохраняется на данном этапе
        user_dto.session.confirm()
        return SuccessResponse(obj=tokens_dto.__dict__, status=HTTP_200_OK)


class LogoutApiView(BaseGenericAPIView):
    """Veiw to logout"""

    permission_classes = [IsAuthenticated, ]
    serializer_class = LogoutSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_dto: UserDTO = request.user
        if not user_dto.session.is_deactivated:
            user_dto.session.deactivate()
            logout(request)
            return Response(serializer.data,
                            status=HTTP_200_OK)
        raise DeactivatedSessionError('Session already deactivated')


class PushTokenCreateAPIView(BaseGenericAPIView):
    """View for create Push token"""

    serializer_class = PushTokenCreateSerializer
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        user: CustomUser = self.request.user.user
        session: Session = self.request.user.session

        serializer: PushTokenCreateSerializer = self.serializer_class(
            data=request.data,
            context={'request': request})
        serializer.is_valid(raise_exception=True)
        dto: PushTokenDTO = serializer.dto
        push_token = AuthAppRepository.create_push_token(
            user=user, session=session, dto=dto)
        if push_token:
            return SuccessResponse(status=200, detail='The push token has been successfully created', obj=serializer.data)
        return ErrorResponse(status=200, detail='The push token was not created')


class ApiVersionView(View):
    def get(self, request):
        api_version = get_api_version()
        data = {'api_version': api_version}
        return JsonResponse(data)
