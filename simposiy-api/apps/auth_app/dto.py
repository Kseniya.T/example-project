from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict


@dataclass(frozen=True)
class RawUserRegistrationDTO:
    """DTO for user registration case"""

    phone_number: str
    device_agent_id: str
    device_agent_represent: str


@dataclass(frozen=True)
class UserAuthenticationDTO:
    """DTO for user authentication case"""

    phone_number:  str
    device_agent_id: str
    device_agent_represent: str
    push_token: str = None
    code: str = None


@dataclass(frozen=True)
class UserResponseAuthDTO:
    """UserResponseAuthDTO"""

    resend_by: datetime
    resend_by_seconds: int
    timeout_resend: int
    otp_code: str | None = None
    new_user: bool = None

    def get_dict(self) -> dict:
        return dict(
            new_user=self.new_user,
            resend_by=self.resend_by,
            resend_by_seconds=self.resend_by_seconds,
            timeout_resend=self.timeout_resend,
        )


@dataclass
class TokensDTO:
    """DTO for tokens"""

    token: str
    refresh_token: str
    phone_number: str = None


@dataclass
class JWTSchema:
    """DTO for JWTSchema"""

    is_refresh_token: bool
    session_id: int
    user_id: int
    exp: int


@dataclass
class FirebaseMessageDTO:
    """DTO for Firebase messages"""

    pk: Any
    title: str
    body: str
    payload: Dict


@dataclass
class PushTokenDTO:
    """DTO for Push Token"""

    push_token: str
