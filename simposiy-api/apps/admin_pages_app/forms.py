from django import forms

class ConfirmPaymentsForm(forms.Form):
    stripe_account_id = forms.CharField(widget=forms.HiddenInput)
    amount_payable = forms.DecimalField(widget=forms.HiddenInput)
    discussion_id = forms.IntegerField(widget=forms.HiddenInput)
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['amount_payable'].required = True
        self.fields['stripe_account_id'].required = True
        self.fields['discussion_id'].required = True
