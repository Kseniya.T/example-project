from django.apps import AppConfig


class AdminPagesAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.admin_pages_app'
    verbose_name = 'Admin Pages App'
