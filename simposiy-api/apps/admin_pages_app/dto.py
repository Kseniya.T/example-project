from dataclasses import dataclass
from typing import Any, Optional, Sequence
from apps.payment_app.models import PayoutData

from apps.user_app.models import Profile


@dataclass(frozen=True)
class TemplateContextDTO:
    """DTO for TemplateView context_data"""

    owner_profile: Profile = None
    total_amount_payable: int = None
    payout: PayoutData = None
    title: Optional[str] = None
    object: Any = None
    objects: Sequence[Any] = None
    error: Any = None
    errors: Sequence[Any] = None
    table_header_row: Any = None


@dataclass(frozen=True)
class AdminPageDTO:
    """DTO for CustomPagesView"""

    title: str
    url: str
