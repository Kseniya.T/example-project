from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from typing import Any, Dict
from django.urls import reverse
from django.shortcuts import render
from apps.payment_app.models import PayoutData

from repositories.custom_pages_repository import get_context_dto, get_payout_context_dto
from apps.admin_pages_app.dto import AdminPageDTO, TemplateContextDTO
from apps.discussion_app.models import Discussion
from apps.user_app.models import Profile
from repositories.custom_pages_repository import get_discussions_for_payment, get_payouts


class CustomPagesView(LoginRequiredMixin, TemplateView):
    """View for list admin custom pages"""

    template_name: str = 'admin_pages_app/custom_pages_list.html'
    login_url: str = '/'

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: dict = super().get_context_data(**kwargs)
        dto = TemplateContextDTO(
            title='Custom admin pages',
            objects=__class__.get_admin_pages_list(),
            table_header_row=('title', ),
        )
        context.update(dto.__dict__)
        return context

    @staticmethod
    def get_admin_pages_list() -> tuple[AdminPageDTO]:
        """Method for returning custom admin pages urls"""

        return (
            AdminPageDTO(
                title='Transfer payments',
                url=reverse('admin_pages_app:payments-details'),
            ),
            AdminPageDTO(
                title='Payouts',
                url=reverse('admin_pages_app:list-payouts'),
            ),
        )


class PaymentTransferView(LoginRequiredMixin, TemplateView):
    """View for generating payments report and transfer payments"""

    template_name: str = 'admin_pages_app/payments_details.html'
    login_url: str = '/'

    def get_context_data(self, **kwargs) -> Dict[str, Any]:

        context: dict = super().get_context_data(**kwargs)

        list_discussions: list[Discussion] = get_discussions_for_payment()
        dto = TemplateContextDTO(
            title='Discussions for transfer payments',
            objects=list_discussions,
            table_header_row=(
                'ID',
                'Discussion',
                'Number of participants',
                'For one seat',
                'Total amount',
                'Service fee',
                'Amount payable',
                'Place',
                'Transfer to',
                ''
            ),
        )

        place_owner_id = self.request.GET.get('search_query')
        if place_owner_id:
            list_discussions: list[Discussion] = [
                discussion for discussion in list_discussions if discussion.place.owner.profile.id == int(place_owner_id)]
            dto = TemplateContextDTO(
                title='Discussions for transfer payments',
                objects=list_discussions,
                table_header_row=(
                    'Check',
                    'ID',
                    'Discussion',
                    'Number of participants',
                    'For one seat',
                    'Total amount',
                    'Service fee',
                    'Amount payable',
                    'Place',
                    'Transfer to',
                ),
            )

        context.update(dto.__dict__)
        return context


class ConfirmPayments(LoginRequiredMixin, TemplateView):
    """View for confirm payments"""

    template_name: str = 'admin_pages_app/confirm_payments.html'
    login_url: str = '/'

    def get(self, request, *args, **kwargs):
        discussion_id = request.GET.get('discussion_id')
        discussions_id_list = [int(discussion_id)]
        owner_profile_id = int(request.GET.get('profile_id'))
        profile = Profile.objects.select_related(
            'user').get(id=owner_profile_id)
        context: dict = super().get_context_data(**kwargs)
        dto: TemplateContextDTO = get_context_dto(
            discussions_id_list=discussions_id_list, owner_profile=profile)
        context.update(dto.__dict__)

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        discussions_id_list: list[int] = [
            int(id) for id in request.POST.getlist('checkbox_discussion')]
        owner_profile_id = int(request.GET.get('profile_id'))
        profile = Profile.objects.select_related(
            'user').get(id=owner_profile_id)
        context: dict = super().get_context_data(**kwargs)
        dto: TemplateContextDTO = get_context_dto(
            discussions_id_list=discussions_id_list, owner_profile=profile)
        context.update(dto.__dict__)

        return render(request, self.template_name, context)


class PayoutDataView(LoginRequiredMixin, TemplateView):
    """View for list PayoutData objects"""

    template_name: str = 'admin_pages_app/payouts.html'
    login_url: str = '/'

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        context: dict = super().get_context_data(**kwargs)
        payouts_list: list[PayoutData] = get_payouts()
        dto = TemplateContextDTO(
            title='List Of Payouts',
            objects=payouts_list,
            table_header_row=(
                'Payout ID',
                'Place owner',
                'Payout ID',
                'Transfered amount',
                'Status',
                'Created at',
                ''
            ),
        )
        context.update(dto.__dict__)
        return context


class PayoutDetailsView(LoginRequiredMixin, TemplateView):
    """View for payout details"""

    template_name: str = 'admin_pages_app/payout_details.html'
    login_url: str = '/'

    def get(self, request, *args, **kwargs):
        payoutdata_id = request.GET.get('payoutdata_id')
        payout = PayoutData.objects.select_related(
            'place_owner').get(id=int(payoutdata_id))
        discussions_id_list = [int(discussion_id)
                               for discussion_id in payout.discussions.split(',')]
        profile = payout.place_owner.profile
        context: dict = super().get_context_data(**kwargs)
        dto: TemplateContextDTO = get_payout_context_dto(
            discussions_id_list=discussions_id_list, owner_profile=profile, payout=payout)
        context.update(dto.__dict__)

        return render(request, self.template_name, context)


# class SuccessView(TemplateView):
#     """View for success page"""

#     template_name = "admin_pages_app/success.html"

#     def get(self, request, *args, **kwargs):
#         return render(request, self.template_name)


# class TransferPayment(LoginRequiredMixin, FormMixin, TemplateView):
#     """View for transfer money to locations owners"""

#     form_class = ConfirmPaymentsForm
#     template_name = 'admin_pages_app/transfer_payments.html'
#     success_url = reverse_lazy('admin_pages_app:tarnsfer-success')

#     def post(self, request, *args, **kwargs):

#         form = self.form_class(request.POST)
#         if form.is_valid():
#             # amount_payable = form.cleaned_data.get('amount_payable')
#             # amount = int(amount_payable*100)
#             stripe_account_id = form.cleaned_data.get('stripe_account_id')
#             # discussion_ids: list = request.POST.getlist('discussion_id')
#             # discussion_ids_str: str = ', '.join(discussion_ids)
#             link = f'https://dashboard.stripe.com/test/connect/accounts/{stripe_account_id}/activity'
#             return redirect(link)
#         else:
#             return self.form_invalid(form)


# class MakeRefundsView(LoginRequiredMixin, TemplateView):
#     """View for making refunds"""

#     template_name: str = 'admin_pages_app/refunds.html'
#     login_url: str = '/'

#     def post(self, request, *args, **kwargs):
#         charge_list: list[str] = request.POST.getlist('checkbox_charge')
#         for charge in charge_list:
#             charge_obj: ChargeData = ChargeData.objects.get(charge_id=charge)
#             order: Order = Order.objects.select_related('user', 'discussion').get(charge=charge_obj)
#             discussion: Discussion = Discussion.objects.get(id=order.discussion.pk)

#             # check how much time left till start of discussion
#             now = timezone.make_naive(timezone.now())
#             discussion_start_date: datetime.datetime = discussion.dt_start.replace(
#                 tzinfo=None)
#             difference: datetime.timedelta = discussion_start_date - now
#             days, hours = difference.days, difference.seconds // 3600

#             if days < 1:
#                 messages.error(request, message=f'Before the start of the discussion left {hours} hr. The deadline for requesting a refund is 24 hours before the start of the discussion.')
#                 return HttpResponseRedirect(reverse('admin_pages_app:order-details'))

#             create_refund_dto: CreateRefundDTO = StripeService.create_refund_dto(
#                 charge_id=charge)
#             StripeService.create_refund(
#                 dto=create_refund_dto)

#             # remove user from participants
#             DiscussionAppRepository.remove_user_from_participants(user=order.user, discussion=discussion)

#             # check if discussion was full make flag is_full=False
#             if discussion.is_full == True:
#                 discussion.update_fields(is_full=False)
#         return HttpResponseRedirect(reverse('admin_pages_app:order-details'))


# class OrdersView(LoginRequiredMixin, TemplateView):
#     """View for generating order report"""

#     template_name: str = 'admin_pages_app/order_details.html'
#     login_url: str = '/'

#     def get_context_data(self, **kwargs) -> Dict[str, Any]:

#         context: dict = super().get_context_data(**kwargs)

#         discussion_id = self.request.GET.get('search_query')

#         list_order_dto: list[Order] = Order.objects.select_related('user', 'discussion').filter(discussion__is_active=True,
#             is_confirmed=True).order_by('-id')

#         if discussion_id:
#             list_order_dto: list[Order] = Order.objects.select_related('user', 'discussion').filter(discussion_id=int(
#                 discussion_id), discussion__is_active=True, is_confirmed=True, refunded=False).order_by('-id')

#         dto = TemplateContextDTO(
#             title='Refund Orders',
#             objects=list_order_dto,
#             table_header_row=(
#                 'To refund',
#                 'Order ID',
#                 'User',
#                 'User email',
#                 'Discussion ID',
#                 'Charge',
#                 'Payment status',
#                 'Created at',
#                 'Updated at',
#                 'Is confirmed',
#                 'Refunded'
#             ),
#         )
#         context.update(dto.__dict__)

#         return context
