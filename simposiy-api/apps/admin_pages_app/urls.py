from django.urls import path

from .views import (
    ConfirmPayments,
    CustomPagesView,
    PaymentTransferView,
    PayoutDataView,
    PayoutDetailsView,
    # SuccessView,
    # TransferPayment,
    # MakeRefundsView,
    # OrdersView,
)

app_name = 'admin_pages_app'


urlpatterns = [
    # main page
    path('custom-pages/', CustomPagesView.as_view(), name='custom-pages-list'),

    # payments
    path('payments/', PaymentTransferView.as_view(), name='payments-details'),
    path('payments/confirm/', ConfirmPayments.as_view(), name='confirm-payments'),

    # payouts data
    path('payouts/list/', PayoutDataView.as_view(), name='list-payouts'),
    path('payout/details/', PayoutDetailsView.as_view(), name='payout-details'),

    # refunds
    # path('make-refunds/', MakeRefundsView.as_view(), name='make-refunds'),
    # path('orders/', OrdersView.as_view(), name='order-details'),

    # transfers
    # path('payments/transfer/', TransferPayment.as_view(), name='transfer-payments'),
    # path('tarnsfer/success/', SuccessView.as_view(), name='tarnsfer-success'),
]
