import pickle
import pika
from pika.adapters import BlockingConnection
from pika.adapters.blocking_connection import BlockingChannel

from typing import Any
from apps.rabbitmq_app.enums import EventEnum, QueuesEnum
from core.settings import (
    VIRTUAL_HOST, RABBITMQ_PRODUCER_USER,
    RABBITMQ_PORT, RABBITMQ_PRODUCER_PASSWORD,
    EXCHANGE_INBOUND, RABBITMQ_HOST, EXCHANGE_OUTBOUND
)


class ProducerRabbitMQ:

    @staticmethod
    def connect(
        host: str, login: str, password: str
    ) -> tuple[BlockingChannel, BlockingConnection]:
        """
        Static Method create connection for producer
        """
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=host,
                virtual_host=VIRTUAL_HOST,
                port=RABBITMQ_PORT,
                credentials=pika.PlainCredentials(
                    username=login,
                    password=password
                )
            )
        )

        channel = connection.channel()
        return channel, connection

    @staticmethod
    def publish(
        data: Any, exchange: str,
        queue: str, host='localhost',
        login=RABBITMQ_PRODUCER_USER,
        password=RABBITMQ_PRODUCER_PASSWORD
    ) -> None:
        """
        Static Method for publish message to queue
        """
        channel, connection = __class__.connect(
            host=host, login=login, password=password
        )
        channel.basic_publish(
            exchange=exchange,
            routing_key=queue,
            body=pickle.dumps(data)
        )
        print("[x] Sent event to cold storage")

        connection.close()


if __name__ == '__main__':
    ProducerRabbitMQ.publish(
        data={
            'user_id': 4,
            'key': 'sdfjlsdjflksjdlkfjslkdjflkslsdjfklsj',
            'event': EventEnum.NEW_KEY.value
        },
        queue=str(QueuesEnum.OUTBOUND),
        exchange=EXCHANGE_OUTBOUND,
        host=RABBITMQ_HOST,
        login='producer_outbound',
        password='9AsodfiosoQjoGMCYVH6BJq'
    )
