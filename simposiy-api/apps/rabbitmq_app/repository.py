from io import BytesIO
from django.core.exceptions import ObjectDoesNotExist
from sentry_sdk import capture_message, set_context
from django.template.loader import render_to_string
from django.core.files.base import ContentFile

from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion
from apps.notification_app.models import ApplicationConfig, Notification
from apps.payment_app.models import ChargeData, Order, PayoutData, Refund
from apps.payment_app.utils import generate_pdf
from apps.rabbitmq_app.enums import DiscussionNotifydDTO, MinParticipantsRichedDTO, QueuesEnum, StripeChargeEventDTO, StripeCheckoutSessionEventDTO, StripePayoutEventDTO, StripeRefundEventDTO, StripeUpdateAccountEventDTO
from apps.rabbitmq_app.producer import ProducerRabbitMQ
from apps.user_app.models import CustomUser, Profile
from core.settings import EXCHANGE_PUSH_NOTIFICATION_INBOUND, RABBITMQ_HOST, RABBITMQ_PRODUCER_PASSWORD, RABBITMQ_PRODUCER_USER
from core.storage_backends import PDFStorage
from repositories.notification_repository import NotificationRepository
from utils.exceptions import ObjectNotFoundError
from repositories.init import push_notification_repository


def create_charge_data_obj(dto: StripeChargeEventDTO):
    """Method for create object of ChargeData"""

    ChargeData.objects.create(
        charge_id=dto.charge_id,
        charge_status=dto.charge_status,
        payment_intent_id=dto.payment_intent_id,
        amount_captured=dto.amount_captured,
        amount_captured_currency='USD',
    )


def update_order(dto: StripeCheckoutSessionEventDTO):
    """Method for update fields for Order, Discussion, ParticipantOfDiscussion obj """

    user: CustomUser = CustomUser.objects.get(id=dto.customer_id)
    discussion: Discussion = Discussion.objects.select_related('initiator').get(id=dto.discussion_id)

    participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.select_related('user').get(
        user=user, discussion=discussion)

    if dto.payment_status == 'paid' and dto.is_initiator:
        participant.update_fields(number_of_paid_seats=1)
        # activate discussion
        discussion.update_fields(is_active=True, paid=True)
        # send notification to initiator of discussion that discussion was activated
        title: str = "📯 Discussion activated 📯"
        body: str = f"Congratulations 🎊! You discussion {discussion.topic} was succesfully activated and now it visible for every user"
        payload = {"discussion_id": f'{discussion.pk}'}
        notification: Notification = NotificationRepository.create_notification(
            related_user=discussion.initiator, title=title, body=body, payload=payload)
        push_notification_repository.add_notification_data_to_rdb(
            user_pk=discussion.initiator.pk,
            notification=notification
        )

    if dto.payment_status == 'paid' and not dto.is_initiator:
        participant.update_fields(number_of_paid_seats=1)
        # send notification that place in disscussion was paid
        title: str = "🎊 Participation paid 🎊"
        body: str = f"Congratulations! You were added as participant to discussion {discussion.topic}. We wish you a pleasant conversation 🫶"
        payload = {"discussion_id": f'{discussion.pk}'}
        notification: Notification = NotificationRepository.create_notification(
            related_user=participant.user, title=title, body=body, payload=payload)
        push_notification_repository.add_notification_data_to_rdb(
            user_pk=participant.user.pk,
            notification=notification
        )

    # Updating Order with data recieved from Stripe
    try:
        charge_obj: ChargeData = ChargeData.objects.get(
            payment_intent_id=dto.payment_intent_id)
    except ObjectDoesNotExist as e:
        set_context('charge_data_was_not_found_case', value=e.__dict__)
        capture_message(
            'ChargeData matching query does not exist error', level='error')
        raise ObjectNotFoundError(
            f'ChargeData matching query does not exist')

    charge_obj.update_fields(user=user, discussion=discussion)
    Order.objects.filter(order_representation_id=dto.order_representation_id).update(
        user_email=dto.customer_email,
        charge_id=charge_obj.pk,
        payment_intent_id=dto.payment_intent_id,
        checkout_session_id=dto.checkout_session_id,
        payment_status=dto.payment_status,
        is_confirmed=True
    )
    order: Order = Order.objects.get(
        order_representation_id=dto.order_representation_id)

    html_content = render_to_string(
        'payment_app/paid_check.html', {'order': order, })

    # Generate the PDF
    pdf_file = BytesIO()
    generate_pdf(html_content=html_content, pdf_file=pdf_file)

    # Save the PDF file to the S3 bucket using PDFStorage
    file_name = f'Cheque_{order.order_representation_id}.pdf'
    pdf_path = f'{file_name}'
    pdf_storage = PDFStorage()
    pdf_storage.save(pdf_path, pdf_file)
    order.pdf_check.name = pdf_path
    order.save()


def update_corporate_profile(dto: StripeUpdateAccountEventDTO):
    """Method for update corporate Profile obj"""

    if dto.account.details_submitted == True and dto.account.charges_enabled == True:
        profile = Profile.objects.get(
            company_stripe_account=dto.account.id)
        profile.update_fields(is_verified=True)


def create_refund_obj(dto: StripeRefundEventDTO):
    """Method for create Refund obj"""

    order_for_refund: Order = Order.objects.select_related('user', 'discussion').get(
        payment_intent_id=dto.payment_intent_id)

    order_for_refund.update_fields(refunded=True)
    refund = Refund.objects.create(order=order_for_refund,
                                   amount_refunded=dto.amount_refunded, status=dto.status)

    title: str = "💸 Refund 💸"
    body: str = f"You discussion doesn't rich minimum number of participants and we initiated a refund. You will see the refund as a credit approximately 5-10 business days later, depending upon the bank. You can contact our support if you will not see refund. "
    payload = {"refund_id": f'{refund.pk}',
               "order_id": f'{refund.order.pk}'}
    notification: Notification = NotificationRepository.create_notification(
        related_user=refund.order.user, title=title, body=body, payload=payload)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=refund.order.user.pk,
        notification=notification
    )


def create_payout_data(dto: StripePayoutEventDTO):
    """Method for update discussions after payout and create PayoutData obj"""

    # update PayoutData object and discussions
    for discussion_id in dto.discussion_ids:
        try:
            discussion = Discussion.objects.select_related(
                'place', 'place__owner').get(id=discussion_id)
        except ObjectDoesNotExist as e:
            set_context('discussion_was_not_found_case',
                        value=e.__dict__)
            capture_message(
                'discussion from payout comment does not exist error', level='error')
            raise ObjectNotFoundError(
                f'Discussion matching query does not exist')

        discussion.update_fields(
            is_active=False, deposit_transferred=True)
        try:
            payout: PayoutData = PayoutData.objects.get(
                discussions=dto.discussions)
            payout.update_fields(status=dto.status)
        except ObjectDoesNotExist:
            PayoutData.objects.create(place_owner=discussion.place.owner,
                                      connected_account=discussion.place.owner.profile.company_stripe_account,
                                      external_account=dto.destination,
                                      payout_id=dto.payout_id,
                                      transferred_amount=dto.amount/100,
                                      status=dto.status,
                                      discussions=dto.discussions,
                                      )
        
        app_configs = ApplicationConfig.objects.last()
        phone_number = app_configs.app_phone_number
        user = CustomUser.objects.get(phone_number=phone_number)
        
        title: str = "🪙 Money transfered 🪙"
        body: str = f"Payout for next discussions: {dto.discussion_ids} was made."
        payload = {"payouted_discussions": f'{dto.discussion_ids}'}
        notification: Notification = NotificationRepository.create_notification(
            related_user=user, title=title, body=body, payload=payload)
        push_notification_repository.add_notification_data_to_rdb(
            user_pk=user.pk,
            notification=notification
        )


def max_participants_riched_notify(dto: DiscussionNotifydDTO):
    """Method for add notification abt discussion is full to rdb"""

    title: str = "🎉 The maximum number of participants has been reached 🎉"
    body: str = f'Your discussion "{dto.discussion.topic}" has reached the maximum number of participants and is closed for bookings'
    notification: Notification = NotificationRepository.create_notification(
        related_user=dto.discussion.initiator, title=title, body=body)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=dto.discussion.initiator.pk,
        notification=notification
    )


def min_participants_riched_notify(dto: MinParticipantsRichedDTO):
    """Method for add notification abt discussion is full to rdb"""

    app_config = ApplicationConfig.objects.last()
    admin_user: CustomUser = CustomUser.objects.get(
        phone_number=app_config.app_phone_number)

    title: str = "🎉 The minimum number of participants has been reached 🎉"

    for discussion in dto.discussions:
        # notify Sam
        body: str = f'Discussion: ID={discussion.pk}, topic - "{discussion.topic}" has riched the minimum number of participants. You can found it in castom pages of admin panel and make payout.'
        notification: Notification = NotificationRepository.create_notification(
            related_user=admin_user, title=title, body=body)
        push_notification_repository.add_notification_data_to_rdb(
            user_pk=admin_user.pk,
            notification=notification
        )

        # notify initiator
        body: str = f'🎉 Congratulations! 🎉 Your discussion "{discussion.topic}" has reached the minimum number of participants and will now definitely take place. We wish you a pleasant conversation.'
        notification: Notification = NotificationRepository.create_notification(
            related_user=discussion.initiator, title=title, body=body)
        push_notification_repository.add_notification_data_to_rdb(
            user_pk=discussion.initiator.pk,
            notification=notification
        )

        discussion.update_fields(is_checked=True)


def host_request_notify(dto: DiscussionNotifydDTO):
    """Method for create and send notification to place owner from initiator of discussion"""

    new_discussion = Discussion.objects.select_related(
        'place', 'initiator').get(id=dto.discussion.id)
    title: str = "🔔 New request to host discussion 🔔"
    body: str = f"{new_discussion.dt_start.strftime('%d-%m-%Y at %H:%M')} {new_discussion.initiator.profile.user_name} wants to hold a discussion on the topic '{new_discussion.topic}' in your location {new_discussion.place.name} for {new_discussion.number_of_participants} people"
    notification: Notification = NotificationRepository.create_notification(
        related_user=dto.discussion.place.owner, title=title, body=body)
    payload = {"discussion_id": f'{dto.discussion.id}',
               "notification_id": f'{notification.pk}'}
    notification.update_fields(payload=payload)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=dto.discussion.place.owner.pk,
        notification=notification
    )


def pending_host_timed_out_notify(dto: DiscussionNotifydDTO):
    """
        Method for create and send notification to initiator of discussion
        that he need to chose new location for his discussion coz the host's previous request
        did not reply within 24 hours
    """
    discussion = (Discussion.objects
                  .prefetch_related('place__owner')
                  .select_related('place', 'initiator', 'type', 'category', 'theme')
                  .get(pk=dto.discussion.pk))
    title: str = "😞 Host request was ignored 😞"
    body: str = f"Unfortunately, the owner of the chosen place '{discussion.place.name}' did not confirm the discussion on the topic '{discussion.topic}'. You can contact him personally by phone number: {discussion.place.owner.phone_number} or choose another location."
    payload = {'discussion_id': str(discussion.pk)}
    notification: Notification = NotificationRepository.create_notification(
        related_user=discussion.initiator, title=title, body=body, payload=payload)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=discussion.initiator.pk,
        notification=notification
    )


def host_deny_response_notify(dto: DiscussionNotifydDTO):
    """Method for notify discussion initiator about deny response from place owner"""

    discussion = Discussion.objects.select_related(
        'place', 'initiator').get(pk=dto.discussion.pk)

    title: str = "😭 Host request was denied 😭"
    body: str = f"Unfortunately, the owner of the chosen place '{discussion.place.name}' did not confirm the discussion on the topic '{discussion.topic}'. You can choose another location 😉."
    payload = {'discussion_id': str(discussion.pk)}
    notification: Notification = NotificationRepository.create_notification(
        related_user=discussion.initiator, title=title, body=body, payload=payload)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=discussion.initiator.pk,
        notification=notification
    )


def host_accept_response_notify(dto: DiscussionNotifydDTO):
    """Method for notify discussion initiator about deny response from place owner"""

    discussion = Discussion.objects.select_related(
        'place', 'initiator').get(pk=dto.discussion.pk)

    title: str = "🤩 Host request was accepted 🤩"
    body: str = f"Congrats 🥳! The owner of the chosen place '{discussion.place.name}' did confirm the discussion on the topic '{discussion.topic}'. Now you can make a deposit 💵 to participate and your discussion will become visible to all users."
    payload = {'discussion_id': str(discussion.pk)}
    notification: Notification = NotificationRepository.create_notification(
        related_user=discussion.initiator, title=title, body=body, payload=payload)
    push_notification_repository.add_notification_data_to_rdb(
        user_pk=discussion.initiator.pk,
        notification=notification
    )
