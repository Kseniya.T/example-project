from decimal import Decimal
from enum import Enum
from typing import Any, Optional
from moneyed import Money
from dataclasses import dataclass


from pydantic import BaseModel
from apps.discussion_app.models import Discussion

from core.settings import (RABBITMQ_APP, RABBITMQ_APP_GROUP,
                           RABBITMQ_PREFIX)


class QueuesEnum(Enum):
    """
    INBOUND:'dead.inbound'
    OUTBOUND:'dead.outbound'
    PUSH_NOTIFICATION_INBOUND:'push.notification.dead.inbound'
    """
    INBOUND = 'dead.inbound'
    OUTBOUND = 'dead.outbound'
    PUSH_NOTIFICATION_INBOUND = 'push.notification.dead.inbound'

    def __str__(self):
        return (
            f'{RABBITMQ_PREFIX}.'
            f'{RABBITMQ_APP_GROUP}.'
            f'{RABBITMQ_APP}.'
            f'{self.value}'
        )


class EventEnum(Enum):
    """
    NEW_KEY: 'new_key'
    NEW_SESSION: 'new_session'
    ENCRYPTED_KEY: 'encrypted_key'
    """
    NEW_KEY = 'new_key'
    NEW_SESSION = 'new_session'
    ENCRYPTED_KEY = 'encrypted_key'


@dataclass
class StripeChargeEventDTO:
    """
        event_type: str
        charge_id: str
        charge_status: str
        payment_intent_id: str
        amount_captured: Money
    """

    event_type: str
    charge_id: str
    charge_status: str
    payment_intent_id: str
    amount_captured: Money


@dataclass
class StripeCheckoutSessionEventDTO:
    """
        event_type: str
        checkout_session_id: str
        payment_status: str
        customer_email: str
        payment_intent_id: str
        discussion_id: int
        customer_id: int
        order_representation_id: str
        is_initiator: bool
    """

    event_type: str
    checkout_session_id: str
    payment_status: str
    customer_email: str
    payment_intent_id: str
    discussion_id: int
    customer_id: int
    order_representation_id: str
    is_initiator: bool


@dataclass
class StripeUpdateAccountEventDTO:
    """
        event_type: str
        account: Any
    """

    event_type: str
    account: Any


@dataclass
class StripeRefundEventDTO:
    """
        event_type: str
        payment_intent_id: str
        amount_refunded: Money
        status: str        
    """

    event_type: str
    payment_intent_id: str
    amount_refunded: Money
    status: str


@dataclass
class StripePayoutEventDTO:
    """
        event_type: str
        payout_id: str
        destination: str
        status: str
        amount: Money
        discussion_ids: list[int]
    """

    event_type: str
    payout_id: str
    destination: str
    status: str
    amount: Money
    discussion_ids: list[int]
    discussions: str


@dataclass
class DiscussionNotifydDTO:
    """
        event_type: str
        discussion: Discussion
    """
    event: str
    discussion: Discussion


@dataclass
class MinParticipantsRichedDTO:
    """
        event_type: str
        discussions: list[Discussion]
    """
    event: str
    discussions: list[Discussion]
