from django.contrib import admin

from apps.place_app.models import Dishes, PlaceImages, PlaceOpeningHours



class PlaceDishesStackedInline(admin.StackedInline):
    """PlaceDishesStackedInline of Place"""
    
    model = Dishes
    verbose_name = 'dish'
    verbose_name_plural = 'dishes'
    extra = 0
    readonly_fields = ('place', 'name', 'image',)
    show_change_link = True
    can_delete = False
    
    def has_add_permission(self, request, obj=None):
        return False


class PlaceOpeningHoursStackedInline(admin.StackedInline):
    """PlaceOpeningHoursStackedInline of Place"""
    
    model = PlaceOpeningHours
    verbose_name = 'opening hours'
    verbose_name_plural = 'opening hours'
    extra = 0
    readonly_fields = ('weekday', 'from_hour', 'to_hour', 'is_dayoff')
    show_change_link = True
    can_delete = False
    
    def has_add_permission(self, request, obj=None):
        return False


class PlaceImagesTabularInline(admin.TabularInline):
    """PlaceImagesTabularInline for Place"""
    
    model =  PlaceImages
    verbose_name = 'place image'
    verbose_name_plural = 'place images'
    extra = 0
    readonly_fields = ('image', )
    show_change_link = True
    can_delete = False
    
    def has_add_permission(self, request, obj=None):
        return False
