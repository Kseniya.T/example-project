from decimal import Decimal
from django.db import models
from django.core.validators import FileExtensionValidator
from apps.place_app.choices import CurrencyChoices, WeekDaysChoices
from utils.base_model import AppBaseModel
from phonenumber_field.modelfields import PhoneNumberField
from moneyed.classes import Money

from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator

from apps.user_app.models import CustomUser
from core.storage_backends import PublicMediaStorage


class PlaceType(AppBaseModel):
    """PlaceType model"""

    name = models.CharField(verbose_name='Type of place', max_length=500)
    image = models.ImageField(verbose_name='Place type image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
        'jpeg', 'jpg', 'png', 'webp'])], blank=True, null=True, storage=PublicMediaStorage())

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name = 'Place type'
        verbose_name_plural = 'Place types'


class Place(AppBaseModel):
    """Place model"""

    owner = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, verbose_name='Owner', related_name='place')
    name = models.CharField(verbose_name='Name', max_length=500)
    type = models.ManyToManyField(PlaceType, verbose_name='Type of place', related_name='place')
    address = models.CharField(verbose_name='Address', max_length=500)
    description = models.TextField(
        verbose_name='Description', blank=True, null=True)
    cover_image = models.ImageField(verbose_name='Cover_image', max_length=250, validators=[FileExtensionValidator(allowed_extensions=[
                              'jpeg', 'jpg', 'png', 'webp'])], storage=PublicMediaStorage(), null=True)
    menu = models.URLField(max_length=300, blank=True, null=True)
    deposit = MoneyField(max_digits=14, decimal_places=2, default=Money(Decimal('0.00'), 'USD'), currency_choices=CurrencyChoices.choices,
                         default_currency='USD', verbose_name='Deposit', validators=[MinMoneyValidator(0)])
    discount = models.IntegerField(
        verbose_name='Discount for participants', blank=True, null=True)
    minimum_available_seats = models.IntegerField(
        verbose_name='Minimum seats available', default=2)
    maximum_available_seats = models.IntegerField(
        verbose_name='Maximum seats available')
    email = models.EmailField(verbose_name='Email',  null=True)
    phone_number = PhoneNumberField(verbose_name='Phone Number',  null=True)
    web_site = models.URLField(max_length=300, blank=True, null=True)
    latitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True, verbose_name="Latitude"
    )
    longitude = models.DecimalField(
        max_digits=9, decimal_places=6, null=True, blank=True, verbose_name="Longitude"
    )
    city = models.CharField(verbose_name='City', max_length=500, null=True)
    is_deleted = models.BooleanField(verbose_name='Is deleted', default=False)

    class Meta:
        verbose_name = 'Place'
        verbose_name_plural = 'Places'

    def __str__(self) -> str:
        return f'ID: {self.pk} - "{self.name}"'


class PlaceOpeningHours(AppBaseModel):
    """PlaceOpeningHours model"""

    place = models.ForeignKey(Place, on_delete=models.CASCADE,
                              verbose_name='Place', related_name='opening_hours')
    weekday = models.SmallIntegerField(
        choices=WeekDaysChoices.choices, verbose_name='Week day')
    from_hour = models.TimeField(verbose_name='From hour', null=True)
    to_hour = models.TimeField(verbose_name='To hour', null=True)
    is_dayoff = models.BooleanField(verbose_name='Day off', default=False)

    def __str__(self):
        return f'{self.get_weekday_display()}: {self.from_hour} - {self.to_hour}'

    class Meta:
        verbose_name = 'Opening hours'
        verbose_name_plural = 'Opening hours'
        ordering = ('weekday', 'from_hour')
        unique_together = ('weekday', 'place')


class Dishes(AppBaseModel):
    "Dishies model"

    place = models.ForeignKey(Place, on_delete=models.CASCADE,
                              verbose_name='Place', related_name='dishes',  blank=True, null=True)
    name = models.CharField(verbose_name='Name', max_length=500)
    image = models.ImageField(verbose_name='Image', storage=PublicMediaStorage(), validators=[
                              FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])

    class Meta:
        verbose_name = 'Dish'
        verbose_name_plural = 'Dishes'

    def __str__(self) -> str:
        return self.name


class PlaceImages(AppBaseModel):
    """PlaceImages model"""
    
    place = models.ForeignKey(Place, on_delete=models.CASCADE,
                              verbose_name='Place', related_name='place_images', null=True)
    image = models.ImageField(verbose_name='Image', storage=PublicMediaStorage(), validators=[
                              FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    
    def __str__(self) -> str:
        return f'ID:{self.pk}'
    
    class Meta:
        verbose_name = 'Place image'
        verbose_name_plural = 'Place images'
