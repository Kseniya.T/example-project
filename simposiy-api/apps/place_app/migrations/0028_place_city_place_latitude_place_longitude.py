# Generated by Django 4.1.7 on 2023-05-22 06:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('place_app', '0027_placetype_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='place',
            name='city',
            field=models.CharField(max_length=500, null=True, verbose_name='City'),
        ),
        migrations.AddField(
            model_name='place',
            name='latitude',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Latitude'),
        ),
        migrations.AddField(
            model_name='place',
            name='longitude',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Longitude'),
        ),
    ]
