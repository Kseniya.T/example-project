# Generated by Django 4.1.3 on 2022-11-18 13:12

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('place_app', '0004_remove_dishes_description_remove_dishes_price_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dishes',
            name='image',
            field=models.ImageField(upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], verbose_name='Image'),
        ),
        migrations.AlterField(
            model_name='place',
            name='image',
            field=models.ImageField(blank=True, max_length=250, null=True, upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], verbose_name='Image'),
        ),
    ]
