# Generated by Django 4.1.7 on 2023-05-03 12:11

import core.storage_backends
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('place_app', '0026_alter_place_email_alter_place_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='placetype',
            name='image',
            field=models.ImageField(blank=True, max_length=250, null=True, storage=core.storage_backends.PublicMediaStorage(), upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], verbose_name='Place type image'),
        ),
    ]
