# Generated by Django 4.1.7 on 2023-03-02 12:10

import core.storage_backends
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('place_app', '0020_alter_dishes_image_alter_place_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='place',
            name='image',
        ),
        migrations.CreateModel(
            name='PlaceImages',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(storage=core.storage_backends.PublicMediaStorage(), upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])], verbose_name='Image')),
                ('place', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='local_images', to='place_app.place', verbose_name='Place')),
            ],
            options={
                'verbose_name': 'Place image',
                'verbose_name_plural': 'Place images',
            },
        ),
    ]
