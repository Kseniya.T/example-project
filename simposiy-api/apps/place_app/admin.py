from typing import Tuple, Type
from django.contrib import admin
from apps.place_app.inlines import (PlaceDishesStackedInline,
                                    PlaceImagesTabularInline,
                                    PlaceOpeningHoursStackedInline)

from apps.place_app.models import (Dishes, Place, PlaceImages,
                                   PlaceOpeningHours, PlaceType)


@admin.register(PlaceType)
class PlaceTypeAdmin(admin.ModelAdmin):
    model: Type[PlaceType] = PlaceType
    list_display: tuple = ('id',
                           'name',
                           'image',
                           )


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    model: Type[Place] = Place
    list_display: tuple = ('id',
                           'name',
                           'address',
                           'deposit',
                           'discount',
                           'email',
                           'phone_number',
                           'web_site',
                           'cover_image',
                           )
    inlines = (PlaceDishesStackedInline, PlaceOpeningHoursStackedInline, PlaceImagesTabularInline)
    search_fields: Tuple[str] = ('id', 'name')
    readonly_fields = ['latitude', 'longitude', 'city']


@admin.register(Dishes)
class DishesAdmin(admin.ModelAdmin):
    model: Type[Dishes] = Dishes
    list_display: tuple = ('id',
                           'name',
                           'place',
                           'image',
                           )


@admin.register(PlaceOpeningHours)
class PlaceOpeningHoursAdmin(admin.ModelAdmin):
    model: Type[PlaceOpeningHours] = PlaceOpeningHours
    list_display: tuple = ('id',
                           'place',
                           'weekday',
                           'from_hour',
                           'to_hour',
                           'is_dayoff',
                           )


@admin.register(PlaceImages)
class PlaceImagesAdmin(admin.ModelAdmin):
    model: Type[PlaceImages] = PlaceImages
    list_display: tuple = ('id', 'place', 'image')
