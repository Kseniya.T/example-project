from django.apps import AppConfig


class PlaceAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.place_app'

    def ready(self):
        import apps.place_app.signals
