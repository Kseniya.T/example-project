from dataclasses import dataclass
from decimal import Decimal
from typing import Any, Dict
from moneyed import Money

from apps.place_app.models import Place
from apps.user_app.models import CustomUser


@dataclass
class CreatePlaceDTO:
    """DTO for creating place/location case"""

    name: str
    address: str
    deposit: Money
    type: list[int]
    minimum_available_seats: int
    maximum_available_seats: int
    email: str
    phone_number: str
    description: str = None
    id: int = None
    menu: str = None
    discount: int = None
    cover_image: Any = None
    web_site: str = None
    latitude: Decimal = None
    longitude: Decimal = None
    city: str = None


@dataclass
class UpdatePlaceDTO:
    """DTO for creating place/location case"""

    name: str  = None
    address: str  = None
    description: str  = None
    deposit: Money  = None
    type: list[int]  = None
    minimum_available_seats: int  = None
    maximum_available_seats: int  = None
    email: str  = None
    phone_number: str  = None
    id: int = None
    menu: str = None
    discount: int = None
    cover_image: Any = None
    web_site: str = None
    latitude: Decimal = None
    longitude: Decimal = None
    city: str = None


@dataclass
class PlaceDTO:
    """DTO for updating place case"""

    name: str
    address: str
    description: str
    deposit: Money
    minimum_available_seats: int
    maximum_available_seats: int
    email: str
    phone_number: str
    type: list
    menu: str = None
    discount: int = None
    owner: CustomUser = None
    cover_image: Any = None
    id: int = None
    web_site: str = None
    latitude: Decimal = None
    longitude: Decimal = None
    city: str = None


@dataclass
class FilterPlaceDTO:
    """DTO for FilterPlaceApiView"""

    def __init__(self, **kwargs):
        type_ids = kwargs.get('type', [None])[0]
        self.type_ids = list(map(int, type_ids.split(','))) if type_ids else []
        self.cost_from = kwargs.get('cost_from', [None])[0]
        self.cost_to = kwargs.get('cost_to', [None])[0]


@dataclass
class CreateDishDTO:
    """DTO for DishSerializer"""

    dishes: list[Dict]


@dataclass
class UpdateDishesDTO:
    """DTO for UpdateDishesSerializer"""
    
    name: str
    image: Any


@dataclass
class DishesDTO:
    """DTO used in CreateDishApiView"""

    place: Place
    dishes: list[Dict]


@dataclass
class CreatePlaceOpeningHoursDTO:
    """DTO for creating place opening hours case"""

    weekdays: list[Dict]


@dataclass
class PlaceOpeningHoursDTO:
    """DTO for place opening hours of place"""

    place: Place
    weekdays: list[Dict]

@dataclass
class CreatePlaceImagesDTO:
    """DTO for PlaceImagesSerializer"""

    images: list[Dict]


@dataclass
class PlaceImagesDTO:
    """DTO used in CreatePlaceImagesApiView"""

    place: Place
    images: list[Dict]
