import base64
from datetime import datetime, timedelta
from decimal import Decimal
from django.http import Http404
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from rest_framework import views
from rest_framework.permissions import AllowAny
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from rest_framework.pagination import LimitOffsetPagination
from django.db.models import Count, Q
from django.core.paginator import Paginator
from django.http import JsonResponse

from apps.place_app.dto import CreateDishDTO, CreatePlaceDTO, CreatePlaceImagesDTO, CreatePlaceOpeningHoursDTO, DishesDTO, FilterPlaceDTO, PlaceDTO, PlaceImagesDTO, PlaceOpeningHoursDTO, UpdateDishesDTO
from apps.place_app.models import Dishes, Place, PlaceImages, PlaceOpeningHours, PlaceType
from apps.place_app.serializers import (CreateDishSerializer, CreatePlaceImagesSerializer, CreatePlaceOpeningHoursSerializer, CreatePlaceSerializer, DeletePlaceImagesSerializer, ListOfPlaceBestDishesSerializer, ListOfPlaceImagesSerializer, ListOfPlaceOpeningHoursSerializer, PlaceListSerializer,
                                        PlaceSerializer, PlaceTypeSerializer, UpdateDishesSerializer, UpdatePlaceOpeningHoursSerializer, UpdatePlaceSerializer)
from apps.user_app.models import CustomUser, Profile
from repositories.place_app_repository import PlaceAppRepository
from utils.exceptions import ObjectNotFoundError, PermissionDeniedError
from utils.generics.base_generics import BaseGenericAPIView, BaseListAPIView, CustomRetrieveAPIView
from utils.permissions import IsAuthenticated, IsCorporate
from utils.response import SuccessResponse


class ListPlaceTypeView(BaseListAPIView):
    """View of place types list"""

    serializer_class = PlaceTypeSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        place_types = cache.get('place_types')
        if place_types is None:
            place_types = PlaceType.objects.all()
            cache.set('place_types', place_types)
        return place_types


class PlaceTypeDetailRetrieveAPIView(CustomRetrieveAPIView):
    """View for view type details"""

    serializer_class = PlaceTypeSerializer
    permission_classes = [AllowAny, ]
    lookup_field: int = 'pk'
    not_found_message: str = 'There is no such place type registered'
    queryset = PlaceType.objects.all()


class PlaceDetailRetrieveAPIView(CustomRetrieveAPIView):
    """View for view place details"""

    serializer_class = PlaceListSerializer
    lookup_field: int = 'pk'
    not_found_message: str = 'There is no such place registered'
    queryset = Place.objects.select_related('owner').prefetch_related('owner__profile',
                                                                      'opening_hours', 'dishes', 'place_images', 'type').all()


class CreatePlaceApiView(BaseGenericAPIView):
    """View for create place"""

    serializer_class = CreatePlaceSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def post(self, request, *args, **kwargs):
        owner: CustomUser = request.user.user
        serializer: CreatePlaceSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        # deposit_currency: str = serializer.validated_data.pop(
        #     'deposit_currency', None)
        # deposit: Decimal = serializer.validated_data.get('deposit', None)
        # deposit.currency = deposit_currency
        place_dto: CreatePlaceDTO = serializer.dto
        # place_dto.deposit = deposit
        new_place: Place = PlaceAppRepository.create_place(
            owner=owner, dto=place_dto)
        _serializer = PlaceSerializer(new_place)

        return SuccessResponse(
            status=HTTP_201_CREATED,
            detail=f'New location was successfully created',
            obj=_serializer.data
        )


class UpdatePlaceApiView(BaseGenericAPIView):
    """View for update place"""

    serializer_class = UpdatePlaceSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail='Place with id={pk} does not exist yet')

    def patch(self, request, pk, *args, **kwargs):
        """Update place data"""

        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError

        serializer: PlaceSerializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        # deposit_currency: str = serializer.validated_data.pop(
        #     'deposit_currency', None)
        # deposit: Decimal = serializer.validated_data.get('deposit', None)
        # if deposit:
        #     deposit.currency = deposit_currency if deposit_currency else 'USD'
        dto: PlaceDTO = serializer.dto
        # dto.deposit = deposit
        updated_place: Place = PlaceAppRepository.partial_update_place(
            place=place, dto=dto)
        _serializer: PlaceSerializer = PlaceSerializer(updated_place)

        return SuccessResponse(detail='Data were successfully updated', obj=_serializer.data)


class FilterPlaceApiView(BaseGenericAPIView):
    """View for filter place by date, time, cost and type"""

    serializer_class = PlaceListSerializer
    permission_classes = [AllowAny]
    pagination_class = LimitOffsetPagination

    def get(self, request):
        filter_params = request.query_params
        dto = FilterPlaceDTO(**filter_params)

        queryset = PlaceAppRepository.filter_places(dto=dto)

        page = self.paginate_queryset(queryset)
        serializer: PlaceListSerializer = self.serializer_class(
            page, many=True)
        return self.get_paginated_response(serializer.data)


class CreateUpdatePlaceOpeningHoursApiView(BaseGenericAPIView):
    """View for create dish"""

    serializer_class = CreatePlaceOpeningHoursSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').prefetch_related('type').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def post(self, request, pk, *args, **kwargs):
        """Create place opening hours"""

        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError

        serializer: CreatePlaceOpeningHoursSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: CreatePlaceOpeningHoursDTO = serializer.dto
        weekdays_list: list[PlaceOpeningHours] = PlaceAppRepository.create_opening_hours(
            place=place, dto=dto)
        place_opening_hours_dto: PlaceOpeningHoursDTO = PlaceOpeningHoursDTO(
            place=place, weekdays=weekdays_list)
        _serializer: CreatePlaceOpeningHoursSerializer = self.serializer_class(
            place_opening_hours_dto)

        return SuccessResponse(
            status=HTTP_201_CREATED,
            obj=_serializer.data
        )

    def patch(self, request, pk, *args, **kwargs):
        """Update place opening hours"""

        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError

        serializer: UpdatePlaceOpeningHoursSerializer = UpdatePlaceOpeningHoursSerializer(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: CreatePlaceOpeningHoursDTO = serializer.dto
        updated_schedule: list = PlaceAppRepository.update_opening_hours(
            place=place, dto=dto)
        place_opening_hours_dto: PlaceOpeningHoursDTO = PlaceOpeningHoursDTO(
            place=place, weekdays=updated_schedule)
        _serializer = UpdatePlaceOpeningHoursSerializer(
            place_opening_hours_dto)

        return SuccessResponse(detail='Data were successfully updated', obj=_serializer.data)


class DeletePlaceOpeningHoursApiView(views.APIView):
    """View for delete opening hours"""

    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def delete(self, request, pk, **kwargs):
        """View for delete interests"""
        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError()

        try:
            place_schedule: PlaceOpeningHours = PlaceOpeningHours.objects.filter(
                place=place)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        place_schedule.delete()

        return SuccessResponse(detail=f"Schedule was deleted", status=HTTP_200_OK)


class CreateDishApiView(BaseGenericAPIView):
    """View for create dish"""

    serializer_class = CreateDishSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').prefetch_related('type').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def post(self, request, pk, *args, **kwargs):
        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError

        serializer: CreateDishSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: CreateDishDTO = serializer.dto
        new_dishes_obj_list: list[Dishes] = PlaceAppRepository.create_dishes(
            place=place, dto=dto)
        dishes_dto: DishesDTO = DishesDTO(
            place=place, dishes=new_dishes_obj_list)
        _serializer = self.serializer_class(dishes_dto)

        return SuccessResponse(
            status=HTTP_201_CREATED,
            detail=f'New dishes were successfully created',
            obj=_serializer.data
        )


class UpdateDishApiView(views.APIView):
    """View for update dishes"""

    serializer_class = UpdateDishesSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').prefetch_related('dishes', 'type').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def patch(self, request, pk, **kwargs):
        """View for update dish"""
        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError()

        dish_id: int = self.kwargs['dish_id']
        try:
            dish: Dishes = place.dishes.get(id=dish_id)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError(
                detail=f'{place.name} has no dish with ID={dish_id} for update')

        serializer: UpdateDishesSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        dto: UpdateDishesDTO = serializer.dto
        updated_dish: Dishes = PlaceAppRepository.partial_update_dish(
            dish=dish, dto=dto)
        _serializer: UpdateDishesSerializer = self.serializer_class(
            updated_dish)

        return SuccessResponse(detail='Data were successfully updated', obj=_serializer.data)


class ListOfPlaceBestDishesView(BaseListAPIView):
    """View for list of best dishes of particular place"""

    serializer_class = ListOfPlaceBestDishesSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        pk = self.kwargs['pk']
        place: Place = Place.objects.get(id=pk)
        try:
            result = Dishes.objects.select_related('place').filter(place=place)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        return result


class ListOfPlaceOpeningHoursView(BaseListAPIView):
    """View for list of opening hours of particular place"""

    serializer_class = ListOfPlaceOpeningHoursSerializer
    permission_classes = [AllowAny, ]

    def get_queryset(self):
        pk = self.kwargs['pk']
        place: Place = Place.objects.get(id=pk)
        try:
            result = PlaceOpeningHours.objects.select_related(
                'place').filter(place=place)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        return result


class DeleteDishApiView(views.APIView):
    """View for delete dish"""

    serializer_class = CreateDishSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def delete(self, request, pk, **kwargs):
        place: Place = Place.objects.select_related(
            'owner',).prefetch_related('dishes', 'type').get(id=pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError()

        dish_id: int = self.kwargs['dish_id']
        dish: Dishes = place.dishes.get(id=dish_id)
        if not dish:
            raise ObjectNotFoundError

        PlaceAppRepository.delete_dish(dish=dish)

        return SuccessResponse(detail='Dish was successfully deleted', status=HTTP_200_OK)


class ListPlaceView(BaseListAPIView):
    """View for list of all places"""

    serializer_class = PlaceListSerializer
    permission_classes = [AllowAny, ]
    pagination_class = LimitOffsetPagination

    def get(self, request):
        type_ids = request.GET.getlist('type_ids')
        if type_ids:
            type_ids_list = [int(type_id)
                             for type_id in type_ids[0].split(',')]
            places = (Place.objects.select_related('owner')
                      .prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type')
                      .filter(Q(type__in=type_ids_list)).exclude(is_deleted=True))

            result = places.order_by('-id')
            page = self.paginate_queryset(result)
            serializer: PlaceListSerializer = self.serializer_class(
                page, many=True)
            return self.get_paginated_response(serializer.data)

        places = (Place.objects.select_related('owner')
                  .prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type')
                  .all().exclude(is_deleted=True))
        result = places.order_by('-id')
        page = self.paginate_queryset(result)
        serializer: PlaceListSerializer = self.serializer_class(
            page, many=True)
        return self.get_paginated_response(serializer.data)


class ListNewPlacesView(ListPlaceView):
    """View for list of 10 new places"""

    def get_queryset(self):
        places = super().get_queryset()
        return places.order_by('-pk')[:10]


class ListMyPlacesView(BaseListAPIView):
    """View for list of all places of authorized user"""

    serializer_class = PlaceListSerializer
    permission_classes = [IsAuthenticated, IsCorporate]
    ordering = ['-created_at']

    def get_queryset(self):
        owner: CustomUser = self.request.user.user
        return Place.objects.select_related('owner').prefetch_related('owner__profile', 'opening_hours', 'dishes', 'place_images', 'type').filter(owner=owner).exclude(is_deleted=True)


class ListPlaceOfSomeOwnerView(BaseListAPIView):
    """View for list places of someone else"""

    serializer_class = PlaceListSerializer
    permission_classes = [IsAuthenticated]
    ordering = ['-created_at']

    def get_queryset(self):
        profile_id: int = self.kwargs['pk']
        try:
            profile: Profile = Profile.objects.select_related(
                'user').get(id=profile_id)
        except Profile.DoesNotExist:
            raise ObjectNotFoundError
        owner: CustomUser = profile.user
        try:
            some_owner_places = PlaceAppRepository.get_all_places_of_some_owner(
                owner_id=owner.id)
        except:
            raise ObjectNotFoundError
        return some_owner_places


class CreatePlaceImagesAPIView(BaseGenericAPIView):
    """View for create images of place"""

    serializer_class = CreatePlaceImagesSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').prefetch_related('type').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def post(self, request, pk, *args, **kwarg):
        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError()

        querydict = request.data
        data: dict = PlaceAppRepository.convert_inmemoryfile_to_files(
            querydict=querydict)

        serializer: CreatePlaceImagesSerializer = self.serializer_class(
            data=data)
        serializer.is_valid(raise_exception=True)
        dto: CreatePlaceImagesDTO = serializer.dto
        new_images_obj_list: list[PlaceImages] = PlaceAppRepository.create_images(
            place=place, dto=dto)
        images_dto: PlaceImagesDTO = PlaceImagesDTO(
            place=place, images=new_images_obj_list)
        _serializer = self.serializer_class(images_dto)

        return SuccessResponse(
            status=HTTP_201_CREATED,
            detail=f'New images were successfully added',
            obj=_serializer.data
        )


class ListOfPlaceImagesView(BaseListAPIView):
    """View for list of images of particular place"""

    serializer_class = ListOfPlaceImagesSerializer
    permission_classes = [AllowAny, ]
    ordering = ['-created_at']

    def get_queryset(self):
        pk = self.kwargs['pk']
        try:
            result = PlaceImages.objects.filter(place_id=pk)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        return result


class DeletePlaceImagesApiView(views.APIView):
    """View for delete images"""

    serializer_class = DeletePlaceImagesSerializer
    permission_classes = [IsAuthenticated, IsCorporate]

    def get_object(self, pk):
        try:
            return Place.objects.select_related('owner').get(pk=pk)
        except (Place.DoesNotExist, ValueError):
            raise ObjectNotFoundError(
                detail=f'Place with id={pk} does not exist yet')

    def delete(self, request, pk, **kwargs):
        """View for delete interests"""
        place: Place = self.get_object(pk)

        # Check if user is owner of the place
        user: CustomUser = request.user.user
        if user != place.owner:
            raise PermissionDeniedError()

        serializer: DeletePlaceImagesSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        image_id = serializer.data.get('id')
        try:
            place_image: PlaceImages = PlaceImages.objects.get(id=image_id)
        except ObjectDoesNotExist:
            raise ObjectNotFoundError
        PlaceAppRepository.delete_image(image=place_image)

        return SuccessResponse(detail=f"Image was successfully deleted", status=HTTP_200_OK)
