from django.urls import path
from .views import (CreateDishApiView, CreatePlaceApiView, CreatePlaceImagesAPIView, CreateUpdatePlaceOpeningHoursApiView, DeleteDishApiView, DeletePlaceImagesApiView, DeletePlaceOpeningHoursApiView, FilterPlaceApiView, ListMyPlacesView, ListOfPlaceBestDishesView, ListOfPlaceOpeningHoursView, ListPlaceOfSomeOwnerView,
                    ListPlaceTypeView, ListPlaceView, ListNewPlacesView, ListOfPlaceImagesView, PlaceDetailRetrieveAPIView, PlaceTypeDetailRetrieveAPIView, UpdateDishApiView, UpdatePlaceApiView)


app_name = 'place_app'

urlpatterns = [
    # Place
    path('place/type-list/', ListPlaceTypeView.as_view(), name='place-types-list'),
    path('place/type/<int:pk>',
         PlaceTypeDetailRetrieveAPIView.as_view(), name='place-type-details'),
    path('place/details/<int:pk>',
         PlaceDetailRetrieveAPIView.as_view(), name='place-details'),
    path('place/create/', CreatePlaceApiView.as_view(), name='place-create'),
    path('place/update/<int:pk>', UpdatePlaceApiView.as_view(), name='place-update'),
    path('place/list/', ListPlaceView.as_view(), name='place-list'),
    path('newplaces/list/', ListNewPlacesView.as_view(), name='newplaces-list'),
    path('myplace/list/', ListMyPlacesView.as_view(), name='my-place-list'),
    path('<int:pk>/place/list/', ListPlaceOfSomeOwnerView.as_view(),
         name='owner-place-list'),
    path('place/filter/', FilterPlaceApiView.as_view(), name='place-filter'),

    # Opening hours
    path('place/<int:pk>/openinghours/create/',
         CreateUpdatePlaceOpeningHoursApiView.as_view(), name='opening-hours-create'),
    path('place/<int:pk>/openinghours/update/',
         CreateUpdatePlaceOpeningHoursApiView.as_view(), name='opening-hours-update'),
    path('place/<int:pk>/openinghours/list/',
         ListOfPlaceOpeningHoursView.as_view(), name='list-opening-hours'),
    path('place/<int:pk>/openingjours/delete/', DeletePlaceOpeningHoursApiView.as_view(), name='opening-hours-delete'),


    # Best dishes
    path('place/<int:pk>/dish/create/',
         CreateDishApiView.as_view(), name='dish-create'),
    path('place/<int:pk>/dish/<int:dish_id>/update/',
         UpdateDishApiView.as_view(), name='dish-update'),
    path('place/<int:pk>/dish/<int:dish_id>/delete/',
         DeleteDishApiView.as_view(), name='dish-delete'),
    path('place/<int:pk>/dishes/list/',
         ListOfPlaceBestDishesView.as_view(), name='list-dishes'),

    # Images
    path('place/<int:pk>/images/add/', CreatePlaceImagesAPIView.as_view(), name='images-add'),
    path('place/<int:pk>/images/list/', ListOfPlaceImagesView.as_view(), name='place-images-list'),
    path('place/<int:pk>/images/delete/',
         DeletePlaceImagesApiView.as_view(), name='image-delete'),
]
