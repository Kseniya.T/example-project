from django.db.models import TextChoices, IntegerChoices


class CurrencyChoices(TextChoices):
    USD = 'USD', 'USD'


class WeekDaysChoices(IntegerChoices):
    monday = 1, 'Monday'
    tuesday = 2, 'Tuesday'
    wednesday = 3, 'Wednesday'
    thursday = 4, 'Thursday'
    friday = 5, 'Friday'
    saturday = 6, 'Saturday'
    sunday = 7, 'Sunday'

    @classmethod
    def get_choice_label_by_value(cls, value) -> str:
        for choice in cls.choices:
            if value in choice:
                return choice[1]
            continue
