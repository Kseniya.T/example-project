from decimal import Decimal
from rest_framework import serializers
from djmoney.contrib.django_rest_framework import MoneyField
from django.core.validators import FileExtensionValidator
from phonenumber_field.serializerfields import PhoneNumberField

from drf_extra_fields.fields import Base64ImageField
from apps.place_app.choices import CurrencyChoices, WeekDaysChoices

from apps.place_app.dto import CreateDishDTO, CreatePlaceDTO, CreatePlaceImagesDTO, CreatePlaceOpeningHoursDTO, PlaceDTO, UpdateDishesDTO, UpdatePlaceDTO
from apps.place_app.models import Dishes, Place, PlaceImages, PlaceOpeningHours, PlaceType
from apps.user_app.models import CustomUser
from apps.user_app.serializers import CreateProfileSerializer
from utils.custom_serializer import MainSerializer


class BoundedUserProfileSerializerForPlace(serializers.Serializer):
    """User profile serializer"""

    profile = CreateProfileSerializer(read_only=True)


class PlaceTypeSerializer(serializers.Serializer):
    """Serializer for PlaceTypeAPIView"""

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    image = Base64ImageField(required=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])


class DishSerializer(MainSerializer):
    """Serializer for Dishes"""

    dto_class = CreateDishDTO

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    image = Base64ImageField(write_only=True, required=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    image_url = serializers.SerializerMethodField()

    def get_image_url(self, obj: Dishes):
        return obj.image.url if obj.image else None


class PlaceSerializer(MainSerializer):
    """
    Serializer for UpdatePlaceApiView.
    Also used in DiscussionRetrieveSerializer, DiscussionListSerializer
    """

    dto_class = PlaceDTO

    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    name = serializers.CharField()
    type = PlaceTypeSerializer(many=True)
    address = serializers.CharField()
    description = serializers.CharField()
    cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    menu = serializers.URLField(required=False)
    deposit = MoneyField(max_digits=14, decimal_places=2, )
    deposit_currency = serializers.ChoiceField(
        choices=CurrencyChoices.choices)
    discount = serializers.IntegerField(required=False)
    dishes = DishSerializer(read_only=True, many=True)
    minimum_available_seats = serializers.IntegerField()
    maximum_available_seats = serializers.IntegerField()
    email = serializers.EmailField()
    phone_number = PhoneNumberField()
    web_site = serializers.URLField(required=False)
    latitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    city = serializers.CharField(required=False)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation


class CreatePlaceSerializer(MainSerializer):
    """Serializer for CreatePlaceAPIView"""

    dto_class = CreatePlaceDTO

    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    name = serializers.CharField()
    type = serializers.PrimaryKeyRelatedField(queryset=PlaceType.objects.all(), many=True)
    address = serializers.CharField()
    description = serializers.CharField(required=False)
    cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    menu = serializers.URLField(required=False)
    deposit = MoneyField(max_digits=14, decimal_places=2)
    deposit_currency = serializers.ChoiceField(required=False,
        choices=CurrencyChoices.choices)
    discount = serializers.IntegerField(required=False)
    minimum_available_seats = serializers.IntegerField(default=2)
    maximum_available_seats = serializers.IntegerField()
    email = serializers.EmailField()
    phone_number = PhoneNumberField()
    web_site = serializers.URLField(required=False)
    latitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    city = serializers.CharField(required=False)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation


class UpdatePlaceSerializer(MainSerializer):
    """Serializer for UpdatePlaceAPIView"""

    dto_class = UpdatePlaceDTO

    id = serializers.IntegerField(read_only=True)
    owner = serializers.PrimaryKeyRelatedField(
        required=False, queryset=CustomUser.objects.all())
    name = serializers.CharField(required=False)
    type = serializers.PrimaryKeyRelatedField(required=False, queryset=PlaceType.objects.all(), many=True)
    address = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    menu = serializers.URLField(required=False)
    deposit = MoneyField(required=False, max_digits=14, decimal_places=2)
    deposit_currency = serializers.ChoiceField(required=False, 
        choices=CurrencyChoices.choices)
    discount = serializers.IntegerField(required=False)
    minimum_available_seats = serializers.IntegerField(required=False)
    maximum_available_seats = serializers.IntegerField(required=False)
    email = serializers.EmailField(required=False)
    phone_number = PhoneNumberField(required=False)
    web_site = serializers.URLField(required=False)
    latitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(required=False, max_digits=9, decimal_places=6,)
    city = serializers.CharField(required=False)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation


class PlaceOpeningHoursSerializer(MainSerializer):
    """Serializer for PlaceOpeningHour"""

    dto_class = CreatePlaceOpeningHoursDTO

    id = serializers.IntegerField(read_only=True)
    weekday = serializers.ChoiceField(choices=WeekDaysChoices.choices)
    from_hour = serializers.TimeField(required=False)
    to_hour = serializers.TimeField(required=False)
    is_dayoff = serializers.BooleanField()


class UpdateDishesSerializer(MainSerializer):
    """Serializer for UpdateDishApiView"""
    dto_class = UpdateDishesDTO
    
    name = serializers.CharField()
    image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])

class UpdateOpeningHoursSerializer(MainSerializer):
    """Serializer for PlaceOpeningHour"""

    dto_class = CreatePlaceOpeningHoursDTO

    id = serializers.IntegerField()
    weekday = serializers.ChoiceField(choices=WeekDaysChoices.choices)
    from_hour = serializers.TimeField(required=False)
    to_hour = serializers.TimeField(required=False)
    is_dayoff = serializers.BooleanField()


class CreatePlaceOpeningHoursSerializer(MainSerializer):
    """Serializer for CreateUpdatePlaceOpeningHoursApiView"""
    
    dto_class = CreatePlaceOpeningHoursDTO

    place = serializers.PrimaryKeyRelatedField(
        required=False, queryset=Place.objects.all())
    weekdays = serializers.ListField(child=PlaceOpeningHoursSerializer())


class UpdatePlaceOpeningHoursSerializer(MainSerializer):
    """Serializer for CreateUpdatePlaceOpeningHoursApiView"""
    
    dto_class = CreatePlaceOpeningHoursDTO

    place = serializers.PrimaryKeyRelatedField(
        required=False, queryset=Place.objects.all())
    weekdays = serializers.ListField(child=UpdateOpeningHoursSerializer())


class ListOfPlaceOpeningHoursSerializer(serializers.Serializer):
    """Serializer for ListOfPlaceBestOpeningHoursView"""
    
    id = serializers.IntegerField(read_only=True)
    weekday = serializers.IntegerField(read_only=True)
    from_hour = serializers.TimeField(read_only=True)
    to_hour = serializers.TimeField(read_only=True)
    is_dayoff = serializers.BooleanField(read_only=True)


class CreateDishSerializer(MainSerializer):
    """Serializer for CreateDishesAPIView"""
    
    dto_class = CreateDishDTO

    place = serializers.PrimaryKeyRelatedField(
        required=False, queryset=Place.objects.all())
    dishes = serializers.ListField(child=DishSerializer())


class BoundedPlaceDishesSerializer(serializers.Serializer):
    """Serializer for PlaceListSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_dish_id')
    name = serializers.CharField(read_only=True)
    image_url = serializers.SerializerMethodField(
        read_only=True, method_name='get_image_url')

    def get_image_url(self, obj: Dishes):
        if obj.image:
            return obj.image.url
        return None

    def get_dish_id(self, obj: Dishes):
        if obj.id:
            return obj.id
        return None


class BoundedPlaceImagesSerializer(serializers.Serializer):
    """Serializer for PlaceListSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_image_id')
    image_url = serializers.SerializerMethodField(
        read_only=True, method_name='get_image_url')

    def get_image_url(self, obj: PlaceImages):
        if obj.image:
            return obj.image.url
        return None

    def get_image_id(self, obj: PlaceImages):
        if obj.id:
            return obj.id
        return None


class BoundedPlaceOpeningHoursSerializer(serializers.Serializer):
    """Serializer for PlaceListSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_place_opening_hours_id')
    weekday = serializers.SerializerMethodField(read_only=True, method_name='get_weekday')
    from_hour = serializers.SerializerMethodField(read_only=True, method_name='get_from_hour')
    to_hour = serializers.SerializerMethodField(read_only=True, method_name='get_to_hour')
    is_dayoff = serializers.SerializerMethodField(read_only=True, method_name='get_is_dayoff')

    def get_place_opening_hours_id(self, obj: PlaceOpeningHours):
        if obj.id:
            return obj.id
            return None

    def get_weekday(self, obj: PlaceOpeningHours):
        if obj.weekday:
            return obj.weekday
        return None
    def get_from_hour(self, obj: PlaceOpeningHours):
        if obj.from_hour:
            return obj.from_hour
        return None
    def get_to_hour(self, obj: PlaceOpeningHours):
        if obj.to_hour:
            return obj.to_hour
        return None
    def get_is_dayoff(self, obj: PlaceOpeningHours):
        if obj.is_dayoff:
            return obj.is_dayoff
        return False


class PlaceListSerializer(MainSerializer):
    """
    Serializer use in 
    (PlaceDetailRetrieveAPIView, ListPlaceView, 
    ListMyPlacesView, ListPlaceOfSomeOwnerView)
    """
    dto_class = PlaceDTO

    id = serializers.IntegerField(read_only=True)
    owner = BoundedUserProfileSerializerForPlace(read_only=True)
    name = serializers.CharField(read_only=True)
    type = PlaceTypeSerializer(read_only=True, many=True)
    address = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    cover_image = Base64ImageField(required=False, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    menu = serializers.URLField(read_only=True)
    deposit = MoneyField(required=False, max_digits=14, decimal_places=2)
    deposit_currency = serializers.ChoiceField(
        choices=CurrencyChoices.choices)
    discount = serializers.IntegerField(read_only=True)
    minimum_available_seats = serializers.IntegerField(read_only=True)
    maximum_available_seats = serializers.IntegerField(read_only=True)
    dishes = BoundedPlaceDishesSerializer(read_only=True, many=True)
    opening_hours = BoundedPlaceOpeningHoursSerializer(
        read_only=True, many=True)
    email = serializers.EmailField(read_only=True)
    phone_number = PhoneNumberField(read_only=True)
    web_site = serializers.URLField(read_only=True)
    place_images = BoundedPlaceImagesSerializer(read_only=True, many=True)
    latitude = serializers.DecimalField(read_only=True, max_digits=9, decimal_places=6,)
    longitude = serializers.DecimalField(read_only=True, max_digits=9, decimal_places=6,)
    city = serializers.CharField(read_only=True)

    def to_decimal(self, obj: Place) -> Decimal:
        return obj.deposit.amount
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.latitude and instance.longitude:
            representation['latitude'] = float(representation['latitude'])
            representation['longitude'] = float(representation['longitude'])
        return representation


class PlaceImagesSerializer(MainSerializer):
    """Serializer for Place images"""

    dto_class = CreatePlaceImagesDTO

    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(write_only=True, required=True, max_length=None, validators=[FileExtensionValidator(
        allowed_extensions=['jpeg', 'jpg', 'png', 'webp'])])
    image_url = serializers.SerializerMethodField()

    def get_image_url(self, obj: PlaceImages):
        return obj.image.url if obj.image else None


class CreatePlaceImagesSerializer(MainSerializer):
    """Serializer for CreatePlaceImagesAPIView"""
    
    dto_class = CreatePlaceImagesDTO

    place = serializers.PrimaryKeyRelatedField(
        required=False, queryset=Place.objects.all())
    images = serializers.ListField(child=PlaceImagesSerializer())


class ListOfPlaceImagesSerializer(serializers.Serializer):
    """Serializer for ListOfPlaceImagesView"""

    id = serializers.IntegerField(read_only=True)
    image_url = serializers.SerializerMethodField()

    def get_image_url(self, obj: PlaceImages):
        return obj.image.url if obj.image else None


class DeletePlaceImagesSerializer(serializers.Serializer):
    """Serializer use in DeletePlaceImagesApiView"""

    id = serializers.IntegerField()


class ListOfPlaceBestDishesSerializer(serializers.Serializer):
    """Serializer for ListOfPlaceBestDishesView"""
    
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    image = serializers.SerializerMethodField(method_name='get_image_url')

    def get_image_url(self, obj: Dishes):
        return obj.image.url if obj.image else None


class BoundedPlaceSerializer(serializers.Serializer):
    """Serializer for MyTicketsserializer"""
    
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    address = serializers.CharField(read_only=True)
