from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.core.cache import cache

from .models import PlaceType


@receiver(post_save, sender=PlaceType)
@receiver(post_delete, sender=PlaceType)
def invalidate_place_types_cache(sender, instance, **kwargs):
    # Invalidate the cache for the given key
    cache.delete('place_types')
