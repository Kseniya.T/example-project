from django.contrib import admin

from apps.payment_app.models import Order


class OrderStackedInline(admin.StackedInline):
    """OrderStackedInline of Discussion"""

    model = Order
    verbose_name = 'order'
    verbose_name_plural = 'orders'
    extra = 0
    readonly_fields = (
        'user',
        'user_email',
        'discussion',
        'charge',
        'payment_intent_id',
        'checkout_session_id',
        'quantity',
        'payment_status',
        'created_at',
        'updated_at',
        'is_confirmed',
        'order_representation_id',
        'refunded',
    )
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False


# class RefundStackedInline(admin.StackedInline):
#     """RefundStackedInline of Discussion"""

#     model = Refund
#     verbose_name = 'refund'
#     verbose_name_plural = 'refunds'
#     extra = 0
#     readonly_fields = ('amount_refunded', 'status', 'created_at',)
#     can_delete = False

#     def has_add_permission(self, request, obj=None):
#         return False
