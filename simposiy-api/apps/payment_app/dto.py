from dataclasses import dataclass

from apps.discussion_app.models import Discussion


@dataclass
class ReservationCalculationDTO:
    """DTO for ReservationSerializer"""

    discussion: int
    place: int
    number_of_available_seats: int
    time_until_the_end: str
    payment_later: str
    reservation: str
    checkout_session_id: str


@dataclass
class DiscussionAuthorizePaymentDTO:
    """DTO for CreateCheckoutSessionApiView"""

    discussion: Discussion


@dataclass
class ChargeIdDTO:
    """DTO for charge id"""

    charge_id: str
