from rest_framework import serializers
from apps.discussion_app.models import Discussion

from apps.payment_app.dto import ChargeIdDTO, DiscussionAuthorizePaymentDTO, ReservationCalculationDTO
from apps.place_app.models import Place
from utils.custom_serializer import MainSerializer


class ReservationSerializer(MainSerializer):
    """Serializer for ReservationDetailRetrieveAPIView"""

    dto_class = ReservationCalculationDTO

    place = serializers.PrimaryKeyRelatedField(queryset=Place.objects.all())
    image = serializers.ImageField(source='place.cover_image')
    discussion = serializers.PrimaryKeyRelatedField(
        queryset=Discussion.objects.all())
    number_of_available_seats = serializers.IntegerField(read_only=True)
    time_until_the_end = serializers.CharField(read_only=True)
    payment_later = serializers.CharField(source='place.deposit')
    reservation = serializers.CharField(read_only=True)
    checkout_session_id = serializers.CharField(read_only=True)


class DiscussionPaymentSerializer(MainSerializer):
    """Serializer for CreateCheckoutSessionApiView"""

    dto_class = DiscussionAuthorizePaymentDTO

    discussion = serializers.PrimaryKeyRelatedField(
        queryset=Discussion.objects.all())


class CreateRefundSerializer(MainSerializer):
    """Serializer for CreateRefundApiView"""

    dto_class = ChargeIdDTO

    charge_id = serializers.CharField()


class BoundedDiscussionSerializer(serializers.Serializer):
    """Bounded serializer for PaymentLinkSerializer"""

    id = serializers.SerializerMethodField(
        read_only=True, method_name='get_discussion_id')
    topic = serializers.SerializerMethodField(
        read_only=True, method_name='get_discussion_topic')

    def get_discussion_id(self, obj: Discussion) -> str | None:
        return obj.pk

    def get_discussion_topic(self, obj: Discussion):
        return obj.topic


class PaymentLinkSerializer(serializers.Serializer):
    """Serializer for PaymentLinkView"""

    discussion = BoundedDiscussionSerializer()
    payment_link = serializers.URLField(read_only=True)
