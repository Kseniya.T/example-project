from typing import Tuple, Type
from django.contrib import admin
from apps.payment_app.inlines import OrderStackedInline

from apps.payment_app.models import ChargeData, Order, Refund


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    model_class: Type[Order] = Order
    list_display: Tuple[str] = ('id',
                                'user_id',
                                'user_email',
                                'discussion',
                                'short_payment_intent_id',
                                'short_checkout_session_id',
                                'charge',
                                'payment_status',
                                'pdf_check',
                                'created_at',
                                'updated_at',
                                'is_confirmed',
                                'refunded',
                                )
    list_filter: Tuple[str] = ('payment_status',)


@admin.register(Refund)
class RefundDataAdmin(admin.ModelAdmin):
    model_class: Type[Refund] = Refund
    list_display: Tuple[str] = ('id',
                                'order',
                                'amount_refunded',
                                'status',
                                'created_at'
                                )
    search_fields: Tuple[str] = ('order__id',)


@admin.register(ChargeData)
class ChargeDataAdmin(admin.ModelAdmin):
    model_class: Type[ChargeData] = ChargeData
    list_display: Tuple[str] = ('id',
                                'user_id',
                                'discussion',
                                'charge_status',
                                'charge_id',
                                'payment_intent_id',
                                'amount_captured',
                                'created_at',
                                )
    inlines = (OrderStackedInline, )
