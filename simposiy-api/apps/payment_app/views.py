import datetime
from sentry_sdk import capture_message, set_context
import stripe
from django.utils import timezone
from django.http import JsonResponse
from django.views.generic import TemplateView
from rest_framework.status import HTTP_200_OK
from apps.notification_app.models import Notification
from apps.payment_app.models import ChargeData, Order, PayoutData, Refund
from apps.rabbitmq_app.enums import QueuesEnum
from apps.rabbitmq_app.producer import ProducerRabbitMQ
from repositories.discussion_app_repository import DiscussionAppRepository
from repositories.payment_app_repository import PaymentAppRepository
from services.stripe_service.dto import CreateCheckoutSessionDTO
from services.stripe_service.stripe_service import StripeService
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import AllowAny
from django.core.exceptions import ObjectDoesNotExist


from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion
from apps.payment_app.dto import DiscussionAuthorizePaymentDTO, ReservationCalculationDTO
from apps.payment_app.serializers import DiscussionPaymentSerializer, PaymentLinkSerializer, ReservationSerializer
from apps.user_app.models import CustomUser, Profile
from core.settings import (RABBITMQ_PRODUCER_PASSWORD, RABBITMQ_PRODUCER_USER, STRIPE_CONNECTED_ACC_WEBHOOK_SECRET,
                           STRIPE_WEBHOOK_SECRET, EXCHANGE_INBOUND,
                           RABBITMQ_HOST)
from utils.exceptions import ObjectNotFoundError
from utils.generics.base_generics import BaseGenericAPIView, BaseListAPIView
from utils.permissions import IsAuthenticated
from utils.response import ErrorResponse, SuccessResponse
from repositories.notification_repository import NotificationRepository
from repositories.init import push_notification_repository


class CreateCheckoutSessionApiView(BaseGenericAPIView):
    """View for create checkout session"""

    serializer_class = DiscussionPaymentSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        user: CustomUser = self.request.user.user
        serializer: DiscussionPaymentSerializer = self.serializer_class(
            data=request.data)
        serializer.is_valid(raise_exception=True)
        discussion_dto: DiscussionAuthorizePaymentDTO = serializer.dto
        amount: int = int(discussion_dto.discussion.place.deposit.amount*100)
        name: str = discussion_dto.discussion.topic
        discussion_id: int = discussion_dto.discussion.pk

        # Check discussion and if it avalibale for payment return tuple(number_of_available_seats: int, is_initiator: bool)
        number_of_available_seats, is_initiator = DiscussionAppRepository.discussion_check(
            user=user, discussion_dto=discussion_dto)

        # create participant with 0 paid seats
        if not is_initiator:
            ParticipantsOfDiscussion.objects.create(
                user=user, discussion=discussion_dto.discussion, number_of_paid_seats=0)

        # Saving data in Order model, add data to CreateCheckoutSessionDTO and create checkout session
        order: Order = Order.objects.create(
            user=user, discussion=discussion_dto.discussion)

        order_representation_id: str = order.order_representation_id

        checkout_session_dto: CreateCheckoutSessionDTO = StripeService.create_checkout_session_dto(
            amount=amount, name=name, dicsussion_id=discussion_id, user_id=user.id, order_representation_id=order_representation_id, is_initiator=is_initiator)

        checkout_session_object = StripeService.create_checkout_session(
            dto=checkout_session_dto)

        # Update order with checkout session
        order.update_fields(
            checkout_session_id=checkout_session_object.stripe_id, payment_link=checkout_session_object.url)

        reservation_calculation_dto: ReservationCalculationDTO = PaymentAppRepository.get_reservation_calculation_dto(
            pk=discussion_id, payment_url=checkout_session_object.url, checkout_session_id=checkout_session_object.stripe_id, number_of_available_seats=number_of_available_seats)
        serializer = ReservationSerializer(reservation_calculation_dto)

        return SuccessResponse(status=HTTP_200_OK, obj=serializer.data)


class SuccessView(TemplateView):
    """View for success page"""

    template_name = "success.html"

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'isSuccess': 'Payment was successfull.'
        })


class CancelView(TemplateView):
    """View for cancel page"""

    template_name = "cancel.html"

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'canceled': 'Payment was canceled'
        })


class StripeWebhookAPIView(BaseGenericAPIView):
    """View for stripe webhooks"""

    @csrf_exempt
    def post(self, request, *args, **kwargs):

        payload = request.body
        sig_header = request.META.get('HTTP_STRIPE_SIGNATURE')
        event = None

        try:
            event = stripe.Webhook.construct_event(
                payload=payload, sig_header=sig_header, secret=STRIPE_WEBHOOK_SECRET
            )
        except ValueError:
            return ErrorResponse(detail='Invalid payload')
        except stripe.error.SignatureVerificationError:
            return ErrorResponse(detail='Invalid signature')

        event_type: str = event.get('type')

        if event_type == 'charge.succeeded':
            charge = event.get('data').get('object')

            data = {'event_type': 'charge.succeeded',
                    'charge_id': charge.id,
                    'charge_status': charge.status,
                    'payment_intent_id': charge.payment_intent,
                    'amount_captured': charge.amount_captured / 100}

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        if event_type == 'checkout.session.completed':
            session = event.get('data').get('object')
            session_metadata = session.metadata
            
            data = {
                'event_type': 'checkout.session.completed',
                'checkout_session_id': session.id,
                'payment_status': session.payment_status,
                'customer_email': session.customer_details.email,
                'payment_intent_id': session.payment_intent,
                'discussion_id': session_metadata.product_id,
                'customer_id': session_metadata.customer_id,
                'order_representation_id': session_metadata.order_representation_id,
                'is_initiator': session_metadata.is_initiator                
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)
            
        if event_type == 'account.updated':
            account = event.get('data').get('object')
            
            data = {'event_type': 'account.updated', 'account': account}

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)
            

        if event_type == 'payout.pending' or event_type == 'payout.in_transit' or event_type == 'payout.failed' or event_type == 'payout.canceled':
            payout = event.get('data').get('object')
            discussion_ids = [int(discussion_id) for discussion_id in (
                payout.statement_descriptor).split(',')]
            
            data = {
                'event_type': event_type,
                'payout_id': payout.stripe_id,
                'destination': payout.destination,
                'status': payout.status,
                'amount': payout.amount,
                'discussion_ids': discussion_ids,
                'discussions': payout.statement_descriptor
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        if event_type == 'payout.paid':
            payout = event.get('data').get('object')
            discussion_ids = [int(discussion_id) for discussion_id in (
                payout.statement_descriptor).split(',')]
            
            data = {
                'event_type': 'payout.paid',
                'payout_id': payout.stripe_id,
                'destination': payout.destination,
                'status': payout.status,
                'amount': payout.amount,
                'discussion_ids': discussion_ids,
                'discussions': payout.statement_descriptor
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        if event_type == "charge.refunded":
            refund = event.get('data').get('object')
            
            data = {
                'event_type':'charge.refunded',
                'payment_intent_id': refund.payment_intent,
                'amount_refunded': refund.amount_refunded / 100,
                'status': refund.status
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        return SuccessResponse()


class StripeConnectedAccWebhookAPIView(BaseGenericAPIView):
    """View for stripe connected accounts webhooks"""

    @csrf_exempt
    def post(self, request, *args, **kwargs):

        payload = request.body
        sig_header = request.META.get('HTTP_STRIPE_SIGNATURE')
        event = None

        try:
            event = stripe.Webhook.construct_event(
                payload=payload, sig_header=sig_header, secret=STRIPE_CONNECTED_ACC_WEBHOOK_SECRET
            )
        except ValueError:
            return ErrorResponse(detail='Invalid payload')
        except stripe.error.SignatureVerificationError:
            return ErrorResponse(detail='Invalid signature')

        event_type: str = event.get('type')

        if event_type == 'account.updated':
            account = event.get('data').get('object')
            
            data = {'event_type': 'account.updated', 'account': account}

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        if event_type == 'payout.pending' or event_type == 'payout.in_transit' or event_type == 'payout.failed' or event_type == 'payout.canceled':
            payout = event.get('data').get('object')
            discussion_ids = [int(discussion_id) for discussion_id in (
                payout.statement_descriptor).split(',')]
            
            data = {
                'event_type': event_type,
                'payout_id': payout.stripe_id,
                'destination': payout.destination,
                'status': payout.status,
                'amount': payout.amount,
                'discussion_ids': discussion_ids,
                'discussions': payout.statement_descriptor
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        if event_type == 'payout.paid':
            payout = event.get('data').get('object')
            discussion_ids = [int(discussion_id) for discussion_id in (
                payout.statement_descriptor).split(',')]
            
            data = {
                'event_type': 'payout.paid',
                'payout_id': payout.stripe_id,
                'destination': payout.destination,
                'status': payout.status,
                'amount': payout.amount,
                'discussion_ids': discussion_ids,
            }

            ProducerRabbitMQ.publish(data=data, queue=str(QueuesEnum.INBOUND), exchange=EXCHANGE_INBOUND,
                                     host=RABBITMQ_HOST, login=RABBITMQ_PRODUCER_USER, password=RABBITMQ_PRODUCER_PASSWORD)

        return SuccessResponse()


class PaymentLinkView(BaseListAPIView):
    """View for retrieve payment links of auth user"""

    serializer_class = PaymentLinkSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user = self.request.user.user
        filter_time = timezone.now() - datetime.timedelta(minutes=28)
        # check user orders before give active links
        PaymentAppRepository.check_user_orders(
            user=user, filter_time=filter_time)
        return Order.objects.filter(user=user, payment_link__isnull=False, created_at__gte=filter_time, payment_status__isnull=True)


class TransferView(BaseGenericAPIView):
    """View for test transfers"""

    permission_classes = [AllowAny, ]

    def post(self, request, *args, **kwargs):
        StripeService.create_transfer()
        return SuccessResponse()


class PayoutView(BaseGenericAPIView):
    """View for test payouts"""

    permission_classes = [AllowAny, ]

    def post(self, request, *args, **kwargs):
        StripeService.create_payout()
        return SuccessResponse()


# class CreateRefundApiView(BaseGenericAPIView):
#     """View for create refund"""

#     serializer_class = CreateRefundSerializer
#     permission_classes = [IsAuthenticated]

#     def post(self, request, *args, **kwargs):
#         serializer: CreateRefundSerializer = self.serializer_class(
#             data=request.data)
#         serializer.is_valid(raise_exception=True)
#         charge_id_dto: ChargeIdDTO = serializer.dto
#         create_refund_dto: CreateRefundDTO = StripeService.create_refund_dto(
#             charge_id=charge_id_dto.charge_id)

#         StripeService.create_refund(dto=create_refund_dto)

#         return SuccessResponse(status=HTTP_200_OK)
