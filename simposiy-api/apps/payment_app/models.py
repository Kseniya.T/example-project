from decimal import Decimal
from uuid import uuid4
import random

from django.db import models
from djmoney.models.validators import MinMoneyValidator
from djmoney.models.fields import MoneyField
from moneyed.classes import Money

from apps.discussion_app.models import Discussion
from apps.place_app.choices import CurrencyChoices
from apps.user_app.models import CustomUser
from core.storage_backends import PDFStorage, PublicMediaStorage
from utils.base_model import AppBaseModel



class ChargeData(AppBaseModel):
    """ChargeData model"""

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='User', related_name='charge_data', null=True)
    discussion = models.ForeignKey(Discussion, on_delete=models.CASCADE,
                                   verbose_name='Discussion', related_name='charge_data', null=True)
    charge_status = models.CharField(
        max_length=30, verbose_name='Charge status')
    charge_id = models.CharField(
        max_length=150, verbose_name='Charge id', unique=True)
    payment_intent_id = models.CharField(
        max_length=150, verbose_name='Payment intent id', unique=True)
    amount_captured = MoneyField(max_digits=14, decimal_places=2, default=Money(Decimal('0.00'), 'USD'), currency_choices=CurrencyChoices.choices,
                         default_currency='USD', verbose_name='Deposit', validators=[MinMoneyValidator(0)])
    created_at = models.DateTimeField(
        verbose_name='Created at', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(
        null=True, verbose_name='Updated at', auto_now=True)

    def __str__(self) -> str:
        return self.charge_id # do not change coz this value used in custom pages for refunds

    class Meta:
        verbose_name = 'Charge'
        verbose_name_plural = 'Charges'


class Order(AppBaseModel):
    """Order model"""

    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='User', related_name='order')
    user_email = models.EmailField(
        verbose_name='User email', null=True)
    discussion = models.ForeignKey(Discussion, on_delete=models.CASCADE,
                                   verbose_name='Discussion', related_name='order')
    charge = models.OneToOneField(ChargeData, on_delete=models.CASCADE,
                                  verbose_name='Charge', related_name='order', null=True)
    payment_intent_id = models.CharField(
        max_length=150, verbose_name='Payment intent id', null=True, unique=True)
    checkout_session_id = models.CharField(
        max_length=150, verbose_name='Checkout session id', null=True, unique=True)
    quantity = models.SmallIntegerField(verbose_name='Quantity', default=1)
    payment_status = models.CharField(
        max_length=30, verbose_name='Payment status', null=True)
    created_at = models.DateTimeField(
        verbose_name='Created at', auto_now_add=True)
    updated_at = models.DateTimeField(
        null=True, blank=True, verbose_name='Updated at', auto_now=True)
    is_confirmed = models.BooleanField(
        default=False, verbose_name='Is confirmed')
    order_representation_id = models.CharField(
        max_length=150, verbose_name='Order representation id', null=True)
    refunded = models.BooleanField(default=False, verbose_name='Refunded')
    payment_link = models.URLField(max_length=1000, verbose_name='Payment link', unique=True, null=True)
    pdf_check = models.FileField(verbose_name='PDF Check', max_length=500, null=True, storage=PDFStorage())

    def __str__(self) -> str:
        return (f'ID: {self.pk}, user_id: {self.user_id}, discussion_id: {self.discussion_id}')

    def save(self, *args, **kwargs) -> None:
        if not self.pk:
            first_part = str(random.randint(1000, 9999))
            second_part = str(random.randint(10, 99))
            third_part = str(random.randint(100000, 999999))

            self.order_representation_id = f"{first_part}-{second_part}-{third_part}"
        return super().save(*args, **kwargs)

    def short_checkout_session_id(self):
        return self.checkout_session_id if not self.checkout_session_id else (self.checkout_session_id[:15] + ' ...')

    def short_payment_intent_id(self):
        return self.payment_intent_id if not self.payment_intent_id else (self.payment_intent_id[:9] + ' ...')

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'


class PayoutData(AppBaseModel):
    """PayoutData model
    """

    place_owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='Place owner', related_name='payout_data', null=True)
    connected_account = models.CharField(max_length=250, verbose_name='Connected account', null=True)
    external_account = models.CharField(max_length=250, verbose_name='External account', null=True)
    payout_id = models.CharField(max_length=250, verbose_name='Payout ID', null=True)
    transferred_amount = MoneyField(max_digits=14, decimal_places=2, default=Money(Decimal('0.00'), 'USD'), currency_choices=CurrencyChoices.choices,
                         default_currency='USD', verbose_name='Transferred amount', validators=[MinMoneyValidator(0)], null=True)
    status = models.CharField(max_length=100, verbose_name='Payout status', null=True)
    discussions = models.CharField(max_length=500, verbose_name='Discussions', null=True)
    created_at = models.DateTimeField(
        verbose_name='Created at', auto_now_add=True)
    updated_at = models.DateTimeField(
        null=True, blank=True, verbose_name='Updated at', auto_now=True)

    def __str__(self) -> str:
        return f'ID: {self.pk}, place_owner: {self.place_owner}'

    class Meta:
        verbose_name = 'Payout data'
        verbose_name_plural = 'Payout data'


class Refund(AppBaseModel):
    """RefundData model"""

    order = models.OneToOneField(Order, on_delete=models.CASCADE,
                                 verbose_name='Order', related_name='refund')
    amount_refunded = MoneyField(max_digits=14, decimal_places=2, default=Money(Decimal('0.00'), 'USD'), currency_choices=CurrencyChoices.choices,
                         default_currency='USD', verbose_name='Deposit', validators=[MinMoneyValidator(0)])
    status = models.CharField(max_length=30, verbose_name='Status')
    created_at = models.DateTimeField(
        verbose_name='Created at', auto_now_add=True, null=True)

    def __str__(self) -> str:
        return (f'ID: {self.pk}, order_id: {self.order_id}')

    class Meta:
        verbose_name = 'Refund'
        verbose_name_plural = 'Refunds'
