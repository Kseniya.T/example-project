from django.urls import path
from .views import (
    CreateCheckoutSessionApiView,
    PaymentLinkView,
    StripeConnectedAccWebhookAPIView,
    StripeWebhookAPIView,
    SuccessView,
    CancelView,
    TransferView,
    PayoutView
)


app_name = 'payment_app'

urlpatterns = [
    # Stripe
    path('payment/cancel/', CancelView.as_view(), name='cancel'),
    path('payment/success/', SuccessView.as_view(), name='success'),

    path('webhooks/stripe/', StripeWebhookAPIView.as_view(), name='stripe-webhook'),
    path('webhooks/connect/', StripeConnectedAccWebhookAPIView.as_view(), name='stripe-connected-acc-webhook'),

    path('payment/checkout/', CreateCheckoutSessionApiView.as_view(),
         name='stripe-checkout'),
    path('payment/links/', PaymentLinkView.as_view(), name='payment-links'),
    path('transfers/', TransferView.as_view(), name='transfers'),
    path('payouts/', PayoutView.as_view(), name='payouts'),
    # path('payment/refunds/', CreateRefundApiView.as_view(), name='stripe-refunds'),    
]
