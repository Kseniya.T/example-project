from datetime import timedelta
from sentry_sdk import capture_message, set_context
import stripe
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from apps.discussion_app.models import Discussion, ParticipantsOfDiscussion
from apps.notification_app.models import Notification
from django.utils import timezone

from apps.payment_app.models import ChargeData, Order
from apps.user_app.models import CustomUser
from core.celery import app
from core.settings import STRIPE_SECRET_KEY
from repositories.notification_repository import NotificationRepository
from repositories.init import push_notification_repository


@app.task
def remove_empty_orders():
    """Function check orders without checkout session and participants for discussion in order and remove it"""

    orders_for_delete = Order.objects.select_related('user', 'discussion').filter(
        Q(is_confirmed=False) & Q(checkout_session_id=None))
    if orders_for_delete:
        for order in orders_for_delete:
            user: CustomUser = order.user
            discussion: Discussion = order.discussion
            participant = ParticipantsOfDiscussion.objects.filter(
                user=user, discussion=discussion)
            if not participant:
                order.delete()
        return 'Empty orders successfully deleted'
    elif not orders_for_delete:
        return 'No empty orders were found'


@app.task
def check_charge_data():
    """Function for ChargeData model for empty user and discussion columns and fill them"""

    charges_for_update = ChargeData.objects.filter(
        Q(user=None) & Q(discussion=None))

    if not charges_for_update:
        return 'No charges for update'

    for charge in charges_for_update:
        payment_intent_id: str = charge.payment_intent_id
        try:
            order: Order = Order.objects.select_related(
                'user', 'discussion').get(payment_intent_id=payment_intent_id)
            charge.update_fields(user=order.user, discussion=order.discussion)
            print(f'Charge {charge} was updated successfully')
        except ObjectDoesNotExist:
            capture_message(
                f'Was found charge:{charge}, with payment intent id: {charge.payment_intent_id} without order', level='error')
            print(
                f'Was found charge:{charge}, with payment intent id: {charge.payment_intent_id} without order')
    return True


@app.task
def check_unconfirmed_orders():
    """Function wich check If there is an entry in the Order table with status is_confirmed=False and payment_status!='unpaid'. 
       If there are such orders request Stripe for retrieve checkout session data.
       Enter the missing data into the Order table."""

    unconfirmed_orders = (Order.objects.exclude(Q(checkout_session_id=None))
                          .filter(Q(is_confirmed=False) & Q(payment_status=None))
                          )

    if not unconfirmed_orders:
        return 'No unconfirmed orders were found'

    for order in unconfirmed_orders:
        try:
            stripe_checkout_session = stripe.checkout.Session.retrieve(
                api_key=STRIPE_SECRET_KEY, id=order.checkout_session_id,  expand=['customer_details'])
        except Exception as e:
            set_context('retrieve_checkout_session_case', value=e.__dict__)
            capture_message(
                'Retrieve checkout session error', level='error')
            raise e

        if stripe_checkout_session.payment_status == 'paid':
            user_email: str = stripe_checkout_session.customer_details.email

            try:
                charge: ChargeData = ChargeData.objects.get(
                    payment_intent_id=stripe_checkout_session.payment_intent)
            except ObjectDoesNotExist as e:
                set_context('chargedata_is_not_found_case',
                            value=e.__dict__)
                capture_message(
                    f'Charge data object wasn\'t created for order {order.pk}', level='error')
                return False

            order.update_fields(
                user_email=user_email,
                charge_id=charge.pk,
                payment_intent_id=stripe_checkout_session.payment_intent,
                payment_status=stripe_checkout_session.payment_status,
                is_confirmed=True,
            )
            # Create new notification that payment was successfull
            # title: str = "Payment was succesfull"
            # body: str = f'Payment for discussion "{order.discussion}" was successfull.'
            # payload = {'stripe_checkout_session_obj': stripe_checkout_session,
            #            'order_id': f'{order.pk}'}
            # notification: Notification = NotificationRepository.create_notification(
            #     related_user=order.user, title=title, body=body, payload=payload)
            # push_notification_repository.add_notification_data_to_rdb(
            #     user_pk=order.user.pk,
            #     notification=notification
            # )

        if stripe_checkout_session.payment_status == 'unpaid':
            filter_time = timezone.now() - timedelta(minutes=29)
            if order.created_at < filter_time:
                user = order.user
                order.update_fields(
                    payment_status=stripe_checkout_session.payment_status,
                )
                try:
                    unconfirmed_participant: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.get(
                        Q(user=user) & Q(discussion=order.discussion) & Q(number_of_paid_seats=0))
                except ObjectDoesNotExist as e:
                    set_context('participant_not_found_case', value=e.__dict__)
                    capture_message(
                        f'ParticipantOfDiscussion object wasn\'t found for order {order.pk}', level='error')
                    return False

                if not unconfirmed_participant.is_initiator:
                    unconfirmed_participant.delete()
            #  Create new notification that payment was not successfull
            # title: str = "Payment was canceled"
            # body: str = f'Payment for discussion "{order.discussion}" failed.'
            # payload = {'stripe_checkout_session_obj': stripe_checkout_session,
            #            'order_id': f'{order.pk}'}
            # notification: Notification = NotificationRepository.create_notification(
            #     related_user=order.user, title=title, body=body, payload=payload)
            # push_notification_repository.add_notification_data_to_rdb(
            #     user_pk=order.user.pk,
            #     notification=notification
            # )
    print('Unconfirmed orders were successfully updated and confirmed')
    return True
