# Generated by Django 4.1.3 on 2023-01-12 11:51

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('discussion_app', '0026_participantsofdiscussion_is_initiator'),
        ('payment_app', '0004_checkoutsessionwithpaymentintent_is_active_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckoutSessionData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_email', models.EmailField(max_length=254, verbose_name='User email')),
                ('checkout_session_id', models.CharField(max_length=150, verbose_name='Checkout session id')),
                ('payment_intent_id', models.CharField(max_length=150, verbose_name='Payment intent id')),
                ('quantity', models.SmallIntegerField(verbose_name='Quantity')),
                ('payment_intent_amount', models.IntegerField(verbose_name='Total amount')),
                ('payment_status', models.CharField(max_length=30, verbose_name='Payment status')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Updated at')),
                ('discussion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='checkout_session', to='discussion_app.discussion', verbose_name='Discussion')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='checkout_session', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Checkout Session',
                'verbose_name_plural': 'Checkout Sessions',
            },
        ),
        migrations.DeleteModel(
            name='CheckoutSessionWithPaymentIntent',
        ),
    ]
