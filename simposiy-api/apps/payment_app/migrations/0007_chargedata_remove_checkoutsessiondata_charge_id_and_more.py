# Generated by Django 4.1.3 on 2023-01-13 10:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payment_app', '0006_checkoutsessiondata_charge_id_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checkoutsessiondata',
            name='charge_id',
        ),
        migrations.RemoveField(
            model_name='checkoutsessiondata',
            name='charge_status',
        ),
        migrations.RemoveField(
            model_name='checkoutsessiondata',
            name='payment_intent_amount',
        ),
        migrations.RemoveField(
            model_name='checkoutsessiondata',
            name='payment_intent_id',
        ),
        migrations.CreateModel(
            name='ChargeData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('charge_id', models.CharField(max_length=150, verbose_name='Charge id')),
                ('charge_status', models.CharField(max_length=30, verbose_name='Charge status')),
                ('payment_intent_id', models.CharField(max_length=150, verbose_name='Payment intent id')),
                ('amount_captured', models.IntegerField(verbose_name='Amount captured')),
            ],
            options={
                'verbose_name': 'Charge',
                'verbose_name_plural': 'Charges',
            },
        ),
        migrations.AddField(
            model_name='checkoutsessiondata',
            name='charge',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='checkout_session', to='payment_app.chargedata', verbose_name='Charge'),
            preserve_default=False,
        ),
    ]
