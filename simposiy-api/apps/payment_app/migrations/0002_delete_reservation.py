# Generated by Django 4.1.3 on 2022-11-30 13:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payment_app', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Reservation',
        ),
    ]
