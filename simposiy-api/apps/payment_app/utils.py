from io import BytesIO
from xhtml2pdf import pisa


def generate_pdf(html_content, pdf_file):

    # Generate the PDF
    pisa.CreatePDF(html_content, dest=pdf_file)
