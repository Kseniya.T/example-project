import logging as logger
import pickle
from collections.abc import Callable

import django

from apps.discussion_app.enums import NotificationEventEnum
django.setup()

import pika
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic, BasicProperties
from sentry_sdk import capture_exception

from apps.rabbitmq_app.enums import (DiscussionNotifydDTO, MinParticipantsRichedDTO, QueuesEnum, StripeChargeEventDTO, StripeCheckoutSessionEventDTO,
                                     StripePayoutEventDTO, StripeRefundEventDTO, StripeUpdateAccountEventDTO)
from apps.rabbitmq_app.repository import (create_charge_data_obj, create_payout_data,
                                          create_refund_obj, host_accept_response_notify, host_deny_response_notify, host_request_notify, max_participants_riched_notify, min_participants_riched_notify, pending_host_timed_out_notify, update_corporate_profile,
                                          update_order)

from core.settings import (RABBITMQ_CONSUMER_PASSWORD,
                           RABBITMQ_CONSUMER_USER, RABBITMQ_HOST,
                           RABBITMQ_PORT, VIRTUAL_HOST)
from apps.notification_app.tasks import send_push_notifications
# logger.basicConfig(level=logger.INFO)


class ConsumerRabbitMQ:

    def subscribe_to_read_queue(
        self, channel,
        queue: str, callback: Callable
    ) -> None:
        """
        Method for subscribe to read queue

        :param channel: Current Channel
        :param queue: str
        :param callback: Callable
        :return: None
        """
        channel.basic_consume(
            queue=queue,
            on_message_callback=callback,
            auto_ack=True
        )

    def main(self):
        """Method for create connect with rabbitmq"""
        parameters = pika.ConnectionParameters(
            host=RABBITMQ_HOST,
            virtual_host=VIRTUAL_HOST,
            port=RABBITMQ_PORT,
            credentials=pika.PlainCredentials(
                username=RABBITMQ_CONSUMER_USER,
                password=RABBITMQ_CONSUMER_PASSWORD
            )
        )
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # TODO put my events here
        self.subscribe_to_read_queue(
            channel=channel, queue=str(QueuesEnum.INBOUND),
            callback=__class__.inbound_handler
        )
        self.subscribe_to_read_queue(
            channel=channel, queue=str(QueuesEnum.PUSH_NOTIFICATION_INBOUND),
            callback=__class__.notification_inbound_handler
        )

        logger.info('Start Consumer')
        channel.start_consuming()

    @staticmethod
    def inbound_handler(
        ch: BlockingChannel,
        method: Basic,
        properties: BasicProperties,
        body: bytes, *args
    ):
        """
        Method for handle message from inbound
        """
        try:
            data = pickle.loads(body)
            event_type = data.get('event_type')

            if event_type == 'charge.succeeded':
                dto = StripeChargeEventDTO(**data)
                create_charge_data_obj(dto=dto)
            elif event_type == 'checkout.session.completed':
                dto = StripeCheckoutSessionEventDTO(**data)
                update_order(dto=dto)
            elif event_type == 'account.updated':
                dto = StripeUpdateAccountEventDTO(**data)
                update_corporate_profile(dto=dto)
            elif event_type == 'charge.refunded':
                dto = StripeRefundEventDTO(**data)
                create_refund_obj(dto=dto)
            elif event_type == 'payout.paid':
                dto = StripePayoutEventDTO(**data)
                create_payout_data(dto=dto)
            elif event_type == 'payout.pending' or event_type == 'payout.in_transit' or event_type == 'payout.failed' or event_type == 'payout.canceled':
                dto = StripePayoutEventDTO(**data)
                create_payout_data(dto=dto)

            send_push_notifications()
            
        except Exception as error:
            logger.error(error)
            capture_exception(error)

    @staticmethod
    def notification_inbound_handler(
        ch: BlockingChannel,
        method: Basic,
        properties: BasicProperties,
        body: bytes, *args
    ):
        """
        Method for handle message from PUSH_NOTIFICATION_INBOUND
        """
        try:
            data = pickle.loads(body)
            event_type = data.get('event')
            
            if event_type == NotificationEventEnum.MAX_PARTICIPANTS_RICHED:
                # event comes from discussion_app.tasks.py check_discussion_is_full task
                dto = DiscussionNotifydDTO(**data)
                max_participants_riched_notify(dto=dto)
            
            elif event_type == NotificationEventEnum.MIN_PARTICIPANTS_RICHED:
                # event comes from discussion_app.tasks.py check_minimum_number_participants task
                dto = MinParticipantsRichedDTO(**data)
                min_participants_riched_notify(dto=dto)
            
            elif event_type == NotificationEventEnum.HOST_REQUEST:
                # event comes from discussion_app.views.py CreateDiscussionApiView/UpdateDiscussionApiView
                dto = DiscussionNotifydDTO(**data)
                host_request_notify(dto=dto)
            
            elif event_type == NotificationEventEnum.PENDING_HOST_TIMED_OUT:
                # event comes from discussion_app.tasks.py check_response_from_place_owner task
                dto = DiscussionNotifydDTO(**data)
                pending_host_timed_out_notify(dto=dto)
            
            elif event_type == NotificationEventEnum.HOST_DENY_RESPONSE:
                # event comes from notification_app.views.py RetrieveNotificationPostResponseApiView
                dto = DiscussionNotifydDTO(**data)
                host_deny_response_notify(dto=dto)
            
            elif event_type == NotificationEventEnum.HOST_ACCEPT_RESPONSE:
                # event comes from notification_app.views.py RetrieveNotificationPostResponseApiView
                dto = DiscussionNotifydDTO(**data)
                host_accept_response_notify(dto=dto)
            
            send_push_notifications()

        except Exception as error:
            logger.error(error)
            capture_exception(error)


if __name__ == '__main__':
    ConsumerRabbitMQ().main()
