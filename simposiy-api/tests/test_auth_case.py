from dataclasses import dataclass
import requests


@dataclass(frozen=True)
class UserAuthenticationDTO:
    """DTO for user authentication case"""
    phone_number: str
    device_agent_id: str
    device_agent_represent: str
    push_token: str = None
    code: str = None

def main():
    auth_case(1)


def auth_case(iterations_number: int):
    print('Started authentication case')
    SIGN_IN_URL = 'http://127.0.0.1:8000/api/v1/auth/send-sms/'
    SIGN_IN_CONFIRM_URL = 'http://127.0.0.1:8000/api/v1/auth/sign-in/'
    PHONE_NUMBER = '+79275644789'


    for i in range(iterations_number):
        DEVICE_AGENT_ID = f'Test Agent ID {i}'
        DEVICE_AGENT_REPRESENT = f'Test Agent Represent {i}'

        dto = UserAuthenticationDTO(
            phone_number=PHONE_NUMBER,
            device_agent_id=DEVICE_AGENT_ID,
            device_agent_represent=DEVICE_AGENT_REPRESENT,
        )

        sign_in_response = requests.post(SIGN_IN_URL, data=dto.__dict__)
        sign_in_response.raise_for_status()

        otp_code = sign_in_response.json().get('detail').split(' ')[-1]

        dto = UserAuthenticationDTO(
            phone_number=PHONE_NUMBER,
            device_agent_id=DEVICE_AGENT_ID,
            device_agent_represent=DEVICE_AGENT_REPRESENT,
            code=otp_code,
        )
    
        sign_in_confirm_response = requests.post(SIGN_IN_CONFIRM_URL, data=dto.__dict__)

        print(f"Case {i + 1}. Success: {sign_in_confirm_response.json().get('isSuccess')}")



if __name__ == '__main__':
    main()