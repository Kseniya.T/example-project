from django.test import TestCase
from moneyed.classes import Money

from apps.user_app.models import CustomUser
from apps.discussion_app.models import Discussion
from apps.payment_app.models import ChargeData, Order, Refund
from tests import fixtures


class PaymentAppModelTest(TestCase):

    @classmethod
    def setUp(cls):
        phone = '+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(
            phone_number=phone, make_auth_user_corporate_profile=False)

    def test_charde_data_model(self):
        """Test case for ChargeData model"""
        
        user: CustomUser = CustomUser.objects.last()
        discussion: Discussion = Discussion.objects.select_related('place').last()
        charge_status = 'succeeded'
        charge_id = 'ch_3Mjfe8FgAcWK6CZn1dBthU0S'
        payment_intent_id = 'pi_3Mjfe8FgAcWK6CZn1pwvG1of'
        amount_captured = discussion.place.deposit.amount
                
        charge_data: ChargeData = ChargeData.objects.create(user=user,
                                                            discussion=discussion,
                                                            charge_status=charge_status,
                                                            charge_id=charge_id,
                                                            payment_intent_id=payment_intent_id,
                                                            amount_captured=amount_captured,
                                                            )
        self.assertEquals(charge_data.user, user)
        self.assertEquals(charge_data.discussion, discussion)
        self.assertEquals(charge_data.charge_status, charge_status)
        self.assertEquals(charge_data.charge_id, charge_id)
        self.assertEquals(charge_data.payment_intent_id, payment_intent_id)
        self.assertTrue(isinstance(charge_data, ChargeData))

    def test_order_model(self):
        """Test case for order model"""
        
        user: CustomUser = CustomUser.objects.last()
        user_email = 'testemail@gmail.com'
        discussion: Discussion = Discussion.objects.last()
        charge: ChargeData = ChargeData.objects.first()
        payment_intent_id = charge.payment_intent_id
        checkout_session_id = 'cs_test_a1FSfEop7WNNwo7CVZHUXKSyNdSbiW79i25UhYfHPypbocfuGnS2n9I0NL'
        quantity = 1
        payment_status = 'paid'
        
        order: Order = Order.objects.create(user=user,
                                            user_email=user_email,
                                            discussion=discussion,
                                            charge=charge,
                                            payment_intent_id=payment_intent_id,
                                            checkout_session_id=checkout_session_id,
                                            quantity=quantity,
                                            payment_status=payment_status,
                                            )
        self.assertEquals(order.user, user)
        self.assertEquals(order.user_email, user_email)
        self.assertEquals(order.discussion, discussion)
        self.assertEquals(order.charge, charge)
        self.assertEquals(order.payment_intent_id, payment_intent_id)
        self.assertEquals(order.checkout_session_id, checkout_session_id)
        self.assertEquals(order.quantity, quantity)
        self.assertEquals(order.payment_status, payment_status)
        self.assertEquals(order.is_confirmed, False)
        self.assertEquals(order.refunded, False)
        self.assertTrue(isinstance(order, Order))
    
    def test_refund_model(self):
        """Test case for Refuns model"""
        
        order: Order = Order.objects.select_related('charge').last()
        amount_refunded = order.charge.amount_captured.amount
        status = 'succeeded'
        
        refund: Refund = Refund.objects.create(order=order, amount_refunded=amount_refunded, status=status)
                
        self.assertEquals(refund.order, order)
        self.assertEquals(refund.status, status)
        self.assertTrue(isinstance(refund, Refund))
        