from rest_framework.test import APITestCase
from rest_framework import status
from apps.discussion_app.models import Discussion

from tests import fixtures



class PaymentTestCases(APITestCase):
    """Test cases for payment_app endpoints"""

    @classmethod
    def setUp(cls):
        phone='+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(phone_number=phone, make_auth_user_corporate_profile=False)
    
    def test_create_checkout_session(self):
        """Test case for /api/v1/payment/checkout/ endpoint"""
        
        discussion = Discussion.objects.first()
        path = '/api/v1/payment/checkout/'
        data = {"discussion": discussion.pk}
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(
            path=path, data=data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
