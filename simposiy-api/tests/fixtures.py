from datetime import timedelta
from django.utils import timezone
from rest_framework.test import APIClient
from apps.discussion_app.models import Discussion, DiscussionCategory, DiscussionTheme, DiscussionType, UserThemes
from apps.notification_app.models import ApplicationConfig
from apps.payment_app.models import ChargeData, Order
from apps.place_app.models import Dishes, Place, PlaceOpeningHours, PlaceType

from apps.user_app.models import CustomUser, Interests, Profile, UserInterests


def create_authenticated_client(phone_number: str) -> str:
    client = APIClient()

    phone_number = phone_number
    device_agent_id = '69aaf87588b4d593'
    device_agent_represent = 'SM-A505FN'

    data = {
        'phone_number': phone_number,
        'device_agent_id': device_agent_id,
        'device_agent_represent': device_agent_represent
    }

    response = client.post(path='/api/v1/auth/send-sms/', data=data)
    otp_code = response.data.get('detail')[-6:]

    sign_in_data = {
        'phone_number': phone_number,
        'device_agent_id': device_agent_id,
        'device_agent_represent': device_agent_represent,
        'code': otp_code
    }

    response = client.post(path='/api/v1/auth/sign-in/', data=sign_in_data)

    token = response.data.get('object').get('token')
    return token


def prefill_test_db(phone_number: str, make_auth_user_corporate_profile: bool):
    
    app_support_email = 'support@mail.com'
    app_support_email_password = 'somestrongpassword'
    app_phone_number = '+18004568923'
    app_service_fee = 10
    
    app_config = ApplicationConfig.objects.create(app_support_email=app_support_email,
                                    app_support_email_password=app_support_email_password,
                                    app_phone_number=app_phone_number,
                                    app_service_fee=app_service_fee,
                                    )
    
    discussion_types = DiscussionType.objects.bulk_create([DiscussionType(name='Group meeting'),
                                                           DiscussionType(name='One-on-one')])

    discussion_categories = DiscussionCategory.objects.bulk_create([DiscussionCategory(name='Game club'),
                                                                    DiscussionCategory(
        name='Support group'),
        DiscussionCategory(
        name='Idea test'),
        DiscussionCategory(
        name='Formal meeting'),
        DiscussionCategory(
        name='Business meeting'),
    ])

    discussion_themes = DiscussionTheme.objects.bulk_create([DiscussionTheme(name='Art'),
                                                             DiscussionTheme(
                                                                 name='Music'),
                                                             DiscussionTheme(
                                                                 name='Education'),
                                                             ])

    user_1 = CustomUser.objects.create(phone_number='+905538652318')
    user_2 = CustomUser.objects.create(phone_number='+905538651118')
    auth_user = CustomUser.objects.get(phone_number=phone_number)

    place_type = PlaceType.objects.create(name='Cafe')

    if make_auth_user_corporate_profile:
        Profile.objects.bulk_create([Profile(user=auth_user, profile_type="Corporate", company_name="Smart Talk", company_legal_name="Smart-Smart Inc",
                                             company_email="smartsmart@corp.com", company_address="802 S.W. 8th St. Bentonville, AK 56716",
                                             company_ein="59-456987", company_phone_number='+18005638978', company_business_type='company', company_business_website='http://smarttalk.com'),
                                    Profile(
                                        user=user_1, profile_type="Personal", user_name='Kate Blanshet'),
                                    Profile(user=user_2, profile_type="Corporate", company_name="Dumb Talk", company_legal_name="Silly-Silly Inc",
                                            company_email="sillysilly@corp.com", company_address="801 S.W. 10th St. Forestville, AK 50706",
                                            company_ein="70-12345", company_phone_number='+18005637978', company_business_type='company', company_business_website='http://dambtalk.com'),
                                     ])
        places = Place.objects.bulk_create([Place(owner=auth_user, name="Dont speak", address="156 DS, New York, NY 12405",
                                                  menu="https://www.menu.com", description="Don't speak while eating", deposit=30,
                                                  deposit_currency="USD", discount=15, minimum_available_seats=2, maximum_available_seats=5,
                                                  email="dontspeak_eat@corp.com", phone_number="+905534578143"),
                                           Place(owner=user_2, name="Speak", address="100 DS, New York, NY 10205",
                                                 menu="https://www.menu.com", description="Speak while eating", deposit=40,
                                                 deposit_currency="USD", discount=15, minimum_available_seats=2, maximum_available_seats=5,
                                                 email="speak_eat@corp.com", phone_number="+905538651118"),
                                           Place(owner=user_2, name="Italian cousin", address="101 DS, New York, NY 11205",
                                                 menu="https://www.menu.com", description="Cafe with italian kitchen", deposit=50,
                                                 deposit_currency="USD", discount=20, minimum_available_seats=2, maximum_available_seats=5,
                                                 email="itcousin@corp.com", phone_number="+905538651108")])

        places[0].type.add(place_type.id)
        places[1].type.add(place_type.id)
        places[2].type.add(place_type.id)
        
        dishes = Dishes.objects.bulk_create([Dishes(place=places[0], name='fish', image='fish.jpg'), Dishes(
            place=places[0], name='pastry', image='pastry.jpg')])

    if not make_auth_user_corporate_profile:
        Profile.objects.bulk_create([Profile(user=user_1, profile_type="Corporate", company_name="Smart Talk", company_legal_name="Smart-Smart Inc",
                                             company_email="smartsmart@corp.com", company_address="802 S.W. 8th St. Bentonville, AK 56716",
                                             company_ein="59-456987", company_phone_number='+18005637978', company_business_type='company', company_business_website='http://smarttalk.com'),
                                    Profile(
                                        user=auth_user, profile_type="Personal", user_name='Kate Blanshet')
                                     ])

        places = Place.objects.bulk_create([Place(owner=user_1, name="Dont speak", address="156 DS, New York, NY 12405",
                                                  menu="https://www.menu.com", description="Don't speak while eating", deposit=30,
                                                  deposit_currency="USD", discount=15, minimum_available_seats=2, maximum_available_seats=5,
                                                  email="dontspeak_eat@corp.com", phone_number="+905534578143"),
                                            Place(owner=user_2, name="Speak", address="100 DS, New York, NY 10205",
                                                  menu="https://www.menu.com", description="Speak while eating", deposit=40,
                                                  deposit_currency="USD", discount=15, minimum_available_seats=2, maximum_available_seats=5,
                                                  email="speak_eat@corp.com", phone_number="+905538651118")])

        places[0].type.add(place_type.id)
        places[1].type.add(place_type.id)
        
        PlaceOpeningHours.objects.bulk_create([PlaceOpeningHours(place=places[0], weekday=1, from_hour="9:00", to_hour="23:00", is_dayoff=False),
                                               PlaceOpeningHours(
            place=places[0], weekday=2, from_hour="00:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=places[0], weekday=3, from_hour="00:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=places[0], weekday=4, from_hour="00:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=places[0], weekday=5, from_hour="00:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=places[0], weekday=6, from_hour="00:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=places[0], weekday=7, from_hour="00:00", to_hour="00:00", is_dayoff=False),
        ])

    date = timezone.now() + timedelta(days=7)
    discussions = Discussion.objects.bulk_create([Discussion(initiator=user_1,
                                                             place=places[1],
                                                             type=discussion_types[0],
                                                             category=discussion_categories[3],
                                                             theme=discussion_themes[0],
                                                             topic='Modern art',
                                                             description='Modern art in modern life',
                                                             dt_start=date,
                                                             number_of_participants=5,
                                                             is_active=True,
                                                             service_fee=app_config.app_service_fee,
                                                             ),
                                                  Discussion(initiator=user_1,
                                                             place=places[1],
                                                             type=discussion_types[0],
                                                             category=discussion_categories[3],
                                                             theme=discussion_themes[2],
                                                             topic='Modern music',
                                                             description='Modern music in modern life',
                                                             dt_start=date,
                                                             number_of_participants=7,
                                                             is_active=True,
                                                             service_fee=app_config.app_service_fee,
                                                             ),
                                                  ])

    discussion: Discussion = Discussion.objects.select_related('place').last()
    charge_status = 'succeeded'
    amount_captured = discussion.place.deposit.amount

    charges = ChargeData.objects.bulk_create([ChargeData(user=auth_user,
                                                                    discussion=discussion,
                                                                    charge_status=charge_status,
                                                                    charge_id='cs_test_a1A7R2WSblN1qdoAZMStz9rR2gXKje9wLje1dmClNniYDCVAakWH1MIDrG',
                                                                    payment_intent_id='pi_3NBFkRFgAcWK6CZn0RXQ6UzH',
                                                                    amount_captured=amount_captured,
                                                                    ),
                                                         ChargeData(user=auth_user,
                                                                    discussion=discussion,
                                                                    charge_status=charge_status,
                                                                    charge_id='cs_test_a1rrOw0QHlsbaUJTfGhvRXN12ZsKKDbeKqS7rR6ZBWLlP57SXpE5NI90FT',
                                                                    payment_intent_id='pi_3NBFeMFgAcWK6CZn18tIcy42',
                                                                    amount_captured=amount_captured,
                                                                    )])

    user_email = 'test@gmail.com'
    checkout_session_id = 'cs_test_a1A7R2WSblN1qdoAZMStz9rR2gXKje9wLje1dmClNniYDCVAakWH1MIDrG'
    quantity = 1
    payment_status = 'paid'

    Order.objects.create(user=auth_user,
                         user_email=user_email,
                         discussion=discussions[0],
                         charge=charges[1],
                         payment_intent_id=charges[1].payment_intent_id,
                         checkout_session_id=checkout_session_id,
                         quantity=quantity,
                         payment_status=payment_status,
                         )
    
    iterests_list: list = [Interests(name=name) for name in ['art', 'animals', 'cinema', 'sport']]
    interests = Interests.objects.bulk_create(iterests_list)
    
    auth_user_interests = UserInterests.objects.bulk_create([UserInterests(user=auth_user, interests=interests[0]),
                                                             UserInterests(user=auth_user, interests=interests[1])])

    # TODO добавить тематики пользователей
    user_themes = UserThemes.objects.bulk_create([UserThemes(user=auth_user, themes=discussion_themes[0]),
                                                  UserThemes(user=auth_user, themes=discussion_themes[1]),
                                                  UserThemes(user=auth_user, themes=discussion_themes[2])])