from django.test import TestCase

from apps.user_app.models import CustomUser, Profile, Interests, UserInterests
from tests import fixtures


class UserAppModelTest(TestCase):

    @classmethod
    def setUp(cls):
        phone = '+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(
            phone_number=phone, make_auth_user_corporate_profile=False)

    def test_custom_user_model(self):
        """Test case for CustomUser model"""

        phone_number = '+905534568975'
        user: CustomUser = CustomUser.objects.create(phone_number=phone_number)

        self.assertEquals(user.phone_number, phone_number)
        self.assertTrue(isinstance(user, CustomUser))

    def test_personal_profile_model(self):
        """Test case for Profile model"""

        phone_number = '+905537894572'
        user: CustomUser = CustomUser.objects.create(phone_number=phone_number)
        profile_type = 'Personal'
        user_name = 'Tom Hanks'
        user_email = 'th@gmail.com'
        user_info = 'Thomas Jeffrey Hanks is an American actor and filmmaker.'
        user_image = 'tom_hanks.png'

        personal_profile: Profile = Profile.objects.create(user=user,
                                                           profile_type=profile_type,
                                                           user_name=user_name,
                                                           user_email=user_email,
                                                           user_info=user_info,
                                                           user_image=user_image,
                                                           )
        
        self.assertEquals(personal_profile.user, user)
        self.assertEquals(personal_profile.profile_type, profile_type)
        self.assertEquals(personal_profile.user_name, user_name)
        self.assertEquals(personal_profile.user_email, user_email)
        self.assertEquals(personal_profile.user_info, user_info)
        self.assertEquals(personal_profile.user_image, user_image)
        self.assertTrue(isinstance(personal_profile, Profile))

    def test_corporate_profile_model(self):
        """Test case for Profile model"""
        
        phone_number = '+905537894512'
        user: CustomUser = CustomUser.objects.create(phone_number=phone_number)
        profile_type = 'Corporate'
        company_name = 'Hollywood Pictures'
        company_legal_name = 'Hollywood Pictures Home Entertainment'
        company_address = '500 South Buena Vista Street, Burbank, California, U.S.'
        company_email = 'https://www.hollywood.com/'
        company_ein = '10-123852693'
        company_info = 'Hollywood Pictures was an American film production label of Walt Disney Studios, founded and owned by The Walt Disney Company.'
        company_image = 'company_pic.png'
        company_phone_number='+18005637908'
        company_business_type='company'
        company_business_website='http://hollywoodpictures.com'

        corporate_profile: Profile = Profile.objects.create(user=user,
                                                            profile_type=profile_type,
                                                            company_name=company_name,
                                                            company_legal_name=company_legal_name,
                                                            company_address=company_address,
                                                            company_email=company_email,
                                                            company_ein=company_ein,
                                                            company_info=company_info,
                                                            company_image=company_image,
                                                            company_phone_number=company_phone_number,
                                                            company_business_type=company_business_type,
                                                            company_business_website=company_business_website,
                                                            )
        
        self.assertEquals(corporate_profile.user, user)
        self.assertEquals(corporate_profile.profile_type, profile_type)
        self.assertEquals(corporate_profile.company_name, company_name)
        self.assertEquals(corporate_profile.company_legal_name, company_legal_name)
        self.assertEquals(corporate_profile.company_address, company_address)
        self.assertEquals(corporate_profile.company_email, company_email)
        self.assertEquals(corporate_profile.company_ein, company_ein)
        self.assertEquals(corporate_profile.company_info, company_info)
        self.assertEquals(corporate_profile.company_image, company_image)
        self.assertEquals(corporate_profile.company_phone_number, company_phone_number)
        self.assertEquals(corporate_profile.company_business_type, company_business_type)
        self.assertEquals(corporate_profile.company_business_website, company_business_website)
        self.assertTrue(isinstance(corporate_profile, Profile))

    def test_interests(self):
        """Test case for Interests model"""
        
        iterests_list: list = [Interests(
            name=name) for name in ['art', 'animals', 'cinema', 'sport']]
        interests = Interests.objects.bulk_create(iterests_list)
        
        self.assertEquals(interests[0].name, 'art')
        self.assertEquals(interests[1].name, 'animals')
        self.assertEquals(interests[2].name, 'cinema')
        self.assertEquals(interests[3].name, 'sport')
        self.assertTrue(isinstance(interests[0], Interests))
    
    def test_user_interests(self):
        """Test case for UserInterests model"""
        
        user: CustomUser = CustomUser.objects.last()
        interest: Interests = Interests.objects.last()
        user_interests: UserInterests = UserInterests.objects.create(user=user, interests=interest)
        
        self.assertEquals(user_interests.user, user)
        self.assertEquals(user_interests.interests, interest)
        self.assertTrue(isinstance(user_interests, UserInterests))
