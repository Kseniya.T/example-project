from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from apps.discussion_app.models import Discussion, UserThemes
from apps.user_app.models import CustomUser, Interests, Profile, UserInterests

from tests import fixtures


class UserTestCases(APITestCase):
    """Test cases for user_app endpoints"""

    @classmethod
    def setUp(cls):
        phone = '+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(
            phone_number=phone, make_auth_user_corporate_profile=False)

    def test_create_user_interests(self):
        """Test case for /api/v1/user/create-interests/ endpoint"""

        path = '/api/v1/user/create-interests/'

        data = {
            "interests": [interest.pk for interest in Interests.objects.all()]
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_list_all_interests(self):
        """Test case for /api/v1/interests/ endpoint"""

        path = '/api/v1/interests/'
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_list_auth_user_interests(self):
        """Test case for /api/v1/user/list-interests/ endpoint"""

        path = '/api/v1/user/list-interests/'
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    # def test_delete_auth_user_interests(self):
    #     """Test case for /api/v1/user/delete-interests/ endpoint"""

    #     path = '/api/v1/user/delete-interests/'
    #     auth_user_interest = UserInterests.objects.last()
    #     data = {"id": auth_user_interest.pk}

    #     self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
    #     # response = self.client.delete(path=path, data=data, format="json")
    #     response = self.client.delete(path=reverse(
    #         'user_app:user-delete-interests'), data=data, format="json")
    #     self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_create_personal_profile(self):
        """Test case for /api/v1/user/profile/create/ endpoint"""

        path = '/api/v1/user/profile/create/'
        user: CustomUser = CustomUser.objects.get(phone_number='+375298001010')
        old_profile: Profile = Profile.objects.get(user=user)
        old_profile.delete()
        data = {
            "profile_type": "Personal",
            "user_name": "John Snow"
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_corporate_profile(self):
        """Test case for /api/v1/user/profile/create/ endpoint"""

        path = '/api/v1/user/profile/create/'
        user: CustomUser = CustomUser.objects.get(phone_number='+375298001010')
        old_profile: Profile = Profile.objects.get(user=user)
        old_profile.delete()
        data = {
            "profile_type": "Corporate",
            "company_name": "Mars",
            "company_legal_name": "Mars Inc",
            "company_email": "mars@corp.com",
            "company_address": "802 S.W. 8th St. Marsville, AK 45689",
            "company_ein": "09-1259878",
            "company_phone_number": "+18005623213",
            "company_business_type": "company",
            "company_business_website": "http://mars.com"
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_get_auth_user_profile(self):
        """Test case for /api/v1/user/profile/ endpoint"""

        path = '/api/v1/user/profile/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_other_user_profile(self):
        """Test case for /api/v1/user/profile/<int:profile_id> endpoint"""

        profile: Profile = Profile.objects.first()
        path = f'/api/v1/user/profile/{profile.pk}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_delete_auth_user_profile(self):
        """Test case for /api/v1/user/delete/ endpoint"""

        path = '/api/v1/user/delete/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.delete(path=path)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_patch_user_profile(self):
        """Test case for /api/v1/user/profile/update/ endpoint"""

        path = '/api/v1/user/profile/update/'
        data = {
            "user_name": "John Snow",
            "user_email": "johnsnow@mail.com"
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.patch(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
    
    def test_create_user_themes(self):
        """Test case for /api/v1/user/add-themes/ endpoint"""
        
        path = '/api/v1/user/add-themes/'

        data = {
            "themes": [theme.pk for theme in UserThemes.objects.all()]
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
    
    def test_list_user_themes(self):
        """Test case for /api/v1/user/list-themes/ endpoint"""
        
        path = '/api/v1/user/list-themes/'
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    # def test_delete_user_theme(self):
    #     """Test case for /api/v1/user/delete-themes/ endpoint"""
        
    #     path = '/api/v1/user/delete-themes/'
        
    #     user_theme = UserThemes.objects.last()
        
    #     data = {"id": user_theme.pk}
    #     self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
    #     response = self.client.delete(path=path, data=data, format="json")
    #     print(response.data)
    #     self.assertEqual(status.HTTP_200_OK, response.status_code)