from rest_framework import status
from rest_framework.test import APITestCase
from apps.place_app.models import Dishes, Place, PlaceOpeningHours, PlaceType
from apps.user_app.models import CustomUser, Profile
from .images_for_tests import *

from tests import fixtures


class PlaceTestCases(APITestCase):
    """Test cases for place_app endpoints"""

    @classmethod
    def setUp(cls):
        phone = '+375298001010'
        cls.token = fixtures.create_authenticated_client(phone_number=phone)
        fixtures.prefill_test_db(
            phone_number=phone, make_auth_user_corporate_profile=True)

    def test_place_types_list(self):
        """Test case for /api/v1/place/type-list/ endpoint"""

        path = '/api/v1/place/type-list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        place_types_number = PlaceType.objects.all().count()

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), place_types_number)

    def test_place_type_details(self):
        """Test case for /api/v1/place/type/<int:pk> endpoint"""

        place_type: PlaceType = PlaceType.objects.last()
        path = f'/api/v1/place/type/{place_type.id}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('name'), place_type.name)

    def test_create_dishes(self):
        """ Test case for /api/v1/place/<int:pk>/dish/create/ endpoint.
            When user is place owner
        """

        place: Place = Place.objects.select_related('owner').first()
        path = f'/api/v1/place/{place.pk}/dish/create/'
        data = {
            "dishes":
            [
                {
                    "name": "coffee",
                    "image": base64_pic
                },
                {
                    "name": "fish",
                    "image": base64_pic
                }
            ]
        }

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_update_dish(self):
        """Test case for /api/v1/place/<int:place_id>/dish/<int:dish_id>/update/ endpoint"""

        place: Place = Place.objects.prefetch_related('dishes').first()
        dish: Dishes = place.dishes.last()
        path = f'/api/v1/place/{place.pk}/dish/{dish.pk}/update/'
        data = {
            "name": "toast",
            "image": base64_pic
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.patch(path=path, data=data, format="json")

        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_delete_dish(self):
        """Test case for /api/v1/place/<int:place_id>/dish/<int:dish_id>/delete/ endpoint"""

        place: Place = Place.objects.prefetch_related('dishes').first()
        dish: Dishes = place.dishes.last()
        path = f'/api/v1/place/{place.pk}/dish/{dish.pk}/delete/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.delete(path=path, format="json")

        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_create_opening_hours(self):
        """Test case for /api/v1/place/<int:place_id>/openinghours/create/ endpoint"""

        place: Place = Place.objects.prefetch_related(
            'dishes', 'opening_hours').first()
        path = f'/api/v1/place/{place.pk}/openinghours/create/'
        data = {
            "weekdays":
            [
                {
                    "weekday": 1,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 2,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 3,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 4,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 5,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 6,
                    "from_hour": "9:00",
                    "to_hour": "23:00",
                    "is_dayoff": False
                },
                {
                    "weekday": 7,
                    "is_dayoff": True
                }
            ]
        }

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        days: int = PlaceOpeningHours.objects.filter(place=place).count()
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(days, 7)

    def test_update_place_opening_hours(self):
        """Test case for /api/v1/place/<int:place_id>/openinghours/update/ endpoint"""

        place: Place = Place.objects.first()
        path = f'/api/v1/place/{place.pk}/openinghours/update/'
        data = {
            "weekdays": [
                {
                    "id": 289,
                    "weekday": 1,
                    "fromHour": "10:00:00",
                    "toHour": "23:00:00",
                    "isDayoff": False
                }
            ]
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.patch(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_create_place(self):
        """Test case for /api/v1/place/create/ endpoint"""

        path = '/api/v1/place/create/'
        place_type = PlaceType.objects.first()
        data = {
            "name": "Avra Madison Estiatorio",
            "address": "14 E 60th St, New York, NY 10022, United States",
            "type": [place_type.pk],
            "description": "Upscale Greek seafood eatery with fresh fish by the pound, villa decor & expansive outdoor seating.",
            "deposit": 100,
            "minimum_available_seats": 2,
            "maximum_available_seats": 20,
            "email": "avra@ny.com",
            "phone_number": "+905530000110",
            "menu": "https://www.theavragroup.com/avra-madison/",
            "cover_image": base64_pic
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_update_place(self):
        """Test case for /api/v1/place/update/<int:place_id> endpoint"""

        place: Place = Place.objects.first()
        place_type = PlaceType.objects.first()
        path = f'/api/v1/place/update/{place.pk}'
        data = {
            "name": "Sunny Moon",
            "address": "225 Park Ave S, New York, NY 10704",
            "type": [place_type.pk],
            "description": "Sunny moon - restaurant of asian kitchen",
            "deposit": 25,
            "minimum_available_seats": 2,
            "maximum_available_seats": 15,
            "email": "sunnymoondinner@corp.com",
            "phone_number": "+375295568942"
        }
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.patch(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_place_detail_info(self):
        """Test case for /api/v1/place/details/<int:place_id> endpoint"""

        place: Place = Place.objects.last()
        path = f'/api/v1/place/details/{place.pk}'
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_list_all_places(self):
        """Test case for /api/v1/place/list/ endpoint"""

        path = '/api/v1/place/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path, format="json")
        response_dict = dict(response.data)
        places_num: int = Place.objects.all().count()
        place_num_in_response = response_dict.get('count')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(places_num, place_num_in_response)

    def test_list_authorized_user_places(self):
        """Test case for /api/v1/myplace/list/ endpoint"""

        path = '/api/v1/myplace/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path, format="json")
        response_dict = dict(response.data)
        owner: CustomUser = CustomUser.objects.get(
            phone_number='+375298001010')
        places_num: int = Place.objects.filter(owner=owner).count()
        place_num_in_response = response_dict.get('count')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(places_num, place_num_in_response)

    def test_list_someone_elses_places(self):
        """Test case for /api/v1/<int:corporate_profile_id>/place/list/ endpoint"""

        profile = Profile.objects.filter(profile_type='Corporate').last()
        path = f'/api/v1/{profile.pk}/place/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path, format="json")
        response_dict = dict(response.data)
        place_num_in_response = response_dict.get('count')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(place_num_in_response, 2)
