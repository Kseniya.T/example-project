from django.test import TestCase
from moneyed.classes import Money

from apps.user_app.models import CustomUser
from apps.place_app.models import PlaceType, Place, PlaceOpeningHours, Dishes
from tests import fixtures


class PlaceAppModelTest(TestCase):

    @classmethod
    def setUp(cls):
        phone = '+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(
            phone_number=phone, make_auth_user_corporate_profile=False)

    def test_place_type_model(self):
        """Test case for PlaceType model"""

        name = 'Cafe'
        place_type: PlaceType = PlaceType.objects.create(name=name)
        self.assertEquals(place_type.name, name)
        self.assertTrue(isinstance(place_type, PlaceType))

    def test_create_place(self):
        """Test case for Place model"""

        owner = CustomUser.objects.last()
        name = "Apple Juice"
        type: PlaceType = PlaceType.objects.first()
        address = "100 DS, New York, NY 78955"
        menu = "https://www.menu.com"
        description = "Apple juice - cozy cafe"
        deposit = 30
        deposit_currency = "USD"
        discount = 10
        minimum_available_seats = 3
        maximum_available_seats = 5
        email = "applwjuice@corp.com"
        phone_number = "+905534578100"

        place: Place = Place.objects.create(owner=owner,
                                            name=name,
                                            address=address,
                                            menu=menu,
                                            description=description,
                                            deposit=deposit,
                                            deposit_currency=deposit_currency,
                                            discount=discount,
                                            minimum_available_seats=minimum_available_seats,
                                            maximum_available_seats=maximum_available_seats,
                                            email=email,
                                            phone_number=phone_number,
                                            )
        place.type.add(type.id)

        self.assertEquals(place.owner, owner)
        self.assertEquals(place.name, name)
        self.assertEquals(place.address, address)
        self.assertEquals(place.menu, menu)
        self.assertEquals(place.description, description)
        self.assertEquals(place.deposit, Money('30', 'USD'))
        self.assertEquals(place.discount, discount)
        self.assertEquals(place.minimum_available_seats,
                          minimum_available_seats)
        self.assertEquals(place.maximum_available_seats,
                          maximum_available_seats)
        self.assertEquals(place.email, email)
        self.assertEquals(place.phone_number, phone_number)
        self.assertTrue(isinstance(place, Place))

    def test_place_opening_hours_model(self):
        """Test case for PlaceOpeningHours model"""

        place: Place = Place.objects.last()
        
        place_opening_hours = PlaceOpeningHours.objects.bulk_create([PlaceOpeningHours(place=place, weekday=1, from_hour="9:00", to_hour="23:00", is_dayoff=False),
                                               PlaceOpeningHours(
            place=place, weekday=2, from_hour="9:00", to_hour="23:00", is_dayoff=False),
            PlaceOpeningHours(
            place=place, weekday=3, from_hour="9:00", to_hour="23:00", is_dayoff=False),
            PlaceOpeningHours(
            place=place, weekday=4, from_hour="9:00", to_hour="23:00", is_dayoff=False),
            PlaceOpeningHours(
            place=place, weekday=5, from_hour="9:00", to_hour="23:00", is_dayoff=False),
            PlaceOpeningHours(
            place=place, weekday=6, from_hour="10:00", to_hour="00:00", is_dayoff=False),
            PlaceOpeningHours(
            place=place, weekday=7, is_dayoff=True),
        ])
        self.assertEquals(len(place_opening_hours), 7)
        self.assertEquals(place_opening_hours[6].is_dayoff, True)
        self.assertTrue(isinstance(place_opening_hours[0], PlaceOpeningHours))
    
    def test_create_dishes(self):
        """Test case for Dishes model"""
        
        place: Place = Place.objects.last()
        name = 'toast'
        image = 'tost.png'
        
        dish: Dishes = Dishes.objects.create(place=place, name=name, image=image)

        self.assertEquals(dish.name, name)
        self.assertTrue(isinstance(dish, Dishes))
