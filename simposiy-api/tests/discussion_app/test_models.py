from datetime import timedelta
from django.utils import timezone
from django.test import TestCase
from apps.discussion_app.models import (
    DiscussionType, DiscussionCategory, DiscussionTheme, Discussion, ParticipantsOfDiscussion, UserThemes)
from apps.user_app.models import CustomUser
from apps.place_app.models import Place
from tests import fixtures


class discussionAppModelTest(TestCase):
    
    @classmethod
    def setUp(cls):
        phone='+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(phone_number=phone, make_auth_user_corporate_profile=False)

    def test_discussion_type_model(self):
        """Test case for DiscussionType model"""

        name = 'One-on-one'
        description = 'Meeting between two people, usually a manager and an employee or two colleagues, for the purpose of discussing work-related issues, progress, feedback, and goals'
        discussion_type = DiscussionType.objects.create(
            name=name, description=description)
        self.assertEquals(discussion_type.name, name)
        self.assertEquals(discussion_type.description, description)
        self.assertTrue(isinstance(discussion_type, DiscussionType))

    def test_discussion_category_model(self):
        """Test case for DiscussionCategory model"""

        name = 'Formal meeting'
        description = 'Takes place for the purpose of achieving a common, stated objective'
        discussion_category = DiscussionCategory.objects.create(
            name=name, description=description)
        self.assertEquals(discussion_category.name, name)
        self.assertEquals(discussion_category.description, description)
        self.assertTrue(isinstance(discussion_category, DiscussionCategory))

    def test_discussion_theme_model(self):
        """Test case for DiscussionTheme model"""

        name = 'Art'
        discussion_theme = DiscussionTheme.objects.create(
            name=name)
        self.assertEquals(discussion_theme.name, name)
        self.assertTrue(isinstance(discussion_theme, DiscussionTheme))

    def test_discussion_model(self):
        """Test case for Discussion model"""
        
        initiator = CustomUser.objects.create(phone_number='+375291234567')
        place: Place = Place.objects.first()
        type: DiscussionType = DiscussionType.objects.first()
        category: DiscussionCategory = DiscussionCategory.objects.first()
        theme: DiscussionTheme = DiscussionTheme.objects.first()
        topic='Modern art'
        description='Modern art in modern life'
        dt_start = timezone.now() + timedelta(days=7)
        number_of_participants=5

        discussion = Discussion.objects.create(initiator=initiator, place=place, type=type, category=category, theme=theme, topic=topic, description=description, dt_start=dt_start, number_of_participants=number_of_participants)
        
        self.assertEquals(discussion.initiator, initiator)
        self.assertEquals(discussion.place, place)
        self.assertEquals(discussion.type, type)
        self.assertEquals(discussion.category, category)
        self.assertEquals(discussion.theme, theme)
        self.assertEquals(discussion.topic, topic)
        self.assertEquals(discussion.description, description)
        self.assertEquals(discussion.dt_start, dt_start)
        self.assertEquals(discussion.number_of_participants, number_of_participants)
        self.assertEquals(discussion.is_active, False)
        self.assertEquals(discussion.is_full, False)
        self.assertEquals(discussion.is_checked, False)
        self.assertTrue(isinstance(discussion, Discussion))
    
    def test_participants_of_discussion_model(self):
        """Test case for ParticipantsOfDiscussion model"""
        
        user = CustomUser.objects.create(phone_number='+375291234567')
        discussion: Discussion = Discussion.objects.first()
        number_of_paid_seats = 1

        participant_of_discussion: ParticipantsOfDiscussion = ParticipantsOfDiscussion.objects.create(user=user, discussion=discussion, number_of_paid_seats=number_of_paid_seats)
        
        self.assertEquals(participant_of_discussion.user, user)
        self.assertEquals(participant_of_discussion.discussion, discussion)
        self.assertEquals(participant_of_discussion.number_of_paid_seats, number_of_paid_seats)
        self.assertEquals(participant_of_discussion.is_initiator, False)
        self.assertTrue(isinstance(participant_of_discussion, ParticipantsOfDiscussion))
    
    def test_user_themes_model(self):
        """Test case for UserThemes model"""
        
        user = CustomUser.objects.create(phone_number='+375291234567')
        theme: DiscussionTheme = DiscussionTheme.objects.first()
        
        user_themes: UserThemes = UserThemes.objects.create(user=user, themes=theme)
        self.assertEquals(user_themes.user, user)
        self.assertEquals(user_themes.themes, theme)
        self.assertTrue(isinstance(user_themes, UserThemes))
