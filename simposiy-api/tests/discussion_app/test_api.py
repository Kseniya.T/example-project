from datetime import timedelta
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.utils import timezone

from apps.discussion_app.models import Discussion, DiscussionCategory, DiscussionTheme, DiscussionType, ParticipantsOfDiscussion
from apps.notification_app.models import ApplicationConfig
from apps.place_app.models import Place
from apps.user_app.models import CustomUser
from tests import fixtures


class DiscussionTestCases(APITestCase):
    """Test cases for discussion_app endpoints"""

    @classmethod
    def setUp(cls):
        phone='+375298001010'
        cls.token = fixtures.create_authenticated_client(
            phone_number=phone)
        fixtures.prefill_test_db(phone_number=phone, make_auth_user_corporate_profile=False)

    def test_discussion_types_list(self):
        """Test case for /api/v1/discussion/type-list/ endpoint"""

        path = '/api/v1/discussion/type-list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), 2)

    def test_discussion_type_details(self):
        """Test case for /api/v1/discussion/type/<int:pk>"""
        type = DiscussionType.objects.first()
        path = f'/api/v1/discussion/type/{type.id}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        data = {'id': type.id, 'name': 'Group meeting',
                'description': None, 'image': None}

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(data, response.data)

    def test_discussion_category_list(self):
        """Test case for /api/v1/discussion/category-list/ endpoint"""

        path = '/api/v1/discussion/category-list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), 5)

    def test_discussion_categoty_details(self):
        """Test case for /api/v1/discussion/category/<int:pk> endpoint"""

        category = DiscussionCategory.objects.last()
        path = f'/api/v1/discussion/category/{category.id}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        data = {'id': category.id, 'name': 'Business meeting',
                'description': None, 'image': None}

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(data, response.data)

    def test_discussion_theme_list(self):
        """Test case for /api/v1/discussion/theme-list/ endpoint"""

        path = '/api/v1/discussion/theme-list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), 3)

    def test_discussion_theme_details(self):
        """Test case for /api/v1/discussion/theme/<int:pk> endpoint"""

        theme = DiscussionTheme.objects.first()
        path = f'/api/v1/discussion/theme/{theme.id}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        data = {'id': theme.id, 'name': 'Art', 'image': None,
                'is_golden': False, 'is_top': False}

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(data, response.data)

    def test_create_discussion(self):
        """Test case for /api/v1/discussion/create/ endpoint"""
        
        path = '/api/v1/discussion/create/'
        dt_start = timezone.now() + timedelta(days=7)
        place: Place = Place.objects.first()
        type: DiscussionType = DiscussionType.objects.first()
        category: DiscussionCategory = DiscussionCategory.objects.first()
        theme: DiscussionTheme = DiscussionTheme.objects.first()
        
        data = {
            "place": place.pk,
            "type": type.pk,
            "category": category.pk,
            "theme": theme.pk,
            "topic": "Stray game",
            "dt_start": dt_start,
            "number_of_participants": 5
        }

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.post(path=path, data=data, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_discussion_detail(self):
        """Test case for /api/v1/discussion/details/<int:pk> endpoint"""
        discussion = Discussion.objects.first()
        
        path = f'/api/v1/discussion/details/{discussion.pk}'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        if response.status_code == 200:
            self.assertEqual(status.HTTP_200_OK, response.status_code)
        elif response.status_code == 404:
            print(response.data)

    def test_all_active_discussions_list(self):
        """Test case for /api/v1/activediscussions/list/ endpoint"""

        path = '/api/v1/activediscussions/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        response_dict = dict(response.data)

        discussnos_number: int = Discussion.objects.filter(
            is_active=True).count()

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), discussnos_number)

    def test_participant_of_discussion(self):
        """Test case for /api/v1/user/discussions/list/ endpoint"""

        path = '/api/v1/user/discussions/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        user: CustomUser = CustomUser.objects.get(phone_number='+375298001010')
        participants_number: int = ParticipantsOfDiscussion.objects.filter(
            user_id=user.id).count()

        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), participants_number)

    def test_someone_else_discussion_list(self):
        """Test case for /api/v1/profile/<int:pk>/discussions/list/ endpoint"""

        user: CustomUser = CustomUser.objects.prefetch_related('profile').get(phone_number='+905538652318')
        participants_number: int = ParticipantsOfDiscussion.objects.filter(
            user_id=user.id).count()
        user_profile_pk = user.profile.id

        path = f'/api/v1/profile/{user_profile_pk}/discussions/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), participants_number)

    def test_all_active_discussions_of_place(self):
        """Test case for /api/v1/place/<int:place_id>/discussions/list/ endpoint"""

        place: Place = Place.objects.last()
        path = f'/api/v1/place/{place.id}/discussions/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)
        response_dict = dict(response.data)

        discussions_number: int = Discussion.objects.filter(
            place=place).count()

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response_dict.get('count'), discussions_number)

    def test_initiated_discussions(self):
        """Test case for /api/v1/discussions/iniciated/list/ endpoint"""

        path = '/api/v1/discussions/iniciated/list/'

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        response = self.client.get(path=path)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
