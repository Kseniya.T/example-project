from datetime import timedelta
from typing import Dict, List, Union
import firebase_admin as fba
from firebase_admin import messaging, credentials

from core.settings import GOOGLE_APPLICATION_CREDENTIALS

cred = credentials.Certificate(GOOGLE_APPLICATION_CREDENTIALS)
fba.initialize_app(cred)


class FirebaseMessage:
    # title: str
    # message: str
    # token_list: list
    # icon: str
    # priority: Priority
    # priority_notifications: PriorityNotifications
    # visibility: Visibility


    # def __init__(self, title: str, 
    #             message: str, token_list: list, 
    #             icon="", color='#1771F1',
    #             priority=Priority.NORMAL, 
    #             priority_notifications=PriorityNotifications.DEFAULT, 
    #             visibility=Visibility.PUBLIC):
    #     self.title = title
    #     self.message = message
    #     self.token_list = token_list
    #     self.icon = icon
    #     self.color = color
    #     self.priority = Priority(priority)
    #     self.priority_notifications = PriorityNotifications(
    #         priority_notifications)
    #     self.visibility = Visibility(visibility)
    
    # def send_push(self):
    #     """Method for sending push notification"""
    
    #     message = messaging.MulticastMessage(
    #         notification=messaging.Notification(
    #             title=self.title,body=self.message),
    #         tokens=self.token_list,
    #         android=messaging.AndroidConfig(
    #             ttl=timedelta(seconds=3600),
    #             priority=self.priority.value,
    #             notification=messaging.AndroidNotification(
    #                 icon=self.icon,
    #                 color=self.color,
    #                 priority=self.priority_notifications.value,
    #                 visibility=self.visibility.value,
    #                 default_sound=True,
    #                 default_vibrate_timings=True,
    #                 default_light_settings=True,
    #             ),
    #             data={
    #                 'content-available': '1'
    #             }
    #         ),
    #         apns=messaging.APNSConfig(
    #             payload=messaging.APNSPayload(
    #                 aps=messaging.Aps(
    #                     badge=1, 
    #                     sound="default",
    #                     mutable_content=True),
    #             ),
    #             headers={
    #                 "apns-priority":"10"}
    #         ),)

    #     response = messaging.send_multicast(message)
    #     return response


    def __init__(self, title: str, message: str, token_list: List[str], data: Dict[str, Union[str, int]]):
        self.title = title
        self.message = message
        self.tokenList = token_list
        self.data = data

    def send_push(self):
        message = messaging.MulticastMessage(
            notification=messaging.Notification(
                title=self.title, body=self.message),
            tokens=self.tokenList,
            data=self.data
        )

        response = messaging.send_multicast(message)

        print(f'Successfully sent message {response}')
        return response
