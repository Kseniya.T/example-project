from storages.backends.s3boto3 import S3Boto3Storage
from core.settings import AWS_PDF_LOCATION, AWS_PUBLIC_MEDIA_LOCATION, AWS_STATIC_LOCATION
import mimetypes
from abc import ABC
import os
import uuid
from storages.backends.s3boto3 import S3Boto3Storage
from storages.utils import clean_name


class AbstractStorage(S3Boto3Storage, ABC):
    acl_arg: str = None

    def _get_write_parameters(self, name, content=None):
        params = {}

        _type, encoding = mimetypes.guess_type(name)
        content_type = getattr(content, 'content_type', None)
        content_type = content_type or _type or self.default_content_type

        params['ContentType'] = content_type
        if encoding:
            params['ContentEncoding'] = encoding

        params.update(self.get_object_parameters(name))

        if self.acl_arg or self.default_acl:
            params['ACL'] = self.acl_arg or self.default_acl

        return params

    def _save(self, name, content):
        _name = name.split('/')[-1]
        file_extention = _name.split('.')[-1]
        name = f"{uuid.uuid4()}.{file_extention}"
        cleaned_name = clean_name(name)
        name = self._normalize_name(cleaned_name)
        params = self._get_write_parameters(name, content)

        if not hasattr(content, 'seekable') or content.seekable():
            content.seek(0, os.SEEK_SET)
        if (self.gzip and
                params['ContentType'] in self.gzip_content_types and
                'ContentEncoding' not in params):
            content = self._compress_content(content)
            params['ContentEncoding'] = 'gzip'

        obj = self.bucket.Object(name)
        obj.upload_fileobj(content, ExtraArgs=params)
        return cleaned_name


class StaticStorage(AbstractStorage):
    location = AWS_STATIC_LOCATION


class PublicMediaStorage(AbstractStorage):
    location = AWS_PUBLIC_MEDIA_LOCATION
    file_overwrite = False
    default_acl = 'public-read'


class PDFStorage(S3Boto3Storage):
    location = AWS_PDF_LOCATION
    file_overwrite = False
    default_acl = 'public-read'
