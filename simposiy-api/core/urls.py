from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from django.conf import settings
from django.conf.urls.static import static
from core.settings import DEBUG
from .yasg import urlpatterns as doc_urls


API_V1_URL_PATH: str = 'api/v1/'

urlpatterns = [
    path('admin/', admin.site.urls),
    path(API_V1_URL_PATH, include('apps.auth_app.urls')),
    path(API_V1_URL_PATH, include('apps.user_app.urls')),
    path(API_V1_URL_PATH, include('apps.notification_app.urls')),
    path(API_V1_URL_PATH, include('apps.place_app.urls')),
    path(API_V1_URL_PATH, include('apps.discussion_app.urls')),
    path(API_V1_URL_PATH, include('apps.payment_app.urls')),
    path(API_V1_URL_PATH, include('apps.admin_pages_app.urls')),
    path(API_V1_URL_PATH, include('apps.geolocations_app.urls')),
]

if DEBUG:
    urlpatterns += doc_urls
    urlpatterns += [
    path(f'{API_V1_URL_PATH}schema/', SpectacularAPIView.as_view(), name='schema'),
    path('silk/', include('silk.urls', namespace='silk')),
    path(f'{API_V1_URL_PATH}schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
