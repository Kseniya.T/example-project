import os
from celery import Celery
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')

app = Celery('core')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    # 'send_push_notifications': {
    #     'task': 'apps.notification_app.tasks.send_push_notifications',
    #     'schedule': crontab(minute='*/10'),
    # },
    'check_discussion_start_date': {
        'task': 'apps.discussion_app.tasks.check_discussion_start_date',
        'schedule': crontab(minute='*/10'),
    },
    'check_discussion_is_full': {
        'task': 'apps.discussion_app.tasks.check_discussion_is_full',
        'schedule': crontab(minute='*/10'),
    },
    'check_unconfirmed_orders': {
        'task': 'apps.payment_app.tasks.check_unconfirmed_orders',
        'schedule': crontab(minute='*/10'),
    },
    'remove_empty_orders': {
        'task': 'apps.payment_app.tasks.remove_empty_orders',
        'schedule': crontab(minute='*/15'),
    },
    'check_charge_data': {
        'task': 'apps.payment_app.tasks.check_charge_data',
        'schedule': crontab(minute='*/30'),
    },
    'check_minimum_number_participants': {
        'task': 'apps.discussion_app.tasks.check_minimum_number_participants',
        'schedule': crontab(minute='*/5'),
    },
    'check_response_from_place_owner': {
        'task': 'apps.discussion_app.tasks.check_response_from_place_owner',
        'schedule': crontab(minute='*/60'),
    },
}
