from rest_framework.generics import GenericAPIView, ListAPIView, RetrieveAPIView
from rest_framework import mixins

from utils.exceptions import NotAuthenticatedError, ObjectNotFoundError, PermissionDeniedError


class PermissionDeniedMixin:
    """Mixin for raising custom `NotAuthenticatedError` and `PermissionDeniedError` exceptions"""

    def permission_denied(self, request, message=None, code=None):
        if request.authenticators and not request.successful_authenticator:
            raise NotAuthenticatedError
        raise PermissionDeniedError


class BaseGenericAPIView(PermissionDeniedMixin, GenericAPIView):
    """Base GenericAPIView class"""
    pass


class BaseListAPIView(PermissionDeniedMixin, ListAPIView):
    """Base ListAPIView class"""
    pass


class BaseRetrieveCreateAPIView(PermissionDeniedMixin, mixins.RetrieveModelMixin,
                                mixins.CreateModelMixin, GenericAPIView):
    """
    Concrete view for retrieving, creating a model instance.
    """

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class CustomRetrieveAPIView(PermissionDeniedMixin, RetrieveAPIView):
    # lookup_url_kwarg: int = 'pk'
    lookup_field: int = 'pk'
    not_found_message: str = 'Object was not found'

    def get_object(self):
        try:
            obj = super().get_object()
            return obj
        except:
            raise ObjectNotFoundError(self.not_found_message)
