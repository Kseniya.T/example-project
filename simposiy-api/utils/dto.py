from dataclasses import dataclass
from typing import Optional
from apps.auth_app.models import Session
from apps.user_app.models import CustomUser


@dataclass
class UserDTO:
    user: Optional[CustomUser] = None
    session: Optional[Session] = None
