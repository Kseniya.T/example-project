from typing import Dict
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK


class ErrorResponse(Response):
    """
        @return By default Response object with data = data = {'error': 'your message'} and status_code = 400
    """

    data: Dict
    status: int

    def __init__(self, detail: str, error_type: str = None, status: int = HTTP_400_BAD_REQUEST, obj: dict = None, *args, **kwargs):
        self.status = status
        self.data = {
            'error': detail,
            'error_type': error_type,
            'object': obj,
        }
        super().__init__(data=self.data, status=self.status, *args, **kwargs)


class SuccessResponse(Response):
    """
        @return By default Response object with data = {'success': True, 'detail': 'OK'} and status_code = 200
    """

    data: Dict
    status: int

    def __init__(self, detail: str = None, status: int = HTTP_200_OK, obj: dict = None, is_success: bool = True, *args, **kwargs):
        self.status = status
        self.data = {
            'is_success': is_success,
            'detail': detail,
            'object': obj
        }
        self.status = status
        super().__init__(data=self.data, status=self.status, *args, **kwargs)
