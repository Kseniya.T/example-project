from dataclasses import dataclass
import hashlib
import uuid
from datetime import datetime, timedelta
from random import randint

from django.core.cache import cache
from django.utils import timezone

from core.settings import OTP_SMS_TTL


@dataclass(frozen=True)
class OTPCheckResultDTO:
    is_expired: bool
    is_valid: bool


@dataclass(frozen=True)
class OTPInCacheDTO:
    is_exists: bool
    expired_at: datetime | None = None


class OTP:
    """Class for generating and checking OTP. Used in user's autorization case"""
    _otp_code: str = None
    expiration: timedelta = None
    phone_number: str = None
    _current_datetime = None
    DEFAULT_SMS_PREFIX: str = 'sms_cache_key'

    def __init__(self, session_pk: int, phone_number: str, expiration: int,
                 prefix: str = DEFAULT_SMS_PREFIX):
        self.phone_number = f'{prefix}_{phone_number}_{session_pk}'
        self.expiration = timedelta(seconds=expiration)

    def __str__(self):
        return f'CustomOTP object {self.phone_number}'

    def generate_otp(self) -> str:
        self._otp_code = str(randint(100000, 999999))
        self._current_datetime = timezone.now()
        hashed_otp = self.__make_hash(self._otp_code)
        value = {'otp_key': hashed_otp,
                 'dt_expiration': self._current_datetime + self.expiration}
        cache.set(self.phone_number, value)
        return self._otp_code

    def verify(self, user_otp: str) -> OTPCheckResultDTO:
        dt = timezone.now()
        value = cache.get(self.phone_number)
        if value:
            dt_cached = value.get('dt_expiration')
            if dt > dt_cached:
                return OTPCheckResultDTO(is_expired=True, is_valid=False)
            else:
                hashed_otp = value.get('otp_key')
                if hashed_otp:
                    result = self.__check_raw_otp(hashed_otp, user_otp)
                    if result:
                        cache.delete(self.phone_number)
                    return OTPCheckResultDTO(
                        is_expired=False,
                        is_valid=result)
                return OTPCheckResultDTO(is_expired=False, is_valid=False)
        return OTPCheckResultDTO(is_expired=True, is_valid=False)

    @staticmethod
    def __check_raw_otp(hashed_otp: str, user_otp: str) -> bool:
        otp, salt = hashed_otp.split(':')
        return otp == hashlib.sha256(salt.encode() +
                                     user_otp.encode()).hexdigest()

    @staticmethod
    def __make_hash(user_otp: str) -> str:
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() +
                              user_otp.encode()).hexdigest() + ':' + salt

    @property
    def otp_code(self):
        return self._otp_code

    @property
    def current_datetime(self) -> datetime:
        return self._current_datetime

    @property
    def datetime_resend(self) -> datetime:
        return self._current_datetime + self.expiration

    @staticmethod
    def find_otp_in_cache(session_pk: int, phone_number: str = None,
                          prefix: str = DEFAULT_SMS_PREFIX) -> OTPInCacheDTO:
        """Methos wich looking for otp in cache"""
        cache_key = f'{prefix}_{phone_number}_{session_pk}'
        cache_value = cache.get(cache_key)
        
        if not cache_value:
            return OTPInCacheDTO(is_exists=False, expired_at=None)
            
        datetime_expiration: datetime = cache_value.get('dt_expiration')
        if datetime_expiration and datetime_expiration > timezone.now():
            return OTPInCacheDTO(is_exists=True, expired_at=datetime_expiration)
        return OTPInCacheDTO(is_exists=False, expired_at=None)