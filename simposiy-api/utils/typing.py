from datetime import datetime
from typing import Any, Dict, NamedTuple
from dateutil.parser import parse
from sentry_sdk import capture_exception


class StringConstantNamedTuple:
    """ 
        This class used for storing string constants which name_field == name_field.lower().
        For example, your field name is SOMETHING_CONSTANT, and his value is "something_constant"
        Then this class can save your time.

        Derivate from this class and in the child class define the fields in the format:

        class SomeClass(StringConstantNamedTuple):
            CONSTANT_1: str
            CONSTANT_2: str
            CONSTANT_3: str

        After, create object from this class.

        some_object = SomeClass()

        And then, you can get access to yout declared fields.

        print(f'{some_object.CONSTANT_3=}')

        Enjoy.
    """
    def __new__(cls, *args, **kwargs):
        list_fields: list[str] = [field for field in cls.__annotations__]
        _dict: dict[str, str] = dict(
            {field: field.lower() for field in list_fields})
        fields: list[tuple(str, type(str))] = [(field, str)
                                               for field in list_fields]
        return NamedTuple(cls.__name__, fields)(**_dict)


class SmartNamedTuple:
    """A class that parses a dictionary and automatically unpacks them into objects"""
    def __new__(cls, *args, **kwargs):
        fields = cls.__annotations__.items()
        _dict: Dict[str, Any] = {
            field_name: cls._parse_value(
                cls_name=_class,
                value=kwargs.get(field_name),
            )
            for field_name, _class in fields
        }
        return NamedTuple(cls.__name__, fields)(**_dict)

    @classmethod
    def _parse_value(cls, cls_name: Any, value: Any, unpack_iterable=False) -> Any:
        if unpack_iterable:
            return [cls_name(**item) for item in value]

        if cls_name is datetime:
            return parse(value)
        elif isinstance(value, dict):
            return cls_name(**value)
        try:
            if hasattr(cls_name, '__iter__'):
                child_cls_name = cls_name.__args__[0]
                if child_cls_name:
                    return cls._parse_value(
                        cls_name=child_cls_name,
                        value=value,
                        unpack_iterable=True,
                    )
        except (AttributeError, IndexError) as e:
            capture_exception(e)
        return value
