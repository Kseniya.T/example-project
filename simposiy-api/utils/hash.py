import hashlib
import uuid


def check_hashed_string(hashed_string: str, raw_string: str) -> bool:
    """Function for comparing `hashed_string` and `raw_string`. Used for refresh token."""
    string, salt = hashed_string.split(':')
    return string == hashlib.sha256(salt.encode() + raw_string.encode()).hexdigest()


def make_hashed_string(raw_string: str, with_salt: bool = True) -> str:
    """Function for making hashed `raw_string`. Used for refresh token."""
    if with_salt:
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + raw_string.encode()).hexdigest() + ':' + salt
    return hashlib.sha256(raw_string.encode()).hexdigest()
