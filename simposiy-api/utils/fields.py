import binascii
import base64
import uuid
from sentry_sdk import capture_exception, capture_message, set_context
import six

from django.core.files.base import ContentFile
from rest_framework.fields import ImageField
from rest_framework.settings import api_settings

from utils.exceptions import CustomAPIException


class CustomImageField(ImageField):
    def to_representation(self, value):
        if not value:
            return None

        use_url = getattr(self, 'use_url', api_settings.UPLOADED_FILES_USE_URL)
        if use_url:
            try:
                url = value.url
            except AttributeError as e:
                capture_exception(e)
                return None
            request = self.context.get('request', None)
            if request is not None:
                return request.build_absolute_uri(url)
            return url

        return value.name


class Base64ImageField(CustomImageField):
    """
        A Django REST framework field for handling image-uploads through raw post data.
        It uses base64 for encoding and decoding the contents of the file.

        Heavily based on
        https://github.com/tomchristie/django-rest-framework/pull/1268

        Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError as e:
                capture_exception(e)
                self.fail('invalid_image')
            except binascii.Error as e:
                set_context('binascii error', value=e.__dict__)
                capture_message('Incorrect image error', level='error')
                raise CustomAPIException(
                    error='incorrect_image',
                    detail='Error! Check the correctness of the entered data! ',
                )

            # Generate file name:
            # 12 characters are more than enough.
            file_name = str(uuid.uuid4())[:12]
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension
