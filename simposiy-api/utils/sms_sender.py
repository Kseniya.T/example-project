from urllib.error import URLError
from urllib.parse import quote
from urllib.request import urlopen
from sentry_sdk import capture_exception

from core.settings import DEBUG, SMS_API_ID, SMS_FROM, OTP_SMS_TTL


class SMSRepository:

    servicecodes = {
        100: "Message has been accepted for sending. On the following lines you will find the identifiers of the sent messages in the same order in which you indicated the numbers on which the sending was made.",
        200: "Wrong api_id",
        201: "Not enough funds in your account",
        202: "Recipient specified incorrectly",
        203: "No message text",
        204: "The sender's name was not agreed with the administration",
        205: "Message is too long (exceeds 8 SMS)",
        206: "The daily limit for sending messages will be exceeded or already exceeded",
        207: "Messages cannot be sent to this number (or one of the numbers), or more than 100 numbers are indicated in the list of recipients",
        208: "'Time' parameter specified incorrectly",
        209: "You have added this number (or one of the numbers) to the stop list",
        210: "GET is used where POST needs to be used",
        211: "Method not found",
        220: "The service is temporarily unavailable, please try again later.",
        300: "Wrong token (probably expired or your IP has changed)",
        301: "Wrong password or user not found",
        302: "The user is authorized, but the account is not confirmed (the user did not enter the code sent in the registration SMS)",
    }


class SendSMSError(Exception):
    pass


def show_debug_messages(message):
    if DEBUG:
        print(message)


def send_sms(phone_number: str, message: str):
    """Method for sending SMS messages to a phone number"""

    sms_api_id = SMS_API_ID
    sms_from = SMS_FROM
    sms_time_out = OTP_SMS_TTL

    show_debug_messages(f'smssend[debug] {phone_number}, {message}')

    # url = f'http://sms.ru/sms/send?api_id={sms_api_id}&to={phone_number}&text={quote(message)}'
    # if sms_from is not None:
    #     url = f"{url}&from={sms_from}"

    # try:
    #     result = urlopen(url, timeout=sms_time_out)
    #     show_debug_messages(f'GET: {result.geturl()} {result.msg}')
    # except URLError as errstr:
    #     capture_exception(errstr)
    #     show_debug_messages(f'smssend[debug]: {errstr}')
    #     raise SendSMSError(f'An error occurred while sending SMS: {errstr}')

    # service_result = result.read().splitlines()
    # if service_result is not None and int(service_result[0]) == 100:
    #     show_debug_messages(f'smssend[debug]: Message send ok. ID: {service_result[1]}')

    # if service_result is not None and int(service_result[0]) != 100:
    #     show_debug_messages(
    #     f'smssend[debug]: Unable send sms message to {phone_number} when service has returned code: {SMSRepository.servicecodes[int(service_result[0])]}')
    #     raise SendSMSError(
    #         f'An error occurred while sending SMS: {SMSRepository.servicecodes[int(service_result[0])]}')

    return False
