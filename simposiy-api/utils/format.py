from typing import Sequence


def to_snake_case(element: str) -> str:
    """Method for making ['Some Thing', ] to ['some_thing', ] """
    return element.replace(' ', '_').lower()