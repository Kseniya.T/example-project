from dataclasses import is_dataclass
from typing import ClassVar
from rest_framework.serializers import Serializer

from rest_framework.fields import empty


class MainSerializer(Serializer):
    """Base serializer"""
    
    dto_class: ClassVar
    """In this dataclass will be unpacked validated_data OrderedDict.
    Fields in DTO class must be same as in serilizer"""
    
    ignore_dto_class: bool = False
    
    def __init__(self, instance=None, data=empty, **kwargs):
        if not self.ignore_dto_class:
            if not self.dto_class:
                raise ValueError(
                    'For using BaseCustomSerializer set "dto_class" variable')
            if not is_dataclass(self.dto_class):
                raise TypeError('"dto_class" must be a dataclass')
        super().__init__(instance, data, **kwargs)

    @property
    def dto(self):
        """Returns `dto_class` from `validated_data`"""
        return self.dto_class(**self.validated_data)
