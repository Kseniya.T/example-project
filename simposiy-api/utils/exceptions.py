import datetime
from typing import Optional

from rest_framework.exceptions import APIException
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_406_NOT_ACCEPTABLE, HTTP_401_UNAUTHORIZED, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN
from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.utils.serializer_helpers import ReturnDict, ReturnList

from .errors import app_errors


def _get_error_details(data, default_code=None):
    """
    Descend into a nested data structure, forcing any
    lazy translation strings or strings into `ErrorDetail`.

    Keeps None, True, False and integers.
    """
    # A copy from DRF 3.9 except for the change below
    if isinstance(data, list):
        ret = [
            _get_error_details(item, default_code) for item in data
        ]
        if isinstance(data, ReturnList):
            return ReturnList(ret, serializer=data.serializer)
        return ret
    elif isinstance(data, dict):
        ret = {
            key: _get_error_details(value, default_code)
            for key, value in data.items()
        }
        if isinstance(data, ReturnDict):
            return ReturnDict(ret, serializer=data.serializer)
        return ret

    # Only change:
    if data is None or data is True or data is False or isinstance(data, int):
        return data
    return force_str(data)


class CustomBaseAPIException(APIException):
    # A copy of ValidationError from DRF 3.9
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Invalid input.')
    default_code = 'invalid'

    def __init__(self, detail=None, code=None):
        if detail is None:
            detail = self.default_detail
        if code is None:
            code = self.default_code

        # For validation failures, we may collect many errors together,
        # so the details should always be coerced to a list if not already.
        if not isinstance(detail, dict) and not isinstance(detail, list):
            detail = [detail]

        self.detail = _get_error_details(detail, code)

    # Changed methods

    def get_codes(self):
        return self.default_code

    def get_full_details(self):
        raise NotImplementedError


class CustomAPIException(CustomBaseAPIException):
    error: str = app_errors.VALIDATION_ERROR
    detail: str = None
    status_code: int = HTTP_400_BAD_REQUEST
    obj: Optional[dict] = None
    data: dict

    def __init__(self, detail: str = None, obj: Optional[dict] = None):
        self.data = {}

        if detail:
            self.detail = detail

        self.data.update({
            'error': self.error,
            'detail': self.detail,
        })

        if obj:
            self.obj = obj
            self.data.update(object=self.obj)

        super(CustomAPIException, self).__init__(self.data)


class UserAlreadyRegistered(CustomAPIException):
    error: str = app_errors.USER_ALREADY_REGISTERED
    detail: str = 'A user with this phone number is already registered in the system'


class UniqueConstraint(CustomAPIException):
    error: str = app_errors.UNIQUE_CONSTRAINT
    detail: str = 'Duplicate unique data.'

# class ApplicationAlreadyRegistered(CustomValidationError):
#     error: str = app_errors.APPLICATION_ALREADY_REGISTERED
#     detail: str = 'Your application has been accepted for processing, wait for confirmation from the operator'


class OTPAlreadySentException(CustomAPIException):
    error: str = app_errors.OTP_HAS_ALREADY_SENT
    detail: str = 'Code has already been sent. Please try again later'


class UserNotFoundError(CustomAPIException):
    error: str = app_errors.USER_NOT_FOUND
    detail: str = 'User not found'


class UserDeactivatedError(CustomAPIException):
    error: str = app_errors.USER_IS_DEACTIVATED
    detail: str = 'User deactivated'
    status_code: int = HTTP_401_UNAUTHORIZED


class UserDeletedError(CustomAPIException):
    error: str = app_errors.USER_WAS_DELETED
    detail: str = 'User was deleted'
    status_code: int = HTTP_401_UNAUTHORIZED


class DeactivatedSessionError(CustomAPIException):
    error: str = app_errors.SESSION_IS_DEACTIVATED
    detail: str = 'Session deactivated'
    status_code: int = HTTP_401_UNAUTHORIZED


class SessionNotFoundError(CustomAPIException):
    error: str = app_errors.SESSION_NOT_FOUND
    detail: str = 'Session not found'
    status_code: int = HTTP_401_UNAUTHORIZED


class RefreshTokenNotValidError(CustomAPIException):
    error: str = app_errors.REFRESH_TOKEN_NOT_VALID
    detail: str = 'Refresh token is not valid'
    status_code: int = HTTP_400_BAD_REQUEST


class BlockedSessionError(CustomAPIException):
    error: str = app_errors.SESSION_IS_BLOCKED
    detail: str = 'Session locked'
    status_code: int = HTTP_401_UNAUTHORIZED

    def __init__(self,  unlock_dt: datetime, detail: str = None, obj: Optional[dict] = None):
        self.unlock_dt = unlock_dt
        self.data.update(unlock_dt=self.unlock_dt)
        super().__init__(detail, obj)


class InputValueError(CustomAPIException):
    error: str = app_errors.INPUT_VALUE_ERROR
    status_code: int = HTTP_400_BAD_REQUEST


class ObjectNotFoundError(CustomAPIException):
    error = app_errors.OBJECT_NOT_FOUND
    status_code = HTTP_404_NOT_FOUND
    detail = 'Object not found'


class NotFullFilledError(CustomAPIException):
    error: str = app_errors.NOT_FULL_FILLED_ERROR


class JWTNotValidError(CustomAPIException):
    error = app_errors.JWT_NOT_VALID
    status_code = HTTP_401_UNAUTHORIZED
    detail = 'Failed to correctly decode user token'


class NotAuthenticatedError(CustomAPIException):
    error = app_errors.NOT_AUTHENTICATED
    status_code = status.HTTP_401_UNAUTHORIZED
    detail = 'Authentication credentials were not provided'


class PermissionDeniedError(CustomAPIException):
    error = app_errors.PERMISSION_DENIED
    status_code = HTTP_403_FORBIDDEN
    detail = 'You do not have permission to perform this action'


class OTPExpiredError(CustomAPIException):
    error = app_errors.OTP_EXPIRED
    status_code = HTTP_400_BAD_REQUEST
    detail = 'The verification code has expired. Request new one'


class OTPNotValidError(CustomAPIException):
    error = app_errors.OTP_NOT_VALID
    status_code = HTTP_400_BAD_REQUEST
    detail = 'Verification code is incorrect'


class AlreadyHasProfileError(CustomAPIException):
    error = app_errors.PROFILE_ALREADY_EXISTS
    status_code = HTTP_403_FORBIDDEN
    detail = 'From one account only one profile can be created'


class DiscussionIsFullError(CustomAPIException):
    error: str = app_errors.ALL_SEATS_FILLED
    detail: str = 'All seats in this discussion are already filled.'


class DiscussionUpdateError(CustomAPIException):
    error: str = app_errors.DISCUSSION_UPDATE_ERROR
    detail: str = 'Active discussions are uneditable.'


class NotEnoughSeatsError(CustomAPIException):
    error: str = app_errors.NOT_ENOUGH_SEATS
    detail: str = 'Sorry, but you can not pay for more seats.'


class MinLimitOfSeatsError(CustomAPIException):
    error: str = app_errors.MINIMUM_LIMIT_OF_SEATS_ERROR
    detail: str = 'Can\'t create discussion with number of partipicians less then defined in chosen place of discussion.'


class MaxLimitOfSeatsError(CustomAPIException):
    error: str = app_errors.MAXIMUM_LIMIT_OF_SEATS_ERROR
    detail: str = 'Can\'t create discussion with number of partipicians more then defined in chosen place of discussion.'


class MaxLimitOfPlaceTypesError(CustomAPIException):
    error: str = app_errors.MAXIMUM_LIMIT_OF_PLACE_TYPES_ERROR
    detail: str = 'Can\'t add more than 3 types to one location'


class WrongNumberOfSeatsError(CustomAPIException):
    error: str = app_errors.WRONG_NUMBER_OF_SEATS_ERROR
    detail: str = 'Only 2 number of seats can be specified for this type of discussion'


class DiscussionIsOver(CustomAPIException):
    error: str = app_errors.DISCUSSION_IS_OVER
    detail: str = 'This discussion is over.'


class WrongDiscussionDate(CustomAPIException):
    error: str = app_errors.WRONG_DISCUSSION_DATE
    detail: str = 'Wrong discussion date selected.'


class LimitNumberOfSeatsPurchased(CustomAPIException):
    error: str = app_errors.MAX_NUMBER_OF_SEATS_PURCHASED
    detail: str = 'Sorry, you have already purchased a seat in this discussion.'


class WeekDayDoesntExists(CustomAPIException):
    error: str = app_errors.NON_EXISTENT_WEEK_DAY
    detail: str = 'You specified a non-existent day of the week.'


class MAxNumberOfDishes(CustomAPIException):
    error: str = app_errors.MAX_NUMBER_OF_DISHES
    detail: str = 'You can add max 5 dishes to one location.'


class MAxNumberOfImages(CustomAPIException):
    error: str = app_errors.MAX_NUMBER_OF_IMAGES
    detail: str = 'You can add max 9 images to one location.'


class ParticipantWithoutPayment(CustomAPIException):
    error: str = app_errors.PARTICIPANT_WITHOUT_PAYMENT
    detail: str = 'Need to pay for seat in discussion to be participant.'


class BankRequisitesWasNotCreatedError(CustomAPIException):
    error: str = app_errors.BANK_REQUISITES_ERROR
    detail: str = 'Please fill out the form with bank details at first.'


class AcceptDenyDiscussionError(CustomAPIException):
    error: str = app_errors.ACCEPT_DENY_DISCUSSION_ERROR
    detail: str = 'Sorry, you can accept or deny same discussion only once'





# class RefundError(CustomAPIException):
#     error: str = app_errors.REFUND_ERROR
#     detail: str = 'The deadline for requesting a refund is 24 hours before the start of the discussion.'


# Selenium exceptions
class SeleniumWebdriverError(CustomAPIException):
    error: str = app_errors.SELENIUM_WEBDRIVER_ERROR
    detail: str = 'Some error occure durin auto filling fields. Please, try again.'


# Stripe service exceptions
class StripeRefundsError(CustomAPIException):
    error: str = app_errors.REFUND_ERROR
    detail: str = 'An error occurred during the refund.'


class StripeUnsupportedPhoneNumberError(CustomAPIException):
    error: str = app_errors.STRIPE_UNSUPPORTED_PHONE_NUMBER
    detail: str = 'You can not create corporate account for your registered phone number.'
    status_code = HTTP_406_NOT_ACCEPTABLE
