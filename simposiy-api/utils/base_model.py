from django.db import models


class AppBaseModel(models.Model):
    """Abstract base model"""

    def update_fields(self, save: bool = True, **kwargs):
        """
        Method for smart updating field 
        :param save: if True, obj will be saved
        :param kwargs: dict(field_name=value)
        """
        _updated_fields = []
        for key, value in kwargs.items():
            if hasattr(self, key):
                update_field = getattr(self, key)
                _updated_fields.append(key)
                if update_field != value:
                    setattr(self, key, value)
        self.save(update_fields=_updated_fields) if save else None

    def check_empty_field(self, field: str):
        """Checking for empty field"""
        res = getattr(self, field)
        return False if res and res != '' or None else True

    def check_filled_field(self, field: str):
        """Checking for filled field"""
        res = getattr(self, field)
        return True if res and res != '' or None else False

    class Meta:
        abstract = True
