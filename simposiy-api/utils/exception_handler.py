from rest_framework import exceptions, status, views

from utils.errors import app_errors


def custom_exception_handler(exc: exceptions.ValidationError, context):
    """Class for handling DRF exception"""

    response = views.exception_handler(exc, context)
    if isinstance(exc, (exceptions.AuthenticationFailed, exceptions.NotAuthenticated)):
        response.status_code = status.HTTP_401_UNAUTHORIZED
    elif isinstance(exc, exceptions.ValidationError):
        customized_response = dict(error=app_errors.VALIDATION_ERROR)
        customized_response.update(detail_list=[])
        for key, value in response.data.items():
            error = {'field': key, 'message': value}
            customized_response['detail_list'].append(error)
        response.data = customized_response
    return response
