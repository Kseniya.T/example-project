from core import settings


def get_api_version():
    """Method for return API_VERSION"""
    try:
        return settings.API_VERSION
    except AttributeError:
        return {"error": "API_VERSION is not defined in settings.py"}
