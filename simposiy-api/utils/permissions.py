from rest_framework.permissions import BasePermission
from django.contrib.auth.models import AnonymousUser
from rest_framework.exceptions import AuthenticationFailed
from django.core.exceptions import ObjectDoesNotExist

from utils.dto import UserDTO


class IsAuthenticated(BasePermission):
    """
    Access for authorized users only
    """

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser):
            return False
        elif isinstance(request.user, UserDTO):
            if not request.user:
                raise AuthenticationFailed("Need authorization")
            return bool(
                request.user.user and not request.user.session.is_deactivated
                and not request.user.session.is_blocked
            )
        return False


class IsCorporate(BasePermission):
    """
    Access only for corporates
    """

    def has_permission(self, request, view):
        if isinstance(request.user, AnonymousUser):
            return False
        elif isinstance(request.user, UserDTO):
            if not request.user:
                raise AuthenticationFailed("Need authorization")
            try:
                request.user.user.profile
            except ObjectDoesNotExist:
                return False
            return bool(
                request.user.user.profile.profile_type == 'Corporate' and not request.user.session.is_deactivated
                and not request.user.session.is_blocked
            )
        return False